<?php
	error_reporting(E_ERROR); // | ~E_NOTICE);
	//error_reporting(E_ALL);

	header('Content-type: text/html; charset=utf-8');
	header('Vary: User-Agent');

	$dir = dirname(__FILE__) . '/inc/page/';
	$filename = $_GET['q'];
	$file = $dir . $filename . '.php';

	if(empty($filename) || !is_readable($file)) {
		foreach(glob($dir . '*') as $file) {
			$title = explode('.', array_reverse(explode('/', $file))[0])[0];
			$ul[] = sprintf('<li><a href="?q=%s" target="_blank">%s</a></li>', $title, $title);
		}
		return printf('<html><style>* {font: 16px Verdana}</style><body><h1>Детстрана</h1>%s</body></html>', implode("\n", $ul));
	}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui, user-scalable=no">
	<title>Детстрана</title>
	<link rel="stylesheet" type="text/css" href="css/style.css?<?php echo time()?>">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="js/mobile.js?<?php echo time() ?>"></script>
    <script src="js/main.js?<?php echo time() ?>"></script>
</head>

<body>


<?php include 'inc/popup/nav-main.php' ?>
<?php include 'inc/popup/register.php' ?>
<?php include 'inc/popup/profile.php' ?>
<?php include 'inc/popup/articles.php' ?>
<?php include 'inc/popup/news.php' ?>
<?php include 'inc/popup/date.php' ?>

<!--div class="overlay"></div-->

<div class="wrapper">
	<!-- Header -->
	<header>
		<div class="header__inner">
			<a href="#" data-target="#popup-profile" class="header__profile">
				<?php if($_GET['logged']) { ?>
				<img src="img/tmp/userpic1.png">
				<?php } ?>
			</a>
			<a href="#" data-target="#popup-register" class="header__logo"></a>
			<a href="#" data-target="#popup-nav-main" class="header__menu"></a>
		</div>
	</header>
	<!-- /Header -->

	<!-- Search -->
	<div class="search">
		<form class="form-search">
			<input type="text" name="q" placeholder="Поиск">
		</form>
	</div>
	<!-- /Search -->

	<!-- Content -->
	<!--p class="text-center">
		<button data-target="#popup-register">Открыть окошко регистрации</button>
	</p-->

	<?php include "inc/page/{$filename}.php" ?>
	
	<!-- /Content -->

	<!-- Footer -->
	<footer>
		<ul class="footer-links">
			<li><a href="#">Реклама</a></li>
			<li><a href="#">Контакты</a></li>
			<li><a href="#">Правообладателям</a></li>
			<li><a href="#">Пользовательское соглашение</a></li>
		</ul>
		<div class="footer-copy">
			&copy; 2017 Детстрана. Все права защищены.
		</div>
	</footer>
	<!-- /Footer -->

</div>


</body>

</html>