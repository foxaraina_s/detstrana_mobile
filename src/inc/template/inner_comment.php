<div class="entries">
    <div class="entries__list">
        <div class="entry entry-nobg">
            <div class="entry__body no-bg p-no mt-20 ">
                <div class="icon-group entry__comment">
                    <div class="rel mb-10">
                        <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                        <button class="btn btn-close entry__comment-close"></button>
                    </div>

                    <form class="entry__comment-form">
                        <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                        <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>