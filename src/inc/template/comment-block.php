<div class="entries">
<div class="entries__morecomments">
    <button class="btn btn-active-flat w-100">Еще 15 комментариев</button>
</div>
<div class="entry-comments__list">

    <div class="entry-comment">

        <div class="entry-comment__header">
            <div class="entry-comment__author">Виктория Кудрявцева</div>
            <div class="entry-comment__date">31 мая 2017 года, 12:58</div>
            <div class="entry-comment__icons">
                <a href="#" class="icon-info icon-info-likes-dark">5</a>
            </div>
        </div>

        <div class="entry-comment__body">
            <div class="entry-comment__inner">
                <div class="entry-comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
            </div>

            <div class="entry-comment__comment active">
                <div class="rel mb-20">
                    <button class="btn btn-flat entry-comment__comment-like">Нравится</button>
                    <button class="btn btn-active-flat entry-comment__comment-open">Ответить</button>
                    <button class="btn btn-close-thin entry-comment__comment-close"></button>
                </div>

                <form class="entry-comment__comment-form">
                    <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                    <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                </form>
            </div>

        </div>
    </div>

    <div class="entry-comment">

        <div class="entry-comment__header">
            <div class="entry-comment__author">Виктория Кудрявцева</div>
            <div class="entry-comment__date">31 мая 2017 года, 12:58</div>
            <div class="entry-comment__icons">
                <a href="#" class="icon-info icon-info-likes-dark">5</a>
            </div>
        </div>

        <div class="entry-comment__body">
            <div class="entry-comment__inner">

                <div class="entry-comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>

                <div class="slider entry-comment__slider">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide">
                                <img src="img/tmp/article5.jpg">
                                <img src="img/tmp/article5.jpg">
                            </div>
                            <div class="slide">
                                <img src="img/tmp/article5.jpg">
                                <img src="img/tmp/article5.jpg">
                            </div>
                            <div class="slide">
                                <img src="img/tmp/article5.jpg">
                                <img src="img/tmp/article5.jpg">
                            </div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

            </div>

            <div class="entry-comment__comment active">
                <div class="rel mb-20">
                    <button class="btn btn-flat entry-comment__comment-like">Нравится</button>
                    <button class="btn btn-active-flat entry-comment__comment-open">Ответить</button>
                    <button class="btn btn-close-thin entry-comment__comment-close"></button>
                </div>

                <form class="entry-comment__comment-form">
                    <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                    <div class="rel mb-10">
                        <button class="btn btn-confirm w-66">Комментировать</button>
                        <button class="btn btn-plus pull-right"></button>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>
</div>