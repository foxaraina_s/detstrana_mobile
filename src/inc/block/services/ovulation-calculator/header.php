	<div class="service-ovulation-calculator__header">
		<div class="service-ovulation-calculator__title">
			Калькулятор овуляции
		</div>
		<div class="service-ovulation__text">
			Получите личный календарь, в&nbsp;котором отмечены фазы цикла&nbsp;&mdash; менструальная, фолликулярная,
			овуляторная и&nbsp;лютеиновая. Вы&nbsp;увидите прогноз на&nbsp;три последующих месяца&nbsp;&mdash; благоприятные дни для зачатия.
			И&nbsp;даже узнаете предполагаемую дату родов!
		</div>
	</div>


	<ul class="page-breadcrumbs">
		<li><a href="#">Сервисы</a></li>
		<li><span>Калькулятор овуляции</span></li>
	</ul>

	<h1 class="mb-40">Калькулятор овуляции</h1>

	<div class="mlr-16 mb-20">
		<p>Укажите первый день своего последнего менструального цикла, его среднюю продолжительность и длительность менструации.</p>
	</div>