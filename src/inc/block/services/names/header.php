	<ul class="page-breadcrumbs">
		<li><a href="#">Сервисы</a></li>
		<li>Имена</li>
	</ul>

	<form class="service__form-search">
		<input type="text" name="city" value="" placeholder="Поиск по имени">
	</form>

	<ul class="nav service-names__icons">
		<li class="service-names__icon-girl active"><a href="#">Имена для<br>девочек</a></li>
		<li class="service-names__icon-boy"><a href="#">Имена для<br>мальчиков</a></li>
		<li class="service-names__icon-all"><a href="#">Все<br>имена</a></li>
	</ul>

	<h1 class="mb-40"><?php echo $title ?></h1>


	<div class="slider nav-tabs-slider mb-20 block-shaded">
		<div class="slider-wrapper slider-changeable">
			<ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100">
				<li class="slide w-50"><a href="#">Популярные</a></li>
				<li class="slide w-50"><a href="#">Редкие</a></li>
				<li class="slide w-50 active"><a href="#">По зодиаку</a></li>
				<li class="slide w-50"><a href="#">По происхождению</a></li>
				<li class="slide w-50"><a href="#">По именинам</a></li>
				<li class="slide w-50"><a href="#">По алфавиту</a></li>
			</ul>
		</div>
	</div>