	<div class="articles__list mb-20">
		<div class="article">
			<div class="article__image">
				<a href="#"><img src="img/tmp/article3.jpg"></a>
			</div>
			
			<div class="article__content">
				<a href="#" class="article__category">Звёздные дети</a>
				<a href="#" class="article__title">Сын принца Уильяма и кейт Миддлтон стал школьником</a>
			</div>

			<div class="icon-group article__icons">
				<a href="#" class="icon-info icon-info-views">24</a>
				<a href="#" class="icon-info icon-info-comments">8</a>
			</div>
		</div>

		<div class="article">
			<div class="article__image">
				<a href="#"><img src="img/tmp/article4.jpg"></a>
			</div>
			
			<div class="article__content">
				<a href="#" class="article__category">Факты и рекорды</a>
				<a href="#" class="article__title">Жительница Башкирии родила в 51 год</a>
			</div>

			<div class="icon-group article__icons">
				<a href="#" class="icon-info icon-info-views">65</a>
				<a href="#" class="icon-info icon-info-comments">1</a>
			</div>
		</div>

	</div>