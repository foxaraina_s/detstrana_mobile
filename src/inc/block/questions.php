	<div class="questions__list">

		<div class="question">
			<div class="question__header">
				<div class="question__avatar">
					<img src="img/tmp/userpic1.png">
				</div>
				<div class="question__author">Спрашивает <strong>Рада Мельникова</strong></div>
				<div class="question__date">31 мая 2017 года, 12:56</div>
			</div>
			
			<div class="question__body">
				<div class="question__category">Специалист по ГВ</div>
				<div class="question__title">Гиперлактация</div>
				<div class="question__text">
					Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа,
					последние две ночи вообще по 5 часов
				</div>
				<a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
				<a href="#" class="btn btn-confirm-flat block-center">Комментировать</a>
			</div>
		</div>

		<div class="question">
			<div class="question__header">
				<div class="question__avatar"></div>
				<div class="question__author"><strong>Анонимный вопрос</strong></div>
				<div class="question__date">31 мая 2017 года, 12:56</div>
			</div>
			
			<div class="question__body">
				<div class="question__category">Специалист по ГВ</div>
				<div class="question__title">Ссадина ранка</div>
				<div class="question__text">
					ЗДРАВСТВУЙТЕ! ребенок четыре дня назад упал на ручку. На ладошке появился пузырек плотный. Обработали зеленкой.
				</div>
				<a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
				<a href="#" class="btn btn-confirm-flat block-center">Комментировать</a>
			</div>
		</div>

	</div>