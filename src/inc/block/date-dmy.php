				<div class="form-birthday-day">
					<select name="dd" class="form-select form-select-center" placeholder="День">
						<option>День</option>
						<?php foreach(range(1, 31) as $day) { ?>
						<option value="<?php echo $day ?>"><?php echo $day ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-birthday-month">
					<select name="mm" class="form-select form-select-center" placeholder="Месяц">
						<option>Месяц</option>
						<?php foreach(['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'] as $key => $mon) { ?>
						<option value="<?php echo $key ?>"><?php echo $mon ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-birthday-year">
					<select name="yy" class="form-select form-select-center" placeholder="Год">
						<option>Год</option>
						<?php foreach(range(1950, 2000) as $year) { ?>
						<option value="<?php echo $year ?>"><?php echo $year ?></option>
						<?php } ?>
					</select>
				</div>