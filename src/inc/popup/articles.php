<!-- Popup articles categories -->
<div id="popup-articles" class="popup popup-articles popup-full">

	<div class="popup__back"></div>
	<div class="popup__close"></div>
	<div class="popup__header text-center" data-placeholder="Разделы статей">Разделы статей</div>	
	
	<div class="popup__body">
		<div class="popup__menu active">
			<div class="popup__submenu active">
				<ul class="popup__links">
					<li data-node="1" class="haschildren"><a href="#">Планирование</a></li>
					<li data-node="2"><a href="#">Беременность</a></li>
					<li data-node="3"><a href="#">Роды</a></li>
					<li data-node="4" class="haschildren"><a href="#">Малыш 0-1</a></li>
					<li data-node="5"><a href="#">Ребенок 1-3</a></li>
					<li data-node="6"><a href="#">Дети 3-7</a></li>
					<li data-node="7"><a href="#">Дети 7-12</a></li>
					<li data-node="8"><a href="#">Родители</a></li>
				</ul>
			</div>

			<div id="submenu-1" class="popup__submenu">
				<ul class="popup__links">
					<li data-node="1_1"><a href="#">Все</a></li>
					<li data-node="1_2"><a href="#">Подготовка к зачатию</a></li>
					<li data-node="1_3"><a href="#">Зачатие</a></li>
					<li data-node="1_4" class="haschildren"><a href="#">Бесплодие и альтернативные методы</a></li>
					<li data-node="1_5"><a href="#">Образ жизни</a></li>
				</ul>
			</div>

			<div id="submenu-4" class="popup__submenu">
				<ul class="popup__links">
					<li data-node="4_1"><a href="#">Все</a></li>
					<li data-node="4_2"><a href="#">Здоровье</a></li>
					<li data-node="4_3"><a href="#">Кормление</a></li>
					<li data-node="4_4"><a href="#">Уход</a></li>
					<li data-node="4_5"><a href="#">Развитие</a></li>
					<li data-node="4_6"><a href="#">Образ жизни</a></li>
				</ul>
			</div>

			<div id="submenu-1_4" class="popup__submenu">
				<ul class="popup__links">
					<li><a href="#">Все</a></li>
					<li><a href="#">Здоровье</a></li>
					<li><a href="#">Кормление</a></li>
					<li><a href="#">Уход</a></li>
					<li><a href="#">Развитие</a></li>
					<li><a href="#">Образ жизни</a></li>
				</ul>
			</div>
		</div>
	</div>

</div>
<!-- /Popup articles categories -->