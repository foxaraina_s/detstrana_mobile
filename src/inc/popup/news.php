<!-- Popup articles news -->
<div id="popup-news" class="popup popup-news popup-full">
	<div class="popup__close"></div>
	<div class="popup__header">Тематики новостей</div>
	<div class="popup__body">
		<ul class="popup__links">
			<li><a href="#">Планирование</a></li>
			<li><a href="#">Беременность</a></li>
			<li><a href="#">Роды</a></li>
			<li><a href="#">Малыш 0&ndash;1</a></li>
			<li><a href="#">Ребенок 3&ndash;7</a></li>
			<li><a href="#">Дети 7&ndash;10</a></li>
			<li><a href="#">Родители</a></li>
		</ul>
	</div>
</div>
<!-- /Popup articles news -->