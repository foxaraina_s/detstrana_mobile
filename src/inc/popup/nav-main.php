<!-- Popup main navigation -->
<div id="popup-nav-main" class="popup popup-nav-main popup-left">
	<div class="popup__close"></div>
	<div class="popup__header">
		<form class="form-search">
			<input type="text" name="q" placeholder="Поиск">
		</form>
	</div>
	<div class="popup__body">
		<div class="popup__inner">
			<ul class="popup-nav-main__links">
				<li><a href="#">Статьи</a></li>
				<li><a href="#">Новости</a></li>
				<li><a href="#">Сервисы</a></li>
				<li><a href="#">Консультации</a></li>
				<li><a href="#">Конкурсы</a></li>
				<li><a href="#">Блоги</a></li>
				<li><a href="#">Сообщества</a></li>
				<li><a href="#">Друзья</a></li>
			</ul>

			<div class="popup-nav-main__links-alt">
				<a href="#" class="btn btn-active-flat w-46">Планирование</a>
				<a href="#" class="btn btn-active-flat w-23">Дети</a>
				<a href="#" class="btn btn-active-flat w-23">Роды</a>
				<a href="#" class="btn btn-active-flat w-46">Беременность</a>
			</div>

			<div class="block-social popup-nav-main__social">
				<?php include 'inc/block/social.php' ?>
			</div>

			<div class="popup-nav-main__bottom-links">
				<a href="#">Полная версия</a>
			</div>
		</div>
	</div>
</div>
<!-- /Popup main navigation -->