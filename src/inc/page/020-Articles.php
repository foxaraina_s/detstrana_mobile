<div class="content content-articles pt-10 scroll-top">
	
	<h1 class="mb-20">Статьи</h1>

	<div class="mb-20">
		<button data-target="#popup-articles" class="btn btn-popup btn-white">Разделы статей</button>
	</div>

	<div class="article">
		<div class="article__image">
			<a href="#"><img src="img/tmp/article11.jpg"></a>
		</div>
		
		<div class="article__content">
			<a href="#" class="article__title">8 удивительных способов родить ребёнка</a>
		</div>

		<ul class="nav nav-dotted pb-20">
			<li class="active"><a href="#"></a></li>
			<li><a href="#"></a></li>
			<li><a href="#"></a></li>
			<li><a href="#"></a></li>
			<li><a href="#"></a></li>
		</ul>
		
	</div>


	<?php $mb = 10; include 'inc/block/tabs.php'; ?>


	<div class="articles mb-40">
		<?php include 'inc/block/articles.php' ?>

		<div class="text-center"><button class="btn btn-active-flat">Все статьи</button></div>
	</div>


	<h2 class="mb-20">Новости</h2>

	<div class="mb-20">
		<button data-target="#popup-news" class="btn btn-popup btn-white">Тематики новостей</button>
	</div>

	<div class="articles mb-40">
		<?php include 'inc/block/news.php' ?>

		<div class="text-center"><a href="#" class="btn btn-active-flat">Все новости</a></div>
	</div>


</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>