<div class="users pt-10 scroll-top">

	<h1 class="mb-40">Пользователи детстраны</h1>

	<form class="form-search">
		<input type="text" name="city" value="" placeholder="Начните вводить имя">
	</form>
	
	<button data-target="#popup-users-filter" class="btn btn-popup btn-white mb-20">Показать фильтр</button>

	<form class="users-filter-form mb-20 w-23 block-center nowrap">
		<div class="form-group form-group-checkbox text-center">
			<input type="checkbox" name="is-online" id="users-filter__is-online">
			<label for="users-filter__is-online" class="form-label-large">В сети</label>
		</div>
	</form>


	<div class="text-grey caption-small mlr-16 mb-10">Люди, которые вам могут быть интересны:</div>
	

	<div class="users__list mb-20">
		<?php foreach(range(1,5) as $i) { ?>
		<div class="user user-clickable <?php echo $i != 3 ? 'user-online' : '' ?> <?php echo $i==2 ? 'user-active' : '' ?> mb-10">
			<div class="user__header clickable">
				<div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
				<div class="user__name">Рада Мельникова</div>
				<div class="user__about">
					День рождения: 32 февраля<br>
					Город: Сургут
				</div>
			</div>
			<div class="user__body">
				<div class="user__body-inner">
					<div class="text-grey caption-small mb-10">Дети:</div>
					<div class="user__children">
						<a href="#" class="user__child user__child--boy">7 лет</a>
						<a href="#" class="user__child user__child--girl">9 лет</a>
					</div>
				
					<a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
					<a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>

	<?php include 'inc/block/pagination.php'; ?>

</div>

<?php include 'inc/block/pregnancy-calendar.php'; ?>

<script>
$('.users').on('click', '.user.user-clickable .user__header', function() {
	$(this)
		.parents('.user')
		.eq(0)
		.toggleClass('user-active')
});
</script>