
<section class="service service-names name-block">
    <div class="service__subtitle"><a href="#">Сервисы</a> <i>❯</i> Имена</div>
    <h1 class="mb-30">Имя Абдулла</h1>
    <div class="mb-20 text-center">
        <a href="#" class="btn btn-active-flat">Вернуться на страницу имен</a>
    </div>
    <div class="service-names__info mb-40">
        <ul>
            <li>Происхождение имени - арабское</li>
            <li>Значение имена - Раб Бога</li>
        </ul>
    </div>

    <div class="icon-group text-center mb-40">
        <a href="#" class="icon-rounded icon-fb"></a>
        <a href="#" class="icon-rounded icon-vk"></a>
        <a href="#" class="icon-rounded icon-ok"></a>
    </div>
    <div class=" mb-40 service__text service__text-no">
        <p>Маленький Абдулла очень любознательный мальчик. Ему интересно буквально все. Он спрашивает обо всем на свете.
            Родители просто не успевают отвечать на все его бесконечные вопросы. Абдулла не избалован и неприхотлив. Для
            счастья ему нужно совсем минимум. Он любит играть со сверстниками и уважает старших. Обучение дается носителю
            имени не очень легко. Ему не хватает терпения и усидчивости. Он может отвлечься от процесса обучения, прослушать
            учителя и просто постесняться признаться, что не понял новую тему. </p>
        <p>Взрослый Абдулла такой же не собранный, как и маленький. Дома, если Абдулла живет один, у него вечно все разбросано.
            Полный хаос. Но Абдулла не в претензии, у него нет дискомфорта. Руководящей должности обычно Абдулла не занимает. Максимум это среднее руководящее звено.
            Друзья его любят, таким, каким он есть, со всеми его недостатками. Он любит шумные компании, на любом празднике
            Абдулла один из первых начинает веселиться и один из последних уходит. Женится он обычно поздно. Он не спешит
            прощаться со своей свободой. Слишком он привык к своему образу жизни. Абдулла прекрасно понимает, что вместе с
            браком придется менять весь уклад его неорганизованного существования. Однако часто приходит все-таки это время.
            Когда Абдулла переходит тридцатилетний рубеж, он начинает задумываться о семье. </p>
        <p>Мужем он старается быть внимательным. Но жене его приходится нелегко. Слишком Абдулла свободолюбивый. Он не привык считаться с кем-то
            еще кроме себя. Ему важен личный комфорт, хотя он и может заключаться в полном хаосе. Он много времени уделяет
            не семье, а другому своему окружению. Супруге Абдуллы нужно быть готовой, что его дом всегда должен быть открыт
            для многочисленных друзей. И посиделки могут быть до утра. Что касается детей, Абдулла не против ребенка, но
            занимается с сыном или дочерью довольно мало. Он просто не в курсе, как обращаться с ребенком. Поэтому все
            воспитание чада ложится на плечи его жены. С годами, впрочем, в носителе имени заметны изменения. Он становится
            более домашним, привыкает к порядку и уюту в доме. Домой его тянет.</p>
    </div>
    <div class="service-names__horoscope">
        <h3 class="title">Подходит для знаков зодиака</h3>
        <div class="text-center">
            <a class="horoscope-item horoscope-item--capricon" href="#">Козерог</a>
            <a class="horoscope-item horoscope-item--aquarius" href="#">Водолей</a>
            <a class="horoscope-item horoscope-item--virgo" href="#">Дева</a>
            <a class="horoscope-item horoscope-item--taurus" href="#">Телец</a>
        </div>

    </div>
    <div class="service-names__compatibility mb-40">
        <h3 class="title">Совместим с именем</h3>
        <ul>
            <li><a href="#">Ольга</a></li>
            <li><a href="#">Ева</a></li>
            <li><a href="#">Лилия</a></li>
            <li><a href="#">Мария</a></li>
            <li><a href="#">Тамара</a></li>
            <li><a href="#">Сабина</a></li>
            <li><a href="#">Анисия</a></li>
            <li><a href="#">Руфина</a></li>
            <li><a href="#">Диляра</a></li>
            <li><a href="#">Тереза</a></li>
            <li><a href="#">Ефимия</a></li>
            <li><a href="#">Гузель</a></li>

        </ul>
    </div>
    <div class="service-names__talisman">
        <h3 class="title">Талисман</h3>
        <ul>
            <li><span>Камень:</span> берилл;</li>
            <li><span>Растение:</span> василек;</li>
            <li><span>Животное:</span> барс;</li>
        </ul>
    </div>
    <div class="service-names__derived">
        <h3 class="title">Производные имени</h3>
        <ul>
            <li>Абдулла</li>
            <li>Абдул</li>
            <li>Абдааллах</li>
            <li>Абдалла</li>
            <li>Абдуло</li>
        </ul>
    </div>
    <div class="service-names__famous mb-40">
        <h3 class="title">Знаменитости с этим именем</h3>
        <ul>
            <li>
                <img src="img/fm-abdulla1.png">
                <h4 class="uppercase-title-sm">Абдаллах Ансари</h4>
                <div class="text-no">Персидский поэт-суфий</div>
            </li>
            <li>
                <img src="img/fm-abdulla2.png">
                <h4 class="uppercase-title-sm">Абдаллах-Хан II</h4>
                <div class="text-no">Персидский поэт-суфий</div>
            </li>
        </ul>
    </div>
    <div class="service-names__also-see">
        <h3 class="title">Также смотрят</h3>
        <ul>
            <li><a href="#">Адам</a></li>
            <li><a href="#">Аарон</a></li>
            <li><a href="#">Автандил</a></li>
        </ul>
    </div>
</section>
//= ../template/calendar.php

