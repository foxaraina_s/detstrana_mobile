
<section class="service service-gender scroll-top">
    <div class="service__subtitle"><a href="#">Сервисы</a> <i>❯</i> Определение пола</div>
    <h2 class="service__title">Определение пола ребенка по китайскому методу</h2>
    <div class="service__text">Чтобы узнать пол ребенка, укажите возраст мамы и месяц зачатия ребенка</div>
    <div class="gender__method--wrapper gender-method-chinease">
        <div class="gender__method--form chinese_table_block">
            <form method="post">
            <div class="gender-form-mom gender-form chinese_table_block_l">
                <div class="gender__section-title">Возвраст мамы:</div>
                <div class="form-group form-group-birthday  mb-20">
                    <div class="form-birthday-month">
                        <select name="mm" placeholder="Лет" class="form-select select_big_input" id="ctb1">
                            <option value="0">18</option>
                            <option value="1">19</option>
                            <option value="2">20</option>
                            <option value="3">21</option>
                            <option value="4">22</option>
                            <option value="5">23</option>
                            <option value="6">24</option>
                            <option value="7">25</option>
                            <option value="8">26</option>
                            <option value="9">27</option>
                            <option value="10">28</option>
                            <option value="11">29</option>
                            <option value="12">30</option>
                            <option value="13">31</option>
                            <option value="14">32</option>
                            <option value="15">33</option>
                            <option value="16">34</option>
                            <option value="17">35</option>
                            <option value="18">36</option>
                            <option value="19">37</option>
                            <option value="20">38</option>
                            <option value="21">39</option>
                            <option value="22">40</option>
                            <option value="23">41</option>
                            <option value="24">42</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="mb-40 gender-form-zigota gender-form chinese_table_block_r">
                <div class="gender__section-title">Месяц зачатия ребенка:</div>
                <div class="form-group form-group-birthday  mb-20">
                    <div class="form-birthday-month">
                        <select name="mm" placeholder="Месяц" class="form-select select_big_input" id="ctb2">
                            <option value="0">Январь</option>
                            <option value="1">Февраль</option>
                            <option value="2">Март</option>
                            <option value="3">Апрель</option>
                            <option value="4">Май</option>
                            <option value="5">Июнь</option>
                            <option value="6">Июль</option>
                            <option value="7">Август</option>
                            <option value="8">Сентябрь</option>
                            <option value="9">Октябрь</option>
                            <option value="10">Ноябрь</option>
                            <option value="11">Декабрь</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a class="btn btn-confirm btn-shaded w-93 baby_gender_button chinese_table">Узнать пол</a>
            </div>
            </form>
        </div>
        <div id="result">
            <div class="blood_group_result baby-gender-result">
                <div class="blood_group_result_content baby-gender-result__inner">
                    <div>ПОЗДРАВЛЯЕМ!</div><div>У ВАС, СКОРЕЕ ВСЕГО, БУДЕТ <span id=bgr_gender>девочка</span></div>
                </div>
            </div>
            <h3 class="text-w--regular text-medium gender__subtitle">Календарь зачатия ребенка</h3>
            <table class="chienese-results csd_table">
                <tbody>
                <tr>
                    <td>Возр.<br>мамы</td>
                    <td>Янв</td>
                    <td>Февр</td>
                    <td>Март</td>
                    <td>Апр</td>
                    <td>Май</td>
                    <td>Июнь</td>
                    <td>Июль</td>
                    <td>Авг</td>
                    <td>Снт</td>
                    <td>Окт</td>
                    <td>Ноябр</td>
                    <td>Дек</td>
                </tr>
                </tbody>
            </table>
            <div class="icon-group text-center mt-20">
                <a href="#" class="icon-rounded icon-fb"></a>
                <a href="#" class="icon-rounded icon-vk"></a>
                <a href="#" class="icon-rounded icon-ok"></a>
            </div>
            <h3 class="gender__subtitle">Другие способы определения пола ребенка</h3>
            <div class="gender__choose">
                <div class="gender__choose--methods">
                    <a href="service-baby-gender--blood-refresh.html" class="method--item blood__refresh">
                        По обновлению крови
                    </a>
                    <a href="service-baby-gender-blood-group.html" class="method--item blood__group">
                        По группе крови родителей
                    </a>
                    <a href="service-baby-gender-japan.html" class="method--item japanese__table">
                        Японский метод
                    </a>
                </div>

            </div>

        </div>
        //= ../template/inner_comment.php
    </div>
        //= ../template/comment-block.php
    <p class="text-center"><button class="btn btn-active-flat">Больше записей</button></p>
    <div class="service__text">Определение пола ребенка по обновлению крови родителей считается одним из самых
        популярных методов. Он основывается на данных о будущих родителях. А именно, на периодах обновления крови у папы
        и у мамы. Считается, что у девушек оно проходит 1 раз за 3 года, а у мужчин – 1 раз в четыре года. <br>
        Так, когда вы пользуетесь калькулятором, определяющим пол будущего ребенка по обновлению крови, нужно знать: чья кровь была
        более «свежая» на момент зачатия, такого пола и будет кроха. <br>
        При расчете пола ребенка по обновлению крови также надо учитывать следующее: если вам делали переливание, то расчет будет вестись от даты последнего переливания
        крови. И еще: результат может быть искаженным, если пол ребенка по обновлению крови родителей вы рассчитали без
        учета недавних операций или различных травм с кровотечениями. Если метод определения пола по обновлению крови
        онлайн кажется вам не совсем достоверным, воспользуйтесь другими видами тестов.
    </div>
</section>
//= ../template/calendar.php
<script src="js/gender_js.js?<?php echo time() ?>"></script>
