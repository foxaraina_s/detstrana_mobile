<div class="content content-articles scroll-top pt-20">

	<ul class="page-breadcrumbs">
		<li class="back"><a href="#">вернуться в раздел</a></li>
	</ul>
	
	<h1 class="mb-40">Здоровый старт: зачем</h1>


	<div class="article-one">
		<div class="article-one__image">
			<img src="img/tmp/article7.jpg">
			<!--div class="slider entry__images">
				<div class="slider-changeable slider-wrapper">
					<div class="slides">
						<div class="slide"><img src="img/tmp/article7.jpg"></div>
						<div class="slide"><img src="img/tmp/article7.jpg"></div>
						<div class="slide"><img src="img/tmp/article7.jpg"></div>
						<div class="slide"><img src="img/tmp/article7.jpg"></div>
						<div class="slide"><img src="img/tmp/article7.jpg"></div>
					</div>
				</div>

				<ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse-transparent nav-dotted-bottom slider-control slider-changeable">
					<li class="active"><a href="#"></a></li>
					<li><a href="#"></a></li>
					<li><a href="#"></a></li>
					<li><a href="#"></a></li>
					<li><a href="#"></a></li>
				</ul>
			</div-->
		</div>
		<div class="article-one__body">
			<div class="article-one__author mb-10">
				Автор: <span class="article-one__author-name">Виктория Кудрявцева</span>
			</div>
			<div class="article-one__date mb-20">
				31 мая 2017, 12:56
			</div>

			<div class="icon-group text-center mb-20">
				<a href="#" class="icon-info icon-info-views">120</a>
				<a href="#" class="icon-info icon-info-comments">3</a>
				<a href="#" class="icon-info icon-info-likes">3</a>
			</div>
		


			<div class="mlr-16 pt-20 pb-20">
				<em>Любите рассматривать «первые фото с УЗИ»? Врачи – тоже, хотя и видят их совсем не так, как вы. Давайте попробуем выяснить, зачем медики постоянно направляют беременных на обследования и что в действительности означают получаемые результаты.</em>
				<h2>Больше, чем картинка</h2>
				<p>Все беременные в обязательном порядке проходят ультразвуковое исследование (УЗИ). Оно поистине незаменимо, поскольку позволяет оценить общее состояние плода (будущего малыша). Этот вид исследований достаточно информативен и при этом безопасен, и поэтому назначается каждой будущей маме. Что может узнать врач, посмотрев на результат УЗИ? 				</p>


				<div class="caption-small mb-20">Теги по теме:</div>
				
				<ul class="nav nav-tags mb-20">
					<li><a href="#">Здоровье</a></li>
					<li class="active"><a href="#">Море</a></li>
				</ul>

				<!--div class="caption-small text-center mb-20">Оцените материал:</div-->

				<div class="block-social">
					<?php include 'inc/block/social.php' ?>
				</div>			
			</div>

			<a href="#" class="btn btn-confirm-flat block-center">Прокомментируйте</a>
		</div>

	</div>

	

	<h2>Смотрите также:</h2>
	
	<div class="articles-subscribe mb-20">
		<div class="block-subscribe__title">Интересно?</div>
		<div class="block-subscribe__subtitle">Подпишись на лучшие материалы в еженедельной рассылке</div>
		<form>
			
			<div class="form-group mb-40">
				<label class="form-label">Ваш e-mail</label>
				<input name="email" type="email" placeholder="Введите e-mail" maxlength="20" class="form-control">
				<div class="form-error">&nbsp;</div>
			</div>

			<button class="btn btn-confirm w-100">Подписаться</button>
		</form>

	</div>



	<div class="articles-also-viewed mb-20">
		<h2>С этой статьёй также смотрят</h2>
		<p><a href="#">Как протекает первая неделя беременности</a></p>
		<p><a href="#">Что можно и нельзя делать в первые недели беременности</a></p>
		<p><a href="#">Как питаться на первых неделях беременности</a></p>
	</div>

	<h2>Комментариев (30)</h2>
	<a href="#" class="btn btn-confirm-flat block-center mb-20">Комментировать</a>
	


	<?php include 'inc/block/comments.php' ?>

	<h2 class="mb-20">Узнайте себя</h2>

	<div class="articles">
			<div class="articles__list mb-20">
			<div class="article">
				<div class="article__image">
					<a href="#"><img src="img/tmp/article9.jpg"></a>
				</div>
				
				<div class="article__content">
					<a href="#" class="article__category">Тест: 6 вопросов</a>
					<a href="#" class="article__title">Какая диета вам подходит?</a>
				</div>

				<div class="icon-group article__icons">
					<a href="#" class="icon-info icon-info-views">120</a>
					<a href="#" class="icon-info icon-info-comments">3</a>
					<a href="#" class="icon-info icon-info-likes">3</a>
				</div>
			</div>

			<div class="article">
				<div class="article__mark">Партнёрский материал</div>

				<div class="article__image">
					<a href="#"><img src="img/tmp/article10.jpg"></a>
				</div>
				
				<div class="article__content">
					<a href="#" class="article__category">Тест: 15 вопросов</a>
					<a href="#" class="article__title">Любовь к себе: каков ваш уровень самооценки?</a>
				</div>

				<div class="icon-group article__icons">
					<a href="#" class="icon-info icon-info-views">674</a>
					<a href="#" class="icon-info icon-info-comments">13</a>
				</div>
			</div>

		</div>
		<a href="#" class="btn btn-active-flat block-center mb-20">Все тесты</a>
	</div>


	<h2 class="mb-20">Вас заинтересует</h2>

	<div class="articles mb-40">
		<?php include 'inc/block/articles.php' ?>

		<div class="text-center"><button class="btn btn-active-flat">Все статьи</button></div>
	</div>

</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>