
<!-- Content -->
<!--p class="text-center">
    <button data-target="#popup-register">Открыть окошко регистрации</button>
</p-->

<div class="mlr-16 pt-10 mb-20 scroll-top">

    <h1>О проекте</h1>

    <p>Дорогие друзья, приглашаем вас в&nbsp;удивительное путешествие в&nbsp;Детстрану!</p>

    <p>Детстрана&nbsp;&mdash; это современная площадка, созданная с&nbsp;учетом интересов и&nbsp;потребностей мам, пап и&nbsp;тех, кто планирует семью. </p>

    <p>Воплощая проект в&nbsp;жизнь, мы&nbsp;думали о&nbsp;том, что могло&nbsp;бы объединять всех нас. Конечно, это любовь. Любовь к&nbsp;дому, семье, детям, работе, учебе, новым впечатлениям и&nbsp;встречам. Любовь во&nbsp;всем&nbsp;&mdash; словах, взглядах, действиях и&nbsp;мыслях.</p>

    <p>Вспомните, почувствуйте, как с&nbsp;любовью можно смотреть на&nbsp;две заветные полоски теста, с&nbsp;какой нежностью хочется выбирать пинетки для малыша, и&nbsp;с&nbsp;какой всепоглощающей заботой&nbsp;&mdash; уберегать его от&nbsp;жизненных невзгод и&nbsp;опасностей.</p>

    <p>Эти частички любви мы&nbsp;взяли с&nbsp;собой в&nbsp;дорогу, куда приглашаем и&nbsp;вас. Кто-то, путешествуя, получит важное руководство к&nbsp;действию, а&nbsp;кто-то вдохновится интересными идеями. Многие наверняка найдут единомышленников, а&nbsp;другие обязательно захотят поделиться своими чувствами и&nbsp;эмоциями. В&nbsp;Детстране можно получить полезные знания или поделиться уникальным опытом с&nbsp;другими путешественниками.</p>

    <p>Блоги, новости, удобные информативные сервисы сделают ваше пребывание на&nbsp;страницах сайта приятным и&nbsp;полезным. А&nbsp;мы&nbsp;также, с&nbsp;любовью, продолжим заботиться о&nbsp;вас.

    <p>Детстрана&nbsp;&mdash; социальная сеть для будущих и&nbsp;состоявшихся родителей.</p>

</div>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides">
                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
//= ../template/calendar.php