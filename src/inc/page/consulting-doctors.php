
<div id="popup-doctors" class="popup popup-center popup-doctors popup-open">
    <div class="popup__close"></div>
    <div class="popup__header">Врачи</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Аллерголог детский</a></li>
            <li><a href="#">Андролог</a></li>
            <li><a href="#">Астролог</a></li>

        </ul>
        <ul class="filter-abc-linear">
            <li><a href="#">А</a></li>
            <li><a href="#">В</a></li>
            <li><a href="#">Ш</a></li>
            <li><a href="#">Н</a></li>
        </ul>
    </div>
</div>
<div class="content content-consulting pt-10 scroll-top">

    <h1 class="mb-40">Онлайн-консультации</h1>

    <a href="#" class="btn btn-active btn-shaded w-93 mb-20 block-center">Задать вопрос экспертам</a>

    <button data-target="#popup-doctors" class="btn btn-popup btn-white mb-20">Врачи</button>

    <h2 class="mb-20">Популярные консультации</h2>

    <div class="search mb-20">
        <form class="form-search">
            <input type="text" name="q" placeholder="Поиск по вопросам">
        </form>
    </div>

    <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100 mb-20">
        <li class="w-50 active"><a href="#">Новое</a></li>
        <li class="w-50"><a href="#">Популярное</a></li>
    </ul>


    <div class="questions mb-40">
        <div class="questions__list">

            <div class="question">
                <div class="question__header">
                    <div class="question__avatar">
                        <img src="img/tmp/userpic1.png">
                    </div>
                    <div class="question__author">Спрашивает <strong>Рада Мельникова</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Гиперлактация</div>
                    <div class="question__text">
                        Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа,
                        последние две ночи вообще по 5 часов
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                    <a href="#" class="btn btn-confirm-flat block-center">Комментировать</a>
                </div>
            </div>

            <div class="question">
                <div class="question__header">
                    <div class="question__avatar"></div>
                    <div class="question__author"><strong>Анонимный вопрос</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Ссадина ранка</div>
                    <div class="question__text">
                        ЗДРАВСТВУЙТЕ! ребенок четыре дня назад упал на ручку. На ладошке появился пузырек плотный. Обработали зеленкой.
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                    <a href="#" class="btn btn-confirm-flat block-center">Комментировать</a>
                </div>
            </div>

        </div>	</div>

    <ul class="pagination text-center mb-20">
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">...</a></li>
    </ul></div>
//= ../template/calendar.php