<div class="profile profile-pregnancy scroll-top">
	<h1 class="mb-10">Город</h1>

	<div class="profile__step">Шаг 3 из 4</div>
	
	<div class="profile__description">Укажите, на каком этапе материнства вы находитесь&nbsp;&mdash; это поможет найти друзей, близких по духу и интересам!</div>


	<form action="#" method="post" class="profile__form">
	
		<div class="mb-20">
			<div class="form-group form-group-radio">
				<input type="radio" name="pregnancy-status" value="1" id="profile-form__pregnancy-status-1" checked>
				<label for="profile-form__pregnancy-status-1">Я не беременна</label>
			</div>
			
			<div class="form-group form-group-radio">
				<input type="radio" name="pregnancy-status" value="2" id="profile-form__pregnancy-status-2">
				<label for="profile-form__pregnancy-status-2">Я планирую беременность</label>
			</div>
			
			<div class="form-group form-group-radio">
				<input type="radio" name="pregnancy-status" value="3" id="profile-form__pregnancy-status-3">
				<label for="profile-form__pregnancy-status-3">Я беременна</label>
			</div>
		</div>
			
		<button class="btn btn-confirm btn-shaded w-100 mb-20">Сохранить и продолжить</button>

		<button class="btn btn-active-flat btn-center">Пропустить шаг</button>
	</form>
</div>