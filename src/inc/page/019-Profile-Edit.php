<div class="profile profile-about pt-10 scroll-top">

	<h1 class="mb-20">Настройки профиля</h1>

	<div class="slider nav-tabs-slider mb-20 block-shaded">
		<div class="slider-wrapper slider-changeable">
			<ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100">
				<li class="slide w-50 active"><a href="#">Обо мне</a></li>
				<li class="slide w-50"><a href="#">Город</a></li>
				<li class="slide w-50"><a href="#">Беременность</a></li>
				<li class="slide w-50"><a href="#">Дети</a></li>
			</ul>
		</div>
	</div>

	<div class="profile-about__body">

		<form action="#" method="post" enctype="multipart/form-data">

			<div class="rel mb-20">
				<div class="profile-about__userpic">
					<img src="img/tmp/userpic1.png">
				</div>
				<div class="profile-about__upload">
					<button class="btn btn-confirm btn-shaded btn-padded mb-10">Загрузить фотографию</button>
					<div class="profile-about__upload-date">.JPG или .PNG до 1 Mb</div>
				</div>
			</div>


			<div class="profile__section-title">Ваш пол</div>

			<div class="form-group form-group-choice mb-20">
				<input type="radio" name="gender" id="profile-about__women" value="2">
				<label for="profile-about__women">Женский</label>

				<input type="radio" name="gender" id="profile-about__men" value="1" checked>
				<label for="profile-about__men">Мужской</label>
			</div>



			<div class="profile__section-title">Ваше имя и фамилия</div>

			<div class="form-group">
				<label class="form-label">Ваше имя</label>
				<input name="user" type="text" placeholder="Ваше имя" maxlength="20" class="form-control">
				<div class="form-limit">&nbsp;</div>
				<div class="form-error">&nbsp;</div>
			</div>

			<div class="form-group mb-20">
				<label class="form-label">Ваша фамилия</label>
				<input name="name" type="text" placeholder="Ваша фамилия" class="form-control">
				<div class="form-limit">&nbsp;</div>
				<div class="form-error">&nbsp;</div>
			</div>


			<div class="profile__section-title">Ваш день рождения</div>



			<div class="form-group form-group-birthday mb-20">
				<?php include 'inc/block/date-dmy.php' ?>
			</div>


			<div class="form-group form-group-checkbox mb-20">
				<input type="checkbox" name="is-hide-year" id="profile-about__is-hide-year">
				<label for="profile-about__is-hide-year" class="form-label-large">Скрывать год рождения</label>
			</div>


			<div class="profile__section-title">Расскажите о себе</div>

			<div class="form-group">
				<textarea name="about" placeholder="Короткий текст, не более 1800 символов, создаст первое впечатление о вашем дневнике."></textarea>
				<div class="form-limit"></div>
				<div class="form-error"></div>
			</div>

			<button class="btn btn-confirm btn-shaded w-100 mb-20">Сохранить и продолжить</button>

			<button class="btn btn-error-flat btn-center">Удалить профиль</button>


		</form>

	</div>
</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>