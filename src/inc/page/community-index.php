
<div class="content content-community scroll-top">
        <h1 class="mb-20">Сообщества</h1>
        <button data-target="#popup-user-filter" class="btn btn-popup btn-white mb-20">Показать фильтр</button>
        <div class="communities mb-20">
            <div class="communities__list">
                <div class="community mb-10">

                    <div class="community__title">
                       Подготовка к зачатию
                    </div>

                    <div class="community__image">
                        <img src="img/tmp/community1.png">
                        </div>
                    <div class="community__body">
                        <div class="icon-group community__icons">
                            <div class="community__icons-item"> <a href="#" class="icon-info icon-info-notes">71 запись</a>
                                <button class="btn btn-confirm btn-shaded w-46">Создать запись</button>
                            </div>
                            <div class="community__icons-item"> <a href="#" class="icon-info icon-info-people">10528 участников</a>
                                <button class="btn btn-confirm btn-shaded w-46">Вступить</button>
                            </div>

                        </div>
                        <ul class="community__notes mb-40">
                            <li><a href="#">Забеременеть в Хаддассе <span>38</span></a></li>
                            <li><a href="#">Хотим зачать пацана <span>52</span></a></li>
                            <li><a href="#">Астрология и дата зачатия связаны? <span>16</span></a></li>
                            <li><a href="#">Забеременеть в Хаддассе <span>7</span></a></li>
                        </ul>
                        <button class="btn btn-active-flat btn-center">Больше записей</button>
                    </div>


                    </div>
                <div class="community mb-10">

                    <div class="community__title">
                        Подготовка к родам и роды
                    </div>

                    <div class="community__image">
                        <img src="img/tmp/community2.png">
                    </div>
                    <div class="community__body">
                        <div class="icon-group community__icons">
                            <div class="community__icons-item"> <a href="#" class="icon-info icon-info-notes">71 запись</a>
                                <button class="btn btn-confirm btn-shaded w-46">Создать запись</button>
                            </div>
                            <div class="community__icons-item"> <a href="#" class="icon-info icon-info-people">10528 участников</a>
                                <button class="btn btn-confirm btn-shaded w-46">Вступить</button>
                            </div>

                        </div>
                        <ul class="community__notes mb-40">
                            <li><a href="#">После родов похудела на 27 кг <span>38</span></a></li>
                            <li><a href="#">Роды за руку с мужем <span>52</span></a></li>
                            <li><a href="#">Боюсь начала схваток! <span>16</span></a></li>
                            <li><a href="#">Забеременеть в Хаддассе <span>7</span></a></li>
                        </ul>
                        <button class="btn btn-active-flat btn-center">Больше записей</button>
                    </div>


                </div>


                </div>
            </div>
    </div>






//= ../template/calendar.php