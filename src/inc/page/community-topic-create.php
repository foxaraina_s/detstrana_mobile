<div id="popup-tags" class="popup popup-center popup-doctors popup-open">
    <div class="popup__close"></div>
    <div class="popup__header">Теги детстраны</div>
    <div class="popup__body">
        <ul class="popup__links mb-40">
            <li><a href="#">Август</a></li>
            <li><a href="#">Беременность</a></li>
            <li><a href="#">Восспитание</a></li>

        </ul>
        <div class="mlr-16">
            <form>
                <div class="form-group form-group-error">
                    <label class="form-label form-label-hidden">Впишите новый тег</label>
                    <input name="tag" type="text" placeholder="Впишите новый тег" maxlength="20" class="form-control">
                    <div class="form-limit">0 / 20</div>
                    <div class="form-error">Максимум 20 символов;</div>
                </div>
                <button class="btn btn-confirm btn-shaded w-100 mb-20">Добавить</button>
            </form>
        </div>


    </div>
</div>
<div class=" content content-community scroll-top pt-20">
    <ul class="page-breadcrumbs">
        <li class="back"><a href="#">вернуться к сообществу</a></li>
    </ul>
        <h1 class="mb-20">Новая публикация для сообщества «Подготовка к зачатия»</h1>
    <div class="mlr-16 mb-20">
        <p>Вы не соостоите в сообществе «Подготовка к зачатию». С публикпцией записи вы автоматически
            становитесь участником сообщества.</p>
    </div>
    <div class="new-record-form mlr-16 mb-20">
        <form action="#" method="post" enctype="multipart/form-data">
            <div class="form-group mb-20">
                <label class="form-label form-label-hidden">Заголовок</label>
                <input name="name" type="text" placeholder="Заголовок" class="form-control">
                <div class="form-limit">&nbsp;</div>
                <div class="form-error">&nbsp;</div>
            </div>
            <div class="text-editor">
                <div class="text-editor__toolbar btn-toolbar">
                    <div class="slider text-editor-slider mb-20">
                        <div class="slider-wrapper slider-changeable">
                            <ul class="nav edit-text-slide edit-text-scrollable  w-100">
                                <li class="slide">
                                    <button class="semibold"><i class="material-icons">&#xE238;</i> </button>
                                    <button class="italic"><i class="material-icons">&#xE23F;</i> </button>
                                    <button class="underline"><i class="material-icons">&#xE249;</i> </button>
                                    <button class="color"><i class="material-icons">&#xE23A;</i> </button>
                                </li>
                                <li class="slide">
                                    <button class="left-align"><i class="material-icons">&#xE236;</i> </button>
                                    <button class="center-align"><i class="material-icons">&#xE234;</i> </button>
                                    <button class="right-align"><i class="material-icons">&#xE234;</i> </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="text-editor__editable">
                    <span>Введите текст</span>
                    <br>
                </div>
            </div>
            <button data-target="#popup-tags" class="btn btn-popup btn-white mb-20">Укажите теги</button>
            <div class="form-group mb-20">
                <select name="see-note" class="form-select form-select-center" placeholder="Запись видна">
                    <option value="0">Запись видна</option>
                    <option value="1">Всем</option>
                    <option value="2">Только друзьям</option>
                    <option value="3">Только мне</option>

                </select>
            </div>
            <div class="mb-20"><em>Вашу запись увидят и прочитают множетсво людей. Вы получите большее количество комментариев,
                лайков и станенте самым читаемы пользователем</em></div>
            <div class="form-group form-group-checkbox mb-10">
                <input type="checkbox" name="active_mom" id="active_mom">
                <label for="active_mom" class="form-label-large">На конкурс «Активная мама»*</label>
            </div>
            <div class="mb-20"><em>*Я подтверждаю, что запись соответствует правила конкурса «Активная мама» и содержит уникальный контент</em></div>
            <button class="btn btn-confirm btn-shaded w-100 mb-20">Опубликовать</button>
            <div class="text-center"><button class="btn btn-active-flat">Отмена</button></div>
        </form>
    </div>

    //= ../template/calendar.php