
<div class="content content-consulting pt-10 scroll-top">
    <ul class="page-breadcrumbs">
        <li><a href="#">Люди</a></li>
        <li><span>Страница консультанта</span></li>
    </ul>
    <div class="users">
        <div class="users__list mb-20">
            <div class="user user-online user-active">
                <div class="user__header clickable">
                    <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="user__name">Нелли Денисова</div>
                    <div class="user__about">Логопед-дефиктолог, высшая квалификационная категория</div>
                </div>
                <div class="user__body">
                    <div class="user__body-inner">
                        <div class="text-grey text-caption-small mb-10">Обо мне:</div>
                        <p>Консультирование по всем вопросам логопедии и дефектологии. Консультирование по следующим
                            диагнозам: Алалия, брадилалия, дизартрия, дисграфия, дислалия, дислексия, дисфония, задержка
                            речевого развития, задержка психоречевого развития, заикание, общее недоразвитие речи,
                            ринолалия, тахилалия, фонематическое нарушение речи, фонетико-фонематическое недоразвитие
                            речи. Речевые нарушения при: аутизме, ДЦП, задержке психического развития, синдроме дефицита
                            внимания и гиперактивности (СДВГ), синдроме Дауна, умственной отсталости.
                        </p>
                        <div class="text-grey text-caption-small mb-10">Место работы:</div>
                        Руководитель Центра Речи «Каркуша»
                        <div class="mb-20"><a href="#" target="_blank">http://karkusha.su</a></div>
                        <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                        <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slider nav-tabs-slider block-shaded mb-10">
        <div class="slider-wrapper slider-changeable">
                <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100">
                    <li class="slide w-50 active"><a href="#">Консультации (50)</a></li>
                    <li class="slide w-50"><a href="#">Записи(250)</a></li>
                    <li class="slide w-50"><a href="#">Друзья</a></li>
                    <li class="slide w-50"><a href="#">Подписчики</a></li>
                    <li class="slide w-50"><a href="#">Сообщества</a></li>
                    <li class="slide w-50"><a href="#">Альбомы</a></li>
            </ul>
        </div>
      </div>
    <div class="questions mb-30">
        <div class="questions__list">

            <div class="question">
                <div class="question__header">
                    <div class="question__avatar">
                        <img src="img/tmp/userpic1.png">
                    </div>
                    <div class="question__author">Спрашивает <strong>Рада Мельникова</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Гиперлактация</div>
                    <div class="question__text">
                        Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа,
                        последние две ночи вообще по 5 часов
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                </div>
            </div>

            <div class="question">
                <div class="question__header">
                    <div class="question__avatar"></div>
                    <div class="question__author"><strong>Анонимный вопрос</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Ссадина ранка</div>
                    <div class="question__text">
                        ЗДРАВСТВУЙТЕ! ребенок четыре дня назад упал на ручку. На ладошке появился пузырек плотный.
                        Обработали зеленкой.
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                </div>
            </div>

        </div>
    </div>

    <ul class="pagination text-center mb-20">
        <li class="active"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">...</a></li>
    </ul>
</div>
//= ../template/calendar.php