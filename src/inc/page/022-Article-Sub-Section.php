<div class="content content-articles scroll-top pt-20">

	<ul class="page-breadcrumbs">
		<li><a href="#">Статьи</a></li>
		<li><a href="#">Планирование</a></li>
		<li>Подготовка к зачатию</li>
	</ul>
	
	<h1 class="mb-20">Подготовка к зачатию</h1>

	<div class="mb-20">
		<button data-target="#popup-articles" class="btn btn-popup btn-white">Разделы статей</button>
	</div>

	<div class="mb-20">
		<button data-target="#popup-articles" class="btn btn-popup btn-white">Рубрики раздела</button>
	</div>

	<?php include 'inc/block/search.php'; ?>

	<?php $mb = 10; include 'inc/block/tabs.php'; ?>

	<div class="articles mb-40">
		<?php include 'inc/block/articles.php' ?>

		<div class="text-center"><button class="btn btn-active-flat">Все статьи</button></div>
	</div>

</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>