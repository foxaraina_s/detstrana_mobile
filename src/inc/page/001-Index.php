<!-- Articles -->
<h2 class="mb-20">Статьи</h2>

<div class="mb-20">
	<button data-target="#popup-articles" class="btn btn-popup btn-white">Разделы статей</button>
</div>

<div class="articles">
	<?php include 'inc/block/articles.php' ?>

	<div class="text-center"><button class="btn btn-active-flat">Все статьи</button></div>
</div>
<!-- /Articles -->


<!-- News -->
<h2 class="mb-20">Новости</h2>

<div class="mb-20">
	<button data-target="#popup-news" class="btn btn-popup btn-white">Тематики новостей</button>
</div>

<div class="articles mb-40">
	<?php include 'inc/block/news.php' ?>

	<div class="text-center"><a href="#" class="btn btn-active-flat">Все новости</a></div>
</div>
<!-- /News -->


<!-- Sevices -->
<h2 class="mb-20">Сервисы</h2>

<div class="services-icons">
	<div class="services-icons__inner">

		<div class="slider services-icons__slider">
			<div class="slider-changeable slider-wrapper">
				<div class="slides services-icons__slides">

					<div class="slide services-icons__slide">
						<div class="services-icons__icon calc-weight"><a href="#">Расчет веса при беременности</a></div>
						<div class="services-icons__icon babyGender"><a href="#">Определение пола ребёнка</a></div>
						<div class="services-icons__icon names"><a href="#">Подбор имени ребёнку</a></div>
						<div class="services-icons__icon dream"><a href="#">Толкование снов</a></div>
					</div>
					<div class="slide services-icons__slide">
						<div class="services-icons__icon encyclopedia_nutrition"><a href="#">Энциклопедия питания</a></div>
						<div class="services-icons__icon recipies"><a href="#">Сборник рецептов</a></div>
						<div class="services-icons__icon maternity_hospitals_rating"><a href="#">Рейтинг роддомов</a></div>
						<div class="services-icons__icon rating_mult_and_film"><a href="#">Рейтинг книг, кино, мультфильмов</a></div>
					</div>
				</div>

			</div>

			<ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
				<?php foreach(range(1, 2) as $i) { ?>
					<li><a href="#"></a></li>
				<?php } ?>
			</ul>
		</div>	

	</div>
	<div class="text-center"><a href="#" class="btn btn-active-flat">Все сервисы</a></div>
</div>
<!-- /Services -->


<!-- Questions -->
<h2 class="mb-20">Вопросы к экспертам</h2>

<a href="#" class="btn btn-active btn-shaded w-93 block-center mb-20">Задать вопрос экспертам</a>
<a href="#" class="btn btn-active-flat block-center mb-20">Все эксперты</a>


<div class="questions mb-40">

	<?php include 'inc/block/questions.php' ?>
	
	<p class="text-center"><a href="#" class="btn btn-active-flat">Все вопросы</a></p>
</div>
<!-- /Questions -->

<h2 class="mb-20">Лента</h2>

<?php include 'inc/block/entries.php' ?>

<?php include 'inc/block/comments.php' ?>

<?php include 'inc/block/pregnancy-calendar.php' ?>