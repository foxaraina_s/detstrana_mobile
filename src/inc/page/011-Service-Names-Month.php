<div class="service service-names scroll-top">

<?php
	$title = 'Январь&nbsp;&mdash; выбор имени по святцам для ребёнка';

	include 'inc/block/services/names/header.php';

?>

	<form class="service-names__form-choose">
		<div class="form-group mb-20">
			<label>Выберите дату:</label>
			<?php include 'inc/block/date-dm.php '?>


		</div>
	</form>

<?php

	include 'inc/block/services/names/table.php';

	include 'inc/block/services/names/text.php';
?>

</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>