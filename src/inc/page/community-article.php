
<div class="content content-community scroll-top pt-20">
    <ul class="page-breadcrumbs">
        <li><a href="#">Каталог сообществ</a></li>
        <li><a href="#">Планирование и беременность</a></li>
        <li><span>Подготовка к зачатию</span></li>
    </ul>
    <h1 class="mb-20">Подготовка к зачатию</h1>
    <div class="communities mb-20">
        <div class="community-one mb-10">
            <div class="community__image">
                <img src="img/tmp/community1.png">
            </div>
            <div class="community__body">
                <div class="comment__text">
                    Как здорово гулять по парку с коляской, пеленать малыша и с упоением
                    слушать, как он гулит. Скоро все будет! А пока - готовимся и постигаем
                    науку измерения базальной температуры, узнаем, как зачать мальчика, и
                    какие тесты на овуляцию лучше!
                </div>
                <div class="community__buttons">
                    <button class="btn btn-confirm btn-shaded w-46">Создать запись</button>
                    <button class="btn btn-confirm btn-shaded w-46">Вступить</button>
                </div>
                <button class="btn btn-active-flat btn-center">Правила сообщества</button>

            </div>

        </div>
    </div>
    <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-scrollable block-shaded w-100 mb-20">
        <li class="w-50"><a href="#">Записей (71)</a></li>
        <li class="w-50"><a href="#">Участников (10528)</a></li>
    </ul>
    <div class="entries">
        <div class="entries__list">
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author"><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                    <div class="entry__section"><em>Планирование и беременность</em></div>

                </div>

                <div class="entry__title">
                   Забеременеть в Хадассе
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text mb-20">
                        — Можете назвать 3 причины, почему стоит пройти процедуру ЭКО именно в «Хадассе»? Почему именно
                        в «Хадассе»? Во-первых, колоссальный опыт. В 1982 году в «Хадассе» открылось первое в Израиле
                        отделение ЭКО, то есть речь идет о 35-летнем опыте! Во-вторых, у нас действует система, которая
                        поддерживает все процессы, связанные с ЭКО, будь то эндокринология, Банк крови, биохимия и пр. И
                        третья, но далеко не последняя причина — обученная команда опытных специалистов, которая
                        заслуженно считается ведущей в Израиле. — Если кто-нибудь из-за границы захочет пройти процедуру
                        экстракорпорального оплодотворения в Израиле… Да, к нам приезжают. Процедура ЭКО может занять 3
                        – 3,5 недели. Это очень длительный срок пребывания за рубежом. Женщина вместе со своим супругом
                        будет находиться в Израиле: расходы на проживание, питание, кроме того, нужно будет оплатить
                        стоимость процедуры. Не каждый может себе позволить такую роскошь, но мы пытаемся сделать
                        процедуру ЭКО доступной для как можно большего числа людей, если они хотят приехать в «Хадассу».
                        — Расскажите подробнее о самой процедуре. Процедура ЭКО состоит из двух этапов. Первый — женщина
                        проходит гормональное лечение и следит за развитием фолликул и яичников, второй — когда все
                        готово, мы проводим операцию по извлечению яйцеклеток. Согласно разработанному
                    </div>
                    <div class="caption-small mb-20">Теги по теме:</div>
                    <ul class="nav nav-tags mb-20">
                        <li><a href="#">Здоровье</a></li>
                        <li class="active"><a href="#">Роды</a></li>
                    </ul>
                    <div class="icon-group text-center mb-20">
                        <a href="#" class="icon-info icon-info-views">120</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">3</a>
                    </div>
                    <div class="block-subscribe-inner mb-20">
                        <div class="block-subscribe-inner__title">Интересно?</div>
                        <div class="block-subscribe-inner__subtitle">Подпишись на сообщество «Мой дом - моя крепость» и получай другие записи в новостную ленту.</div>
                        <form>

                            <div class="form-group mb-40">
                                <label class="form-label form-label-hidden">Ваш e-mail</label>
                                <input name="email" type="email" placeholder="Введите e-mail" maxlength="20" class="form-control">
                                <div class="form-error">&nbsp;</div>
                            </div>

                            <button class="btn btn-confirm w-100">Подписаться</button>
                        </form>

                    </div>
                    <div class="article-buttons">
                        <a href="#" class="article-buttons__button-rewind">Предыдущая запись</a>
                        <a href="#" class="article-buttons__button-forward">Следующая запись</a>
                    </div>
                    <div class="block-social mb-20">
                        <a href="#" class="icon-fb"></a>
                        <a href="#" class="icon-vk"></a>
                        <a href="#" class="icon-ok"></a>
                    </div>
                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h2>Другие записи сообщества</h2>
    <div class="entries mb-20">
        <div class="entries__list mb-20">
            <div class="entry mb-10">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в сообществе<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
            <div class="entry mb-10">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в сообществе<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
            <div class="entry mb-10">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в сообществе<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>

        </div>

        <div class="text-center"><button class="btn btn-active-flat">Больше записей</button></div>

    </div>




</div>







//= ../template/calendar.php

<script>
    $('.users').on('click', '.user.user-clickable .user__header', function() {
        $(this)
            .parents('.user')
            .eq(0)
            .toggleClass('user-active')
    });
</script>
