
<div class="content content-consulting pt-10 scroll-top">
    <ul class="page-breadcrumbs">
        <li><a href="#">Консультации</a></li>
        <li><span>Логопед</span></li>
    </ul>
    <h1 class="mb-40">Логопед - онлайн-консультация</h1>
    <div class="consulting mb-20">

        <div class="consulting-one mb-10">
            <div class="consulting__image">
                <img src="img/tmp/community1.png">
            </div>
            <div class="consulting__body">
                <div class="comment__text mb-20">
                    Как радостно любому родителю услышать первые «мама» и «папа» из уст малыша!
                    И как важно, чтобы речь ребёнка развивалась, соответствуя его возрастным нормам.
                    Чтобы для переживаний не осталось причин, узнайте об особенностях развития речи у логопеда
                </div>
                <button class="btn btn-confirm btn-shaded w-100 mb-20">Задать вопрос</button>
                <button class="btn btn-active-flat btn-center">Правила и модерация</button>

            </div>

        </div>
    </div>
    <div class="questions mb-10">
        <div class="question-answer">
            <div class="question-answer__header">
                <div class="question-answer__avatar">
                    <img src="img/tmp/userpic1.png">
                </div>
                <div class="question-answer__author">Спрашивает <strong>Рада Мельникова</strong></div>
                <div class="question-answer__date">31 мая 2017 года, 12:56</div>
                <div class="question-answer__status">Консультант по грудному вскармливанию</div>
            </div>
            <div class="question-answer__body">
                <div class="question-answer__title">Гиперлактация</div>
                <div class="question-answer__text">
                    Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа,
                    последние две ночи вообще по 5 часов
                </div>
            </div>
        </div>
        <div class="question-answer mb-20 pb-20">
            <div class="question-answer__header question-answer__header--dark">
                <div class="question-answer__avatar">
                    <img src="img/tmp/userpic1.png">
                </div>
                <div class="question-answer__author">Отвечает <strong>Рада Мельникова</strong></div>
                <div class="question-answer__date">31 мая 2017 года, 12:56</div>
                <div class="question-answer__status">Консультант по грудному вскармливанию</div>
            </div>
            <div class="question-answer__body mb-20">
                <div class="question-answer__text">
                    Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа,
                    последние две ночи вообще по 5 часов перерыв получался. Молока очень много, грудь переполняется и вся в комочках, после
                    корления стараюсь не сцеживать, дабы не провоцировать ещё большую лактацию, но получается, что я почти при каждом кормлении
                    сцеживаю другую грудь до состояния облегчения, а это около 40-60мл. Во время кормления происходит 3-5 приливов.
                    Подскажите пожалуйста, не провоцирую ли я лактацию, можно ли так часто цедить грудь? Получается каждые 3-4 часа я одной кормлю, другую цежу и так при каждом кормлении.
                </div>
            </div>
            <a href="#" class="btn btn-confirm-flat block-center">Комментировать</a>
        </div>
        <div class="text-center mb-20"><button class="btn btn-active-flat">Все вопросы</button></div>
        <div class="consulting-buttons">
            <a href="#" class="consulting-buttons__button-rewind">Предыдущая консультация</a>
            <a href="#" class="consulting-buttons__button-forward">Следующая консультация</a>
        </div>
    </div>
        </div>
<h2 class="mb-20">Популярные консультации</h2>

    <div class="questions mb-20">
        <div class="questions__list">


            <div class="question">
                <div class="question__header">
                    <div class="question__avatar">
                        <img src="img/tmp/userpic1.png">
                    </div>
                    <div class="question__author">Спрашивает <strong>Рада Мельникова</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Гиперлактация</div>
                    <div class="question__text">
                        Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа,
                        последние две ночи вообще по 5 часов
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                </div>
            </div>

            <div class="question">
                <div class="question__header">
                    <div class="question__avatar"></div>
                    <div class="question__author"><strong>Анонимный вопрос</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Ссадина ранка</div>
                    <div class="question__text">
                        ЗДРАВСТВУЙТЕ! ребенок четыре дня назад упал на ручку. На ладошке появился пузырек плотный. Обработали зеленкой.
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                </div>
            </div>

        </div>	</div>

//= ../template/calendar.php