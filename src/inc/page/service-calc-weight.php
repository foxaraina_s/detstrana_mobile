
<section class="service service-birthcalendar pt-20 scroll-top">
    <ul class="page-breadcrumbs">
        <li><a href="#">Сервисы</a></li>
        <li><span>Калькулятор расчет веса для беременных</span></li>
    </ul>
    <h1 class="mb-20">Калькулятор расчета веса при беременности</h1>
    <div class="service__text">Чтобы рассчитать норму прибавки по неделям, укажите свой вес, рост и срок беременности</div>
    <div class="weight-form">
        <form method="post">
            <div class="weight-form__week">
                <div class="weight-form__title">Неделя беременности:</div>
                <div class="form-group mb-20">
                    <select name="wp" placeholder="Неделя" class="form-select">
                        <option>Неделя</option>
                        <option value="0">1 неделя</option>
                        <option value="1">2 неделя</option>
                        <option value="2">3 неделя</option>
                        <option value="3">4 неделя</option>
                        <option value="4">5 неделя</option>
                        <option value="5">6 неделя</option>
                        <option value="6">7 неделя</option>
                        <option value="7">8 неделя</option>
                        <option value="8">9 неделя</option>
                        <option value="9">10 неделя</option>
                        <option value="10">11 неделя</option>
                        <option value="11">12 неделя</option>
                        <option value="12">13 неделя</option>
                        <option value="13">14 неделя</option>
                        <option value="14">15 неделя</option>
                        <option value="15">16 неделя</option>
                        <option value="16">17 неделя</option>
                        <option value="17">18 неделя</option>
                        <option value="18">19 неделя</option>
                        <option value="19">20 неделя</option>
                        <option value="20">21 неделя</option>
                        <option value="21">22 неделя</option>
                        <option value="22">23 неделя</option>
                        <option value="23">24 неделя</option>
                        <option value="24">25 неделя</option>
                        <option value="25">26 неделя</option>
                        <option value="26">27 неделя</option>
                        <option value="27">28 неделя</option>
                        <option value="28">29 неделя</option>
                        <option value="29">30 неделя</option>
                        <option value="30">31 неделя</option>
                        <option value="31">32 неделя</option>
                        <option value="32">33 неделя</option>
                        <option value="33">34 неделя</option>
                        <option value="34">35 неделя</option>
                        <option value="35">36 неделя</option>
                        <option value="36">37 неделя</option>
                        <option value="37">38 неделя</option>
                        <option value="38">39 неделя</option>
                        <option value="39">40 неделя</option>
                    </select>
                </div>

            </div>
            <div class="weight-form__growth">
                <div class="weight-form__title">Рост (см):</div>
                <div class="form-group mb-20">
                    <select name="wp" placeholder="см" class="form-select">
                        <option>см</option>
                    </select>
                </div>

            </div>
            <div class="weight-form__weight-b">
                <div class="weight-form__title">Вес до берем-сти (кг):</div>
                <div class="form-group mb-20">
                    <select name="wp" placeholder="кг" class="form-select">
                        <option>кг</option>
                    </select>
                </div>

            </div>
            <div class="weight-form__weight-a">
                <div class="weight-form__title">Вес сейчас (кг):</div>
                <div class="form-group mb-20">
                    <select name="wp" placeholder="кг" class="form-select">
                        <option>кг</option>
                    </select>
                </div>

            </div>
            <a class="btn btn-confirm btn-shaded w-100">Рассчитать</a>
        </form>
    </div>
    <div class="inner-block inner-block--shadow mb-20">
        <div class="inner-block__doctor">
            <div class="inner-block__doctor-image">
                <img src="img/birthcalendar/dc1.png" style="width: 64px;">
            </div>
            <div class="inner-block__doctor-content">
                <div class="inner-block__doctor-title">
                    Коцарев Евгений Александрович
                </div>
                <div class="inner-block__doctor-text">
                    Акушер-гинеколог, гинеколог-эндокринолог Центр иммунологии и репродукции
                </div>
            </div>
        </div>
    </div>
    <div class="accordion">
        <div class="accordion__item">
            <div class="accordion__title">
               Вес плода (ребенка) при беременности
            </div>
            <div class="accordion__content">
               <div class="service__text">
                   Иногда девушки во время беременности едят «за двоих» и, в результате, набирают лишний вес.
                   Врачи-гинекологи в одни голос твердят – лишний вес вреден, питание должно быть правильным, а продукты
                   – полезными. Однако при традиционном взвешивании килограммы растут. И это вовсе не потому, что
                   будущий малыш активно прибавляет в весе. Это лишний вес мамы, который является ненужным балластом,
                   который, к тому же, ещё и опасен. Tак не поправиться при беременности? И чем опасен лишний
                   <div class="inner-list">
                       <ul>
                          <li> возрастает вероятность развития сердечно-сосудистых заболеваний;</li>
                          <li> появляется риск нарушения и функций эндокринной системы;</li>
                           <li>значительно возрастает и нагрузка, оказываемая на позвоночник;</li>
                           <li>увеличивается нагрузка на внутренние органы;</li>
                           <li> есть риск варикозного расширения вен;</li>
                           <li>повышается вероятность позднего токсикоза.</li>
                           <p>Лишний вес при беременности может привести к трудностям
                           при родах. Например, приходится делать кесарево сечение, так как естественные роды при избыточно
                               массе тела опасны для жизни мамы и малыша.</p>
                           <p>А ещё лишний вес при беременности – досадный фактор,
                               который мешает девушкам быстро сбрасывать его после рождения малыша.</p>
                       </ul>
                   </div>

               </div>
            </div>
        </div>
        <div class="accordion__item">
            <div class="accordion__title">
                Чем опасен лишний вес при беременности
            </div>
            <div class="accordion__content">
                <div class="service__text">
                    Иногда девушки во время беременности едят «за двоих» и, в результате, набирают лишний вес.
                    Врачи-гинекологи в одни голос твердят – лишний вес вреден, питание должно быть правильным, а продукты
                    – полезными. Однако при традиционном взвешивании килограммы растут. И это вовсе не потому, что
                    будущий малыш активно прибавляет в весе. Это лишний вес мамы, который является ненужным балластом,
                    который, к тому же, ещё и опасен. Tак не поправиться при беременности? И чем опасен лишний
                    <div class="inner-list">
                        <ul>
                            <li> возрастает вероятность развития сердечно-сосудистых заболеваний;</li>
                            <li> появляется риск нарушения и функций эндокринной системы;</li>
                            <li>значительно возрастает и нагрузка, оказываемая на позвоночник;</li>
                            <li>увеличивается нагрузка на внутренние органы;</li>
                            <li> есть риск варикозного расширения вен;</li>
                            <li>повышается вероятность позднего токсикоза.</li>
                            <p>Лишний вес при беременности может привести к трудностям
                                при родах. Например, приходится делать кесарево сечение, так как естественные роды при избыточно
                                массе тела опасны для жизни мамы и малыша.</p>
                            <p>А ещё лишний вес при беременности – досадный фактор,
                                который мешает девушкам быстро сбрасывать его после рождения малыша.</p>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <div class="accordion__item">
            <div class="accordion__title">
                Как не набрать вес во время беременности
            </div>
            <div class="accordion__content mt-20">
                <div class="service__text">
                    Иногда девушки во время беременности едят «за двоих» и, в результате, набирают лишний вес.
                    Врачи-гинекологи в одни голос твердят – лишний вес вреден, питание должно быть правильным, а продукты
                    – полезными. Однако при традиционном взвешивании килограммы растут. И это вовсе не потому, что
                    будущий малыш активно прибавляет в весе. Это лишний вес мамы, который является ненужным балластом,
                    который, к тому же, ещё и опасен. Tак не поправиться при беременности? И чем опасен лишний
                    <div class="inner-list">
                        <ul>
                            <li> возрастает вероятность развития сердечно-сосудистых заболеваний;</li>
                            <li> появляется риск нарушения и функций эндокринной системы;</li>
                            <li>значительно возрастает и нагрузка, оказываемая на позвоночник;</li>
                            <li>увеличивается нагрузка на внутренние органы;</li>
                            <li> есть риск варикозного расширения вен;</li>
                            <li>повышается вероятность позднего токсикоза.</li>
                            <p>Лишний вес при беременности может привести к трудностям
                                при родах. Например, приходится делать кесарево сечение, так как естественные роды при избыточно
                                массе тела опасны для жизни мамы и малыша.</p>
                            <p>А ещё лишний вес при беременности – досадный фактор,
                                который мешает девушкам быстро сбрасывать его после рождения малыша.</p>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <div class="accordion__item">
            <div class="accordion__title">
                Норма прибавки веса при беременности по неделям
            </div>
            <div class="accordion__content mt-20">
                <div class="service__text">
                    Иногда девушки во время беременности едят «за двоих» и, в результате, набирают лишний вес.
                    Врачи-гинекологи в одни голос твердят – лишний вес вреден, питание должно быть правильным, а продукты
                    – полезными. Однако при традиционном взвешивании килограммы растут. И это вовсе не потому, что
                    будущий малыш активно прибавляет в весе. Это лишний вес мамы, который является ненужным балластом,
                    который, к тому же, ещё и опасен. Tак не поправиться при беременности? И чем опасен лишний
                    <div class="inner-list">
                        <ul>
                            <li> возрастает вероятность развития сердечно-сосудистых заболеваний;</li>
                            <li> появляется риск нарушения и функций эндокринной системы;</li>
                            <li>значительно возрастает и нагрузка, оказываемая на позвоночник;</li>
                            <li>увеличивается нагрузка на внутренние органы;</li>
                            <li> есть риск варикозного расширения вен;</li>
                            <li>повышается вероятность позднего токсикоза.</li>
                            <p>Лишний вес при беременности может привести к трудностям
                                при родах. Например, приходится делать кесарево сечение, так как естественные роды при избыточно
                                массе тела опасны для жизни мамы и малыша.</p>
                            <p>А ещё лишний вес при беременности – досадный фактор,
                                который мешает девушкам быстро сбрасывать его после рождения малыша.</p>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <div class="accordion__item">
            <div class="accordion__title">
                Таблица прибавки веса при беременности
            </div>
            <div class="accordion__content mt-20">
                <div class="service__text">
                    Иногда девушки во время беременности едят «за двоих» и, в результате, набирают лишний вес.
                    Врачи-гинекологи в одни голос твердят – лишний вес вреден, питание должно быть правильным, а продукты
                    – полезными. Однако при традиционном взвешивании килограммы растут. И это вовсе не потому, что
                    будущий малыш активно прибавляет в весе. Это лишний вес мамы, который является ненужным балластом,
                    который, к тому же, ещё и опасен. Tак не поправиться при беременности? И чем опасен лишний
                    <div class="inner-list">
                        <ul>
                            <li> возрастает вероятность развития сердечно-сосудистых заболеваний;</li>
                            <li> появляется риск нарушения и функций эндокринной системы;</li>
                            <li>значительно возрастает и нагрузка, оказываемая на позвоночник;</li>
                            <li>увеличивается нагрузка на внутренние органы;</li>
                            <li> есть риск варикозного расширения вен;</li>
                            <li>повышается вероятность позднего токсикоза.</li>
                            <p>Лишний вес при беременности может привести к трудностям
                                при родах. Например, приходится делать кесарево сечение, так как естественные роды при избыточно
                                массе тела опасны для жизни мамы и малыша.</p>
                            <p>А ещё лишний вес при беременности – досадный фактор,
                                который мешает девушкам быстро сбрасывать его после рождения малыша.</p>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <div class="accordion__item">
            <div class="accordion__title">
                Диета при беременности для снижения веса
            </div>
            <div class="accordion__content mt-20">
                <div class="service__text">
                    Иногда девушки во время беременности едят «за двоих» и, в результате, набирают лишний вес.
                    Врачи-гинекологи в одни голос твердят – лишний вес вреден, питание должно быть правильным, а продукты
                    – полезными. Однако при традиционном взвешивании килограммы растут. И это вовсе не потому, что
                    будущий малыш активно прибавляет в весе. Это лишний вес мамы, который является ненужным балластом,
                    который, к тому же, ещё и опасен. Tак не поправиться при беременности? И чем опасен лишний
                    <div class="inner-list">
                        <ul>
                            <li> возрастает вероятность развития сердечно-сосудистых заболеваний;</li>
                            <li> появляется риск нарушения и функций эндокринной системы;</li>
                            <li>значительно возрастает и нагрузка, оказываемая на позвоночник;</li>
                            <li>увеличивается нагрузка на внутренние органы;</li>
                            <li> есть риск варикозного расширения вен;</li>
                            <li>повышается вероятность позднего токсикоза.</li>
                            <p>Лишний вес при беременности может привести к трудностям
                                при родах. Например, приходится делать кесарево сечение, так как естественные роды при избыточно
                                массе тела опасны для жизни мамы и малыша.</p>
                            <p>А ещё лишний вес при беременности – досадный фактор,
                                который мешает девушкам быстро сбрасывать его после рождения малыша.</p>
                        </ul>
                    </div>

                </div>
            </div>
        </div>




    </div>
    <h2 class="mb-20 mlr-16">Календарь анализов и обследований при беременности</h2>
    <h3>Расчет прибавки веса по неделям беременности:</h3>
    <div class="analysis-calendar mb-40">
        <div class="analysis-calendar__item">
            <div class="analysis-calendar__header">
                <div class="analysis-calendar__title">
                    I триместр
                </div>
            </div>
            <div class="analysis-calendar__body">
                <div class="slider analysis-calendar-slider mb-20">
                    <div class="slider-wrapper slider-changeable">
                        <ul class="nav calendar-analysis-nav analysis-calendar-list analysis-calendar-bordered analysis-calendar-scrollable analysis-calendar__first w-100">
                            <li class="slide"><a href="#">1</a></li>
                            <li class="slide"><a href="#">2</a></li>
                            <li class="slide"><a href="#">3</a></li>
                            <li class="slide"><a href="#">4</a></li>
                            <li class="slide"><a href="#">5</a></li>
                            <li class="slide"><a href="#">6</a></li>
                            <li class="slide"><a href="#">7</a></li>
                            <li class="slide"><a href="#">8</a></li>
                            <li class="slide"><a href="#">9</a></li>
                            <li class="slide"><a href="#">10</a></li>
                            <li class="slide"><a href="#">11</a></li>
                            <li class="slide"><a href="#">12</a></li>
                            <li class="slide"><a href="#">13</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="analysis-calendar__item">
            <div class="analysis-calendar__header">
                <div class="analysis-calendar__title">
                    II триместр
                </div>
            </div>
            <div class="analysis-calendar__body">
                <div class="slider analysis-calendar-slider mb-20">
                    <div class="slider-wrapper slider-changeable">
                        <ul class="nav calendar-analysis-nav analysis-calendar-list analysis-calendar-bordered analysis-calendar-scrollable analysis-calendar__second w-100">
                            <li class="slide"><a href="#">14</a></li>
                            <li class="slide"><a href="#">15</a></li>
                            <li class="slide"><a href="#">16</a></li>
                            <li class="slide"><a href="#">17</a></li>
                            <li class="slide"><a href="#">18</a></li>
                            <li class="slide"><a href="#">19</a></li>
                            <li class="slide"><a href="#">20</a></li>
                            <li class="slide"><a href="#">21</a></li>
                            <li class="slide"><a href="#">22</a></li>
                            <li class="slide"><a href="#">23</a></li>
                            <li class="slide"><a href="#">24</a></li>
                            <li class="slide"><a href="#">25</a></li>
                            <li class="slide"><a href="#">26</a></li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="analysis-calendar__item">
            <div class="analysis-calendar__header">
                <div class="analysis-calendar__title">
                    III триместр
                </div>
            </div>
            <div class="analysis-calendar__body">
                <div class="slider analysis-calendar-slider mb-20">
                    <div class="slider-wrapper slider-changeable">
                        <ul class="nav calendar-analysis-nav analysis-calendar-list analysis-calendar-bordered analysis-calendar-scrollable analysis-calendar__third w-100">
                            <li class="slide"><a href="#">27</a></li>
                            <li class="slide"><a href="#">28</a></li>
                            <li class="slide"><a href="#">29</a></li>
                            <li class="slide"><a href="#">30</a></li>
                            <li class="slide"><a href="#">31</a></li>
                            <li class="slide active"><a href="#">32</a></li>
                            <li class="slide"><a href="#">33</a></li>
                            <li class="slide"><a href="#">34</a></li>
                            <li class="slide"><a href="#">35</a></li>
                            <li class="slide"><a href="#">36</a></li>
                            <li class="slide"><a href="#">37</a></li>
                            <li class="slide"><a href="#">38</a></li>
                            <li class="slide"><a href="#">39</a></li>
                            <li class="slide"><a href="#">40</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
//= ../template/calendar.php