<div class="service service-names scroll-top">
<?php
	$title = 'Редкие имена для детей';

	include 'inc/block/services/names/header.php';

	include 'inc/block/services/names/switch-filter.php';

	include 'inc/block/services/names/years.php';

	include 'inc/block/services/names/table.php';

	include 'inc/block/services/names/text.php';
?>
</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>