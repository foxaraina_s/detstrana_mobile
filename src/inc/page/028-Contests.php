<div class="content content-user-articles scroll-top pt-10">
	
	<h1 class="mb-20">Конкурсы</h1>

	<div class="mlr-16 mb-20">
		Каждый этап беременности и родительства может приносить не только радость, но и пользу. Участвуйте в тематических конкурсах, делитесь фотографиями, историями, оставляйте комментарии и голосуйте за лучшие работы. Вас ждут ценные призы, подарки и хорошее настроение!		
	</div>

	<h2 class="mb-20">Текущие конкурсы</h2>

	<div class="contests">
		<div class="contests__list">
			<div class="contest mb-10">
				<div class="contest__status">Прием заявок до 4 октября</div>
				<div class="contest__category">Ваш супер-помощник на кухне</div>
				<div class="contest__image"><img src="img/tmp/contest1.jpg"></div>
				<div class="contest__title">Блендер-пароварка Beaba Babycook</div>
				<div class="contest__description mb-20">Готовьте для малыша легко, полезно и вкусно с блендером-пароваркой Beaba Babycook!</div>
				<a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Условия конкурса</a>
				<div class="text-center"><span class="icon-info icon-participants">246</span></div>
			</div>

			<div class="contest mb-10">
				<div class="contest__status">Победители определены</div>
				<div class="contest__category">С заботой о малышах</div>
				<div class="contest__image"><img src="img/tmp/contest1.jpg"></div>
				<div class="contest__title">Подгузники и трусики Pampers</div>
				<div class="contest__description mb-20">Ваш кроха будет чувствовать себя комфортно в подгузниках Рampers</div>
				<a href="#" class="btn btn-active-flat block-center">Условия конкурса</a>
			</div>
		</div>
	</div>


	<h2 class="mb-20">Прошлые конкурсы</h2>

	<div class="contests mb-20">
		<div class="contests__list">
			<div class="contest mb-10">
				<div class="contest__closed">Конкурс завершен</div>
				<div class="contest__category">Уборка на «отлично»</div>
				<div class="contest__image"><img src="img/tmp/contest3.jpg"></div>
				<div class="contest__title">Проведите время с удовольствием, а уборку доверьте роботу-пылесосу Clever&amp;Clean</div>
				<div class="contest__description mb-20">Проводите время с семьёй, а уборку доверьте роботу-пылесосу Clever&amp;Clean</div>
				<a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Результаты</a>
				<div class="text-center"><span class="icon-info icon-participants">246</span></div>
			</div>
		</div>
	</div>

	<?php include 'inc/block/pagination.php'; ?>

</div>

<?php include 'inc/block/pregnancy-calendar.php'; ?>