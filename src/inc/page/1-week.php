
<div id="popup-month" class="popup popup-services popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Выберите месяц</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Месяц 1</a></li>
            <li><a href="#">Месяц 2</a></li>
            <li><a href="#">Месяц 3</a></li>
            <li><a href="#">Месяц 4</a></li>
            <li><a href="#">Месяц 5</a></li>
            <li><a href="#">Месяц 6</a></li>
            <li><a href="#">Месяц 7</a></li>
            <li><a href="#">Месяц 8</a></li>
            <li><a href="#">Месяц 9</a></li>
        </ul>
    </div>
</div>
<div id="popup-content" class="popup popup-services popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Содержание</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Раздел 1</a></li>
            <li><a href="#">Раздел 2</a></li>
            <li><a href="#">Раздел 3</a></li>
            <li><a href="#">Раздел 4</a></li>
            <li><a href="#">Раздел 5</a></li>
            <li><a href="#">Раздел 6</a></li>
            <li><a href="#">Раздел 7</a></li>
            <li><a href="#">Раздел 8</a></li>
            <li><a href="#">Раздел 9</a></li>
        </ul>
    </div>
</div>
<section class="service service-birthcalendar pt-20 scroll-top">
    <ul class="page-breadcrumbs">
        <li class="back"><a href="#">Вернуться в календарь беременности</a></li>
    </ul>
    <h1 class="mb-40">1 неделя беременности</h1>
    <div class="mb-20">
        <button data-target="#popup-month" class="btn btn-popup btn-white">Выберите месяц</button>
    </div>
    <div class="mb-20">
        <button data-target="#popup-content" class="btn btn-popup btn-white">Содержание</button>
    </div>
    <div class="inner-content mb-20">
        <div class="service__text">
            <p>
            Первая неделя беременности — это самое начало длинного, но очень интересного пути. Впереди девять месяцев,
            которые пролетят, как одно мгновение, и о которых девушка, став самой настоящей мамой, будет вспоминать
            только тёплое, хорошее и нежное. А этого — уж поверьте — будет действительно много. Не верьте тому, кто
            говорит, что выносить малыша сложно. Это прекрасное время, и им нужно наслаждаться. Беременность — не
            болезнь. Это стоит уяснить с самого начала...
            </p>
            <p>
                Итак, первая неделя беременности — старт первого месяца и одновременно первого триместра. Это как раз
                тот момент, когда вы «немножко беременны». <a href="#">Овуляция уже произошла? Или ещё нет?</a> Этого пока не знает
                никто. Как правило, овуляция наступает где-то на 12-14 день цикла. Однако у каждой представительницы
                прекрасного пола этот момент индивидуален. Яйцеклетка может выйти навстречу своему единственному
                сперматозоиду на седьмой и даже на двадцать первый день цикла. Теперь вы понимаете, почему к первой
                неделе определение «немножко беременна» применимо, как ни к какой другой?
            </p>
        </div>
        <div class="img__block text-center mb-20">
            <img src="img/birthcalendar/in3.jpg" class="img-responsive">
        </div>
        <div class="service__text">
            <p>Окружность вашего животика пока не изменилась в большую сторону, равно как и вес тела. Всё пока остаётся на своих местах, в пределах нормы. Но ваш организм, по всей видимости, уже активно готовится к вынашиванию крохи. У вас есть время подготовиться к этому мысленно, попытаться понять, что совсем скоро жизнь кардинально изменится, открыв вам новые горизонты счастья.
            </p>
        </div>
        <div class="inner-block inner-block--shadow mb-20">
            <h3 class="mb-30 service__section-title">Вам будет интересно прочитать:</h3>
            <div class="simple-list">
              <ul>
                  <li><a href="#">Как протекает первая неделя беременности</a></li>
                  <li><a href="#">Что можно и нельзя делать в первые недели беременности</a></li>
                  <li><a href="#">Как питаться на первых неделях беременности</a></li>
              </ul>
            </div>
        </div>
    </div>

    <div class="slider nav-tabs-slider mb-20 block-shaded">
        <div class="slider-wrapper slider-changeable">
            <ul class="nav nav-tabs nav-tabs-font--sm nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100"
                style="white-space: nowrap;">
                <li class="slide w-50 active"><a href="#">Твой малыш</a></li>
                <li class="slide w-50"><a href="#">Ты </a></li>
                <li class="slide w-50"><a href="#">Что ты делаешь?</a></li>
                <li class="slide w-50"><a href="#">Что ты думаешь?</a></li>
            </ul>
        </div>
    </div>
    <div class="nav-tabs-container mb-40">
        <div class="service__text mb-20">
            То, как выглядит эмбрион в первом триместре, всецело зависит от того, о какой неделе идёт речь. К примеру, на
            первой акушерской неделе беременности эмбриона ещё и вовсе не существует, ведь зачатия не произошло. После
            слияния яйцеклетки и сперматозоида образовывается зигота, которая начинает движение от места встречи женской и
            мужской половой клетки к матке. Приблизительно на пятые сутки с момента оплодотворения эмбрион попадёт в полость
            матки и начнёт своё активное развитие. У него начнут формироваться органы и системы, и очень важно, чтобы
            закладка их была максимально правильной и своевременной. Пока что ваш малыш — песчинка в море. И лишь к концу
            первого триместра эмбриончик будет весить 13 граммов и достигнет роста 61 миллиметр.
        </div>
        <h3 class="mb-20 service__section-title">Определение пола</h3>
        <div class="service__text">
            Пока что определить, каким будет пол малыша, невозможно. Однако вы должны знать, что Природа уже всё расставила
            по своим местам. Если на встречу с яйцеклеткой быстрее всех успеет сперматозоид с хромосомой Х, то у вас через 9
            месяцев родится девочка. Если же более прытким и проворным будет сперматозоид с хромосомой Y, то вы станете
            мамой мальчика. Но не исключено, что у вас родится два однополых или два (а то и больше) разнополых малыша.
            Хотите попытаться определить пол будущего ребёночка по дате зачатия? <a href="#">Следуйте изложенным в статье рекомендациям.</a>
        </div>
    </div>
    <h3 class="mb-20 service__section-title">На 1 триместре беременности полезно:</h3>
    <ul class="nav nav-tabs nav-tabs-bordered w-100 mb-10 nav-tabs-font--sm">
        <li class="w-50 active"><a href="#">Читать блоги</a></li>
        <li class="w-50"><a href="#">Читать консультации</a></li>
    </ul>
    <div class="entries mb-40">
        <div class="entries__list mb-20">
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:58</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                            <div class="slide active"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li class="active"><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">120</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">1</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open entry__comment-open--green ">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="text-center">
            <button class="btn btn-active-flat">Больше записей</button>
        </div>
    </div>
</section>
//= ../template/calendar.php


