<div class="content content-users scroll-top pt-20">
    <ul class="page-breadcrumbs">
        <li><a href="#">Сервисы</a></li>
        <li><span>Определение пола</span></li>
    </ul>
    <div class="users">
        <div class="users__list mb-20">
            <div class="user user-online user-active">
                <div class="user__header clickable">
                    <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="user__name">Рада Мельникова</div>
                    <div class="user__about">
                        День рождения: 24 сентября<br>
                        Город: Сургут
                    </div>
                </div>
                <div class="user__body">
                    <div class="user__body-inner">
                        <div class="text-grey text-caption-small mb-10">Дети:</div>
                        <div class="user__children">
                            <a href="#" class="user__child user__child--girl">9 лет</a>
                            <a href="#" class="user__child user__child--boy">3 мес.</a>
                        </div>
                        <div class="user__about mb-20">
                            <div class="text-grey text-caption-small mb-10">Обо мне:</div>
                            <div class="user__about--text">Счастливая мама двух прекрасных сыночков.</div>
                        </div>
                        <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                        <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slider nav-tabs-slider mb-20 block-shaded">
        <div class="slider-wrapper slider-changeable">
            <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100">
                <li class="slide w-50 active"><a href="#">Записи (250)</a></li>
                <li class="slide w-50"><a href="#">Друзья (315)</a></li>
                <li class="slide w-50"><a href="#">Подписчики</a></li>
                <li class="slide w-50"><a href="#">Сообщества</a></li>
                <li class="slide w-50"><a href="#">Альбомы</a></li>
            </ul>
        </div>
    </div>
    <form class="form-search mb-20">
        <input type="text" name="city" value="" placeholder="Поиск записей в блоге">
    </form>
    <button data-target="#popup-user-filter" class="btn btn-popup btn-white mb-40">Показать фильтр</button>
    <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-scrollable block-shaded w-100">
        <li class="w-50 active"><a href="#">Записи в дневнике</a></li>
        <li class="w-50"><a href="#">Записи из сообществ</a></li>
    </ul>
    <div class="entries">
        <div class="entries__list">
                <div class="entry__no-note mb-20">
                    Ксения Cидорова еще не написала ни одной записи.
                </div>
        </div>


    </div>



</div>

//= ../template/calendar.php