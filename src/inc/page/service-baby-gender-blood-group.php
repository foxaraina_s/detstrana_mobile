
<section class="service service-gender scroll-top">
    <div class="service__subtitle"><a href="#">Сервисы</a> <i>❯</i> Определение пола</div>
    <h2 class="service__title">Определение пола ребенка<br> по группе крови</h2>
    <div class="service__text">Чтобы узнать пол ребенка, укажите группу крови родителей</div>
    <div class="gender__method--wrapper gender-method-group-blood">
        <div class="gender__method--form">
            <div class="gender-form-father gender-form">
                <div class="gender__section-title">Группа крови папы:</div>
                <div class="group-blood-block mb-20 blood_group_links dad">
                    <a href="#" class="blood__group--btn blood_group_link active" data-val="1">I</a>
                    <a href="#" class="blood__group--btn blood_group_link" data-val="2">II</a>
                    <a href="#" class="blood__group--btn blood_group_link" data-val="3">III</a>
                    <a href="#" class="blood__group--btn blood_group_link" data-val="4">IV</a>

                </div>
            </div>
            <div class="mb-40 gender-form-mom gender-form mb-40">
               <div class="gender__section-title">Группа крови мамы:</div>
               <div class="group-bloud-block mb-20 blood_group_links mother">
                   <a href="#" class="blood__group--btn blood_group_link active" data-val="1">I</a>
                   <a href="#" class="blood__group--btn blood_group_link" data-val="2">II</a>
                   <a href="#" class="blood__group--btn blood_group_link" data-val="3">III</a>
                   <a href="#" class="blood__group--btn blood_group_link" data-val="4">IV</a>

               </div>
           </div>
            <div class="text-center btn-wrapper">
                <button class="btn btn-confirm btn-shaded w-93 baby_gender_button blood_group">Узнать пол</button>
            </div>
        </div>
        <div id="result">
            <div class="blood_group_result baby-gender-result">
                <div class="blood_group_result_content baby-gender-result__inner">
                    <div>ПОЗДРАВЛЯЕМ!</div><div>У ВАС, СКОРЕЕ ВСЕГО, БУДЕТ <span id=bgr_gender>девочка</span></div>
                </div>
            </div>
            <div class="icon-group text-center">
                <a href="#" class="icon-rounded icon-fb"></a>
                <a href="#" class="icon-rounded icon-vk"></a>
                <a href="#" class="icon-rounded icon-ok"></a>
            </div>
            <h3 class="gender__subtitle">Другие способы определения пола ребенка</h3>
            <div class="gender__choose">
                <div class="gender__choose--methods">
                    <a href="service-baby-gender-japan.html" class="method--item japanese__table">
                        Японский метод
                    </a>
                    <a href="service-baby-gender-chinise.html" class="method--item chinese_table">
                        Китайский метод
                    </a>
                    <a href="service-baby-gender--blood-refresh.html" class="method--item blood__refresh">
                        По обновлению крови
                    </a>
                </div>
            </div>
        </div>
            //= ../template/inner_comment.php
    </div>
    //= ../template/comment-block.php
    <p class="text-center"><button class="btn btn-active-flat">Больше записей</button></p>
    <div class="service__text">Определение пола ребенка по обновлению крови родителей считается одним из самых
        популярных методов. Он основывается на данных о будущих родителях. А именно, на периодах обновления крови у папы
        и у мамы. Считается, что у девушек оно проходит 1 раз за 3 года, а у мужчин – 1 раз в четыре года. <br>
        Так, когда вы пользуетесь калькулятором, определяющим пол будущего ребенка по обновлению крови, нужно знать: чья кровь была
        более «свежая» на момент зачатия, такого пола и будет кроха. <br>
        При расчете пола ребенка по обновлению крови также надо учитывать следующее: если вам делали переливание, то расчет будет вестись от даты последнего переливания
        крови. И еще: результат может быть искаженным, если пол ребенка по обновлению крови родителей вы рассчитали без
        учета недавних операций или различных травм с кровотечениями. Если метод определения пола по обновлению крови
        онлайн кажется вам не совсем достоверным, воспользуйтесь другими видами тестов.
    </div>
</section>
//= ../template/calendar.php
<script src="js/gender_js.js?<?php echo time() ?>"></script>

