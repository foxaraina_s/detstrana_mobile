
<div class="content content-consulting pt-10 scroll-top">
    <ul class="page-breadcrumbs">
        <li><a href="#">Консультации</a></li>
        <li><span>Логопед</span></li>
    </ul>
    <h1 class="mb-40">Логопед - онлайн-консультация</h1>
    <div class="consulting mb-20">

            <div class="consulting-one mb-10">
                <div class="consulting__image">
                    <img src="img/tmp/community1.png">
                </div>
                <div class="consulting__body">
                    <div class="comment__text mb-20">
                        Как радостно любому родителю услышать первые «мама» и «папа» из уст малыша!
                        И как важно, чтобы речь ребёнка развивалась, соответствуя его возрастным нормам.
                        Чтобы для переживаний не осталось причин, узнайте об особенностях развития речи у логопеда
                    </div>
                    <button class="btn btn-confirm btn-shaded w-100 mb-20">Задать вопрос</button>
                    <button class="btn btn-active-flat btn-center">Правила и модерация</button>

                </div>

            </div>
    </div>
    <div class="nav-tabs-container slides"><ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100 mb-20">
        <li class="w-50 active"><a href="#">Консультаций (71)</a></li>
        <li class="w-50"><a href="#">Экспертов (8)</a></li>
    </ul><div class="border"></div></div>
    <div class="search mb-20">
        <form class="form-search">
            <input type="text" name="q" placeholder="Поиск по вопросам">
        </form>
    </div>

    <div class="nav-tabs-container slides"><ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100 mb-20">
        <li class="w-50 active"><a href="#">Новое</a></li>
        <li class="w-50"><a href="#">Популярное</a></li>
    </ul><div class="border"></div></div>


    <div class="questions mb-40">
        <div class="questions__list">

            <div class="question">
                <div class="question__header">
                    <div class="question__avatar">
                        <img src="img/tmp/userpic1.png">
                    </div>
                    <div class="question__author">Спрашивает <strong>Рада Мельникова</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Гиперлактация</div>
                    <div class="question__text mb-20">
                       <p> Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа,
                           последние две ночи вообще по 5 часов </p>
                        <a href="#" class="question__readmore">Читать далее..</a>
                    </div>
                    <div class="question__text mb-20">
                        Ответил ведущий врач-невролог, врач высшей врачебной квалификационной категории <a href="#">Любовь Максимова</a>
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                    <a href="#" class="btn btn-confirm-flat block-center">Комментировать</a>
                </div>
            </div>

            <div class="question">
                <div class="question__header">
                    <div class="question__avatar"></div>
                    <div class="question__author"><strong>Анонимный вопрос</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Ссадина ранка</div>
                    <div class="question__text">
                        <p>ЗДРАВСТВУЙТЕ! ребенок четыре дня назад упал на ручку. На ладошке появился пузырек плотный. Обработали зеленкой.</p>
                        <a href="#" class="question__readmore">Читать далее..</a>
                    </div>
                    <div class="question__text mb-20">
                        Ответил ведущий врач-невролог, врач высшей врачебной квалификационной категории <a href="#">Любовь Максимова</a>
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                    <a href="#" class="btn btn-confirm-flat block-center">Комментировать</a>
                </div>
            </div>

        </div>	</div>

    <ul class="pagination text-center mb-20">
        <li class="active"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">...</a></li>
    </ul></div>
<h2 class="mb-20">Смотрите также</h2>
<div class="articles mb-20">
    <div class="articles__list mb-20">
        <div class="article">
            <div class="article__image">
                <a href="#"><img src="img/tmp/article1.jpg"></a>
            </div>

            <div class="article__content">
                <a href="#" class="article__category">Психология</a>
                <a href="#" class="article__title">В чем мама всегда виновата</a>
            </div>

            <div class="icon-group article__icons">
                <a href="#" class="icon-info icon-info-views">120</a>
                <a href="#" class="icon-info icon-info-comments">3</a>
                <a href="#" class="icon-info icon-info-likes">3</a>
            </div>
        </div>

        <div class="article">
            <div class="article__mark">Партнёрский материал</div>

            <div class="article__image">
                <a href="#"><img src="img/tmp/article2.jpg"></a>
            </div>

            <div class="article__content">
                <a href="#" class="article__title">8 признаков овуляции, которые можно увидеть или почуствовать</a>
            </div>

            <div class="icon-group article__icons">
                <a href="#" class="icon-info icon-info-views">674</a>
                <a href="#" class="icon-info icon-info-comments">13</a>
            </div>
        </div>

    </div>
    <div class="text-center"><button class="btn btn-active-flat">Больше статей</button></div>
</div>
//= ../template/calendar.php