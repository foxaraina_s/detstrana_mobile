
<div class="content content-community scroll-top pt-20">
    <ul class="page-breadcrumbs">
        <li><a href="#">Каталог сообществ</a></li>
        <li><a href="#">Планирование и беременность</a></li>
        <li><span>Подготовка к зачатию</span></li>
    </ul>
        <h1 class="mb-20">Подготовка к зачатию</h1>
        <div class="communities mb-20">
                <div class="community-one mb-10">
                    <div class="community__image">
                        <img src="img/tmp/community1.png">
                    </div>
                    <div class="community__body">
                        <div class="comment__text">
                            Как здорово гулять по парку с коляской, пеленать малыша и с упоением
                            слушать, как он гулит. Скоро все будет! А пока - готовимся и постигаем
                            науку измерения базальной температуры, узнаем, как зачать мальчика, и
                            какие тесты на овуляцию лучше!
                        </div>
                        <div class="community__buttons">
                                <button class="btn btn-confirm btn-shaded w-46">Создать запись</button>
                                <button class="btn btn-confirm btn-shaded w-46">Вступить</button>
                        </div>
                        <button class="btn btn-active-flat btn-center">Правила сообщества</button>

                    </div>

                        </div>
                </div>
    <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-scrollable block-shaded w-100 mb-20">
        <li class="w-50 active"><a href="#">Записей (71)</a></li>
        <li class="w-50"><a href="#">Участников (10528)</a></li>
    </ul>
    <form class="form-search mb-20">
        <input type="text" name="city" value="" placeholder="Поиск записей в блоге">
    </form>
    <button data-target="#popup-user-filter" class="btn btn-popup btn-white mb-40">Показать фильтр</button>
    <div class="entries">
        <div class="entries__list">
            <div class="entry mb-10">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="enty__autor">Оксана Новикова</div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                    <div class="entry__section"><em>Планирование и беременность</em></div>
                </div>

                <div class="entry__title">
                   Беременность важно планировать!
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        С первым ребенком беременность протекала хорошо и спокойно. Потом через несколько лет я снова
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">120</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">1</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:58</div>
                </div>

                <div class="entry__title">
                    Гиперлактация
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div class="entries__morecomments">
            <button class="btn btn-active-flat w-100">Еще 15 комментариев</button>
        </div>
    </div>
    <div class="comments">

        <div class="comments__list">

            <div class="comment">

                <div class="comment__header">
                    <div class="comment__author">Виктория Кудрявцева</div>
                    <div class="comment__date">31 мая 2017 года, 12:58</div>
                    <div class="comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>

                <div class="comment__body">
                    <div class="comment__inner">
                        <div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
                    </div>

                    <div class="comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin comment__comment-close"></button>
                        </div>

                        <form class="comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>

            <div class="comment">

                <div class="comment__header">
                    <div class="comment__author">Виктория Кудрявцева</div>
                    <div class="comment__date">31 мая 2017 года, 12:58</div>
                    <div class="comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>

                <div class="comment__body">
                    <div class="comment__inner">

                        <div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>

                        <div class="slider comment__slider">
                            <div class="slider-changeable slider-wrapper">
                                <div class="slides">
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                </div>
                            </div>

                            <ul class="nav nav-dotted nav-dotted-small slider-control slider-changeable">
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin comment__comment-close"></button>
                        </div>

                        <form class="comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <div class="rel mb-10">
                                <button class="btn btn-confirm w-66">Комментировать</button>
                                <button class="btn btn-plus pull-right"></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <div class="comment">

                <div class="comment__header">
                    <div class="comment__author">Катя Медунцева <span>ответила</span></div>
                    <div class="comment__author">Виктория Кудрявцева</div>
                    <div class="comment__date">31 мая 2017 года, 12:58</div>
                    <div class="comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>

                <div class="comment__body">
                    <div class="comment__inner">
                        <div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
                    </div>

                    <div class="comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin comment__comment-close"></button>
                        </div>

                        <form class="comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <div class="entries mb-20 block-shaded">
        <div class="entries__list mb-20">
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                    Гиперлактация
                </div>

                <div class="entry__body pt-0">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="text-center pb-20"><button class="btn btn-active-flat">Больше записей</button></div>

    </div>
</div>
//= ../template/calendar.php