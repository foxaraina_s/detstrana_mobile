
<section class="service service-disease pt-20 scroll-top">
    <ul class="page-breadcrumbs">
        <li><a href="#">Сервисы</a></li>
        <li><span>Справочник болезней</span></li>
    </ul>
    <h1 class="mb-20">Справочник симптомов заболевания</h1>
    <form class="service__form-search">
        <input type="text" name="city" value="" placeholder="Поиск по болезням">
    </form>
    <div class="service__text">Укажите, для кого вы ищете</div>
    <ul class="service-disease__icons">
        <li class="service-disease__icon-pg active"><a href="#">Беременность</a></li>
        <li class="service-disease__icon-fm"><a href="#">Кормящие мамы</a></li>
        <li class="service-disease__icon-nch"><a href="#">Новорожденные<br>(0-4 нед.)</a></li>
        <li class="service-disease__icon-ch"><a href="#">Дети</a></li>
    </ul>
    <div class="slider nav-tabs-slider mb-20 block-shaded">
        <div class="slider-wrapper slider-changeable">
            <div class="nav-tabs-container slides">
                <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100"
                    style="white-space: nowrap;">
                    <li class="slide w-50"><a href="#">По алфавиту</a></li>
                    <li class="slide w-50"><a href="#">По категория</a></li>
                    <li class="slide w-50 active"><a href="#">По симптомам</a></li>
                </ul>
                <div class="border"></div>
            </div>
        </div>
        <a href="#" class="slider-rewind"></a><a href="#" class="slider-forward"></a>
    </div>
    <ul class="filter-abc">

        <li><a href="#">А</a></li>

        <li><a href="#">Б</a></li>

        <li><a href="#">В</a></li>

        <li><a href="#">Г</a></li>

        <li><a href="#">Д</a></li>

        <li><a href="#">Е</a></li>

        <li><a href="#">Ж</a></li>

        <li><a href="#">З</a></li>

        <li><a href="#">И</a></li>

        <li><a href="#">К</a></li>

        <li><a href="#">Л</a></li>

        <li><a href="#">М</a></li>

        <li><a href="#">Н</a></li>

        <li><a href="#">О</a></li>

        <li><a href="#">П</a></li>

        <li><a href="#">Р</a></li>

        <li><a href="#">С</a></li>

        <li><a href="#">Т</a></li>

        <li><a href="#">У</a></li>

        <li><a href="#">Ф</a></li>

        <li><a href="#">Х</a></li>

        <li><a href="#">Ц</a></li>

        <li><a href="#">Ч</a></li>

        <li><a href="#">Ш</a></li>

        <li><a href="#">Щ</a></li>

        <li><a href="#">Э</a></li>

        <li><a href="#">Ю</a></li>

        <li><a href="#">Я</a></li>

    </ul>
    <table class="table table-striped table-arrowed w-100 mb-20">
        <tbody>
        <tr>
            <td><a href="#">Бёдра</a></td>
        </tr>
        <tr>
            <td><a href="#">Горло</a></td>
        </tr>
        <tr>
            <td><a href="#">Половые органы</a></td>
        </tr>
        <tr>
            <td><a href="#">Боли в процессе деятельности</a></td>
        </tr>
        </tbody>
    </table>
<div class="service__text">
    <p>Многие мамы обладают способностью выявлять признаки заболеваний и симптомы болезней у своего ребёнка прежде, чем
    малыш признается, что у него болит горло или поднялась температура. Действительно, чтобы помочь крохе и оказать ему
        первую помощь, важно знать, как по симптомам определить заболевание.Родители, специально для вас мы создали справочник с удобным поиском, где в один клик можно получить актуальную информацию. Диагностика заболеваний по
        симптомам позволит вам принимать оперативные меры для оперативного лечения.</p>
</div>
    <h3 class="mb-30 service__section-title">Темы дня</h3>
//= ../template/articles.php
</section>
//= ../template/calendar.php

