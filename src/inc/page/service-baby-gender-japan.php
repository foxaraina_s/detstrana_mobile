
<section class="service service-gender scroll-top">
    <div class="service__subtitle"><a href="#">Сервисы</a> <i>❯</i> Определение пола</div>
    <h2 class="service__title">Определение пола ребенка<br> по японской таблице</h2>
    <div class="service__text">Чтобы узнать пол ребенка, укажите месяцы рождения родителей и месяц зачатия ребенка</div>

        <div class="gender__method--wrapper gender-method-japan">
            <div class="gender__method--form japanese_table_block">
                <form method="post">
            <div class="gender-form-father gender-form japanese_table_block_l">
            <div class="gender__section-title">Месяц рождения папы:</div>
            <div class="form-group form-group-birthday mb-20">
                <div class="form-birthday-month">
                    <select name="mm" placeholder="Месяц" id="jtb1" class="form-select select_big_input">
                        <option value="0">Январь</option>
                        <option value="1">Февраль</option>
                        <option value="2">Март</option>
                        <option value="3">Апрель</option>
                        <option value="4">Май</option>
                        <option value="5">Июнь</option>
                        <option value="6">Июль</option>
                        <option value="7">Август</option>
                        <option value="8">Сентябрь</option>
                        <option value="9">Октябрь</option>
                        <option value="10">Ноябрь</option>
                        <option value="11">Декабрь</option>
                    </select>
                </div>
            </div>
            </div>
            <div class="gender-form-mom gender-form japanese_table_block_r">
            <div class="gender__section-title">Месяц рождения мамы:</div>
            <div class="form-group form-group-birthday  mb-20">
                <div class="form-birthday-month">
                    <select name="mm" placeholder="Месяц" id="jtb2" class="form-select select_big_input">
                        <option value="0">Январь</option>
                        <option value="1">Февраль</option>
                        <option value="2">Март</option>
                        <option value="3">Апрель</option>
                        <option value="4">Май</option>
                        <option value="5">Июнь</option>
                        <option value="6">Июль</option>
                        <option value="7">Август</option>
                        <option value="8">Сентябрь</option>
                        <option value="9">Октябрь</option>
                        <option value="10">Ноябрь</option>
                        <option value="11">Декабрь</option>
                    </select>
                </div>
            </div>
            </div>
            <div class="gender-form-zigota gender-form mb-40 japanese_table_block_c">
            <div class="gender__section-title">Месяц зачатия ребенка:</div>
            <div class="form-group form-group-birthday  mb-20">
                <div class="form-birthday-month">
                    <select name="mm" placeholder="Месяц" id="jtb3" class="form-select select_big_input">
                        <option value="0">Январь</option>
                        <option value="1">Февраль</option>
                        <option value="2">Март</option>
                        <option value="3">Апрель</option>
                        <option value="4">Май</option>
                        <option value="5">Июнь</option>
                        <option value="6">Июль</option>
                        <option value="7">Август</option>
                        <option value="8">Сентябрь</option>
                        <option value="9">Октябрь</option>
                        <option value="10">Ноябрь</option>
                        <option value="11">Декабрь</option>
                    </select>
                </div>
            </div>
            </div>
            <div class="text-center btn-wrapper">
                <a class="btn btn-confirm btn-shaded baby_gender_button japanese_table w-93">Узнать пол</a>
            </div>
                </form>
        </div>
            <div id="result">
                <div class="blood_group_result baby-gender-result">
                    <div class="blood_group_result_content baby-gender-result__inner">
                    </div>
                </div>
                <div class="icon-group text-center">
                    <a href="#" class="icon-rounded icon-fb"></a>
                    <a href="#" class="icon-rounded icon-vk"></a>
                    <a href="#" class="icon-rounded icon-ok"></a>
                </div>
                <h3 class="gender__subtitle">Другие способы определения пола ребенка</h3>
                <div class="gender__choose">
                    <div class="gender__choose--methods">
                        <a href="service-baby-gender--blood-refresh.html" class="method--item blood__refresh">
                            По обновлению крови
                        </a>
                        <a href="service-baby-gender-blood-group.html" class="method--item blood__group">
                            По группе крови родителей
                        </a>
                        <a href="service-baby-gender-chinise.html" class="method--item chinese_table">
                            Китайский метод
                        </a>
                    </div>
                </div>

            </div>
            //= ../template/inner_comment.php
        </div>
    //= ../template/comment-block.php
    <p class="text-center"><button class="btn btn-active-flat">Больше записей</button></p>
    <div class="service__text">Определение пола ребенка по обновлению крови родителей считается одним из самых
        популярных методов. Он основывается на данных о будущих родителях. А именно, на периодах обновления крови у папы
        и у мамы. Считается, что у девушек оно проходит 1 раз за 3 года, а у мужчин – 1 раз в четыре года. <br>
        Так, когда вы пользуетесь калькулятором, определяющим пол будущего ребенка по обновлению крови, нужно знать: чья кровь была
        более «свежая» на момент зачатия, такого пола и будет кроха. <br>
        При расчете пола ребенка по обновлению крови также надо учитывать следующее: если вам делали переливание, то расчет будет вестись от даты последнего переливания
        крови. И еще: результат может быть искаженным, если пол ребенка по обновлению крови родителей вы рассчитали без
        учета недавних операций или различных травм с кровотечениями. Если метод определения пола по обновлению крови
        онлайн кажется вам не совсем достоверным, воспользуйтесь другими видами тестов.
    </div>
</section>
//= ../template/calendar.php
<script src="js/gender_js.js?<?php echo time() ?>"></script>

