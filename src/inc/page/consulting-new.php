
<div class=" content content-community scroll-top pt-20">
    <ul class="page-breadcrumbs">
        <li class="back"><a href="#">вернуться к сообществу</a></li>
    </ul>
    <h1 class="mb-20">Задать вопрос:</h1>
    <button data-target="#popup-category" class="btn btn-popup btn-white mb-20">Выберите специализацию</button>
    <div class="mlr-16 mb-20">
        <p>Вы не соостоите в сообществе «Подготовка к зачатию». С публикпцией записи вы автоматически
            становитесь участником сообщества.</p>
    </div>
    <div class="new-record-form mlr-16 mb-20">
        <form action="#" method="post" enctype="multipart/form-data">
            <div class="form-group mb-20">
                <label class="form-label form-label-hidden">Четко и кратко сформулируйте вопрос</label>
                <input name="name" type="text" placeholder="Четко и кратко сформулируйте вопрос" class="form-control">
                <div class="form-limit">&nbsp;</div>
                <div class="form-error">&nbsp;</div>
            </div>
            <div class="text-editor mb-20">
                <div class="text-editor__toolbar btn-toolbar">
                    <div class="slider text-editor-slider mb-20">
                        <div class="slider-wrapper slider-changeable">
                            <ul class="nav edit-text-slide edit-text-scrollable  w-100">
                                <li class="slide">
                                    <button class="semibold"><i class="material-icons">&#xE238;</i> </button>
                                    <button class="italic"><i class="material-icons">&#xE23F;</i> </button>
                                    <button class="underline"><i class="material-icons">&#xE249;</i> </button>
                                    <button class="color"><i class="material-icons">&#xE23A;</i> </button>
                                </li>
                                <li class="slide">
                                    <button class="left-align"><i class="material-icons">&#xE236;</i> </button>
                                    <button class="center-align"><i class="material-icons">&#xE234;</i> </button>
                                    <button class="right-align"><i class="material-icons">&#xE234;</i> </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="text-editor__editable" contenteditable data-maxlength="800">
                    <span>Введите текст</span>
                    <br>
                </div>
                <div class="form-limit">0/800</div>
            </div>
            <div class="form-group form-group-checkbox mb-20">
                <input type="checkbox" name="annonim" id="annonim">
                <label for="annonim" class="form-label-large">анонимный вопрос</label>
            </div>
            <button class="btn btn-confirm btn-shaded w-100 mb-20">Отправить вопрос</button>
        </form>
    </div>

    //= ../template/calendar.php