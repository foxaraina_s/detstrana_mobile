
<div id="popup-services" class="popup popup-services popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Сервисы</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Сервис 1</a></li>
            <li><a href="#">Сервис 2</a></li>
            <li><a href="#">Сервис 3</a></li>
            <li><a href="#">Сервис 4</a></li>
            <li><a href="#">Сервис 5</a></li>
            <li><a href="#">Сервис 6</a></li>
        </ul>
    </div>
</div>
<section class="service">
    <h1 class="mb-20">Онлайн-сервисы и калькуляторы</h1>
    <div class="mb-20">
        <button data-target="#popup-services" class="btn btn-popup btn-white">Все сервисы</button>
    </div>
    <div class="service_count">Показано 23 сервиса</div>
    <h3 class="mb-20 service__section-title">Популярные сервисы</h3>
    <div class="service__list">
        <div class="service__item">
            <a href="#">
<div class="service__item--title">Рейтинги товаров для детей <span class="service__icon service__icon--star"></span> </div>
            <div class="service__item--image"><img src="img/tmp/serv1.jpg"> </div>
            <div class="service__item--category">Мама и ребенок</div>
            <div class="service__item--content">
                Выбирайте лучшие товары для дома и детей. Смотрите оценки других родителей, оставляйте отзывы!
            </div>
            </a>
        </div>
    </div>
    <h3 class="mb-20 service__section-title">Все сервисы</h3>
    <div class="service__list">
        <div class="service__item">
            <a href="#">
            <div class="service__item--title">Расчет даты родов и график обследований </div>
            <div class="service__item--image"><img src="img/tmp/serv2.jpg"> </div>
            <div class="service__item--category">Беременность и роды</div>
            <div class="service__item--content">
                Узнайте дату появления на свет своего малыша и воспользуйтесь личным календарем диагностики здоровья.
            </div>
            </a>
        </div>
    </div>
</section>
//= ../template/calendar.php
