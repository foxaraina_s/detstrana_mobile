
<section class="service service-gender scroll-top">
    <div class="service__subtitle"><a href="#">Сервисы</a> <i>❯</i> Определение пола</div>
    <h2 class="service__title">Определение пола ребенка</h2>
    <div class="service__text">Используйте 4 популярных способа, чтобы узнать пол вашего ребенка</div>
    <div class="gender__choose gender__choose--main">
        <div class="gender__choose--wrapper">
            <div class="gender__choose--title">
                <div>4</div>
                <div>способа узнать</div>
                <div>пол ребенка</div>
            </div>
        </div>
        <div class="gender__choose--methods">
            <a href="service-baby-gender--blood-refresh.html" class="method--item blood__refresh">
                По обновлению крови
            </a>
            <a href="service-baby-gender-blood-group.html" class="method--item blood__group">
                По группе крови родителей
            </a>
            <a href="service-baby-gender-japan.html" class="method--item japanese__table">
                Японский метод
            </a>
            <a href="service-baby-gender-chinise.html" class="method--item chinese_table">
                Китайский метод
            </a>
        </div>
    </div>
    <div class="service__text">Девочка или мальчик? Этим вопросом озадачены многие будущие родители. Вы тоже хотите
        узнать, какой пол будет у ребёнка? Как выявить его со всей точностью? Воспользуйтесь нашим сервисом, благодаря
        которому вы сможете без труда определить пол будущего малыша. Выбирайте, какой вариант вам ближе: китайские или
        японские таблицы для определения пола, или вы хотели бы рассчитать пол ребенка по обновлению крови? Кстати, наши
        прабабушки и прадедушки тоже пытались прогнозировать, кто появится на свет – маленькая принцесса или мальчуган.
        И со всей тщательностью подходили к выбору даты зачатия. А еще вокруг вопроса о том, кто же родится у мамы и
        папы, сформировалось множество суеверий и примет. Мы вам предлагаем методы определения пола ребёнка (например,
        по таблицам), которыми воспользовались уже тысячи родителей. Попробуйте один из четырех предложенных вариантов и
        не забудьте поделиться с нами своим результатом!
    </div>
</section>
//= ../template/calendar.php

