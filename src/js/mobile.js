$(function() {


	$('header')
		.on('ds-resize', function() {
			$(this).width($('.wrapper').width());
		})
		.trigger('ds-resize');

	$(window)
		.on('resize orientationchange', function(event) {
			$('header, ul.nav-tabs-bordered li.active').trigger('ds-resize');
		})
		.on('ds-lock', function() {
			var st = $(this).scrollTop();
			$('body')
				.data('scroll-top', st)
				//.css('top', -st)
				.addClass('lock-screen');
			$('body .wrapper').css('top', -st)
		})
		.on('ds-unlock', function() {
			$('body')
				.removeClass('lock-screen');
				$('body .wrapper').css('top', 0);
			$(this)
				.scrollTop($('body').data('scroll-top'));
		});


	$('ul.nav li, ul.js-changeable li').on('click', function(event) {
		event.preventDefault()
		$(this).addClass('active').siblings().removeClass('active');
	});


	$('.popup')
		.on('ds-open', function(event) {
			$(window).trigger('ds-lock');
			$(this).addClass('popup-open');
		})
		.on('ds-close', function() {
			$(window).trigger('ds-unlock');
			$(this).removeClass('popup-open');
		})
		.find('.popup__close')
		.on('click', function(event) {
			event.preventDefault();
			$(this).parents('.popup').trigger('ds-close');
		});


	$('a,button').filter('[data-target]').on('click', function(event) {
		event.preventDefault();
		$($(this).data('target')).trigger('ds-open');
	});
	

	$('.form-control').on('keyup mouseup change ds-change', function() {

		var
			maxlength = $(this).attr('maxlength'),
			length = $(this).val().length,
			$parent = $(this).parents('.form-group').eq(0);

			if(length == 0) $(this).prev('.form-label').addClass('form-label-hidden');
			else $(this).prev('.form-label').removeClass('form-label-hidden');


		if(!maxlength) return true;
		if(maxlength <= length) $parent.addClass('form-group-limit');
		else $parent.removeClass('form-group-limit');
		
		$('.form-limit', $parent).text(length + ' / ' + maxlength);

		//if(length > 0) $(this).prev('label').css('opacity', 1);
		//else $(this).prev('label').css('opacity', 0);
	}).trigger('ds-change');

	$('ul.nav-tabs li')
		.on('ds-init click', function(event) {

			event.preventDefault();
			var id = $(this).find('a').attr('href');
			if(id == '#') return;
			var
				$tab = $(id),
				$tabs = $tab.parents('.tabs').eq(0)
				$old = $('.tab.active', $tabs);
				$tabs.css('transform', 'translate3D(-' + ($tabs.find('.tab').index($tab) * 100) + '%, 0, 0)');

				//alert($tabs.find('.tab').index($tab))
		})
		.filter('.active')
		.trigger('ds-init');

	$('ul.nav-tabs-bordered')
		.each(function() {
			$(this)
				.wrap('<div class="nav-tabs-container"/>')
				.parents('.nav-tabs-container')
				.eq(0)
				.addClass($(this).hasClass('nav-tabs-scrollable') ? 'slides' : '')
				.find('.border')
				.remove()
				.end()
				.append('<div class="border"/>');
				
			if($(this).hasClass('nav-tabs-scrollable'))
				$(this).parents('.slider').eq(0).append('<a href="#" class="slider-rewind"></a><a href="#" class="slider-forward"></a>')
			
			$(this).find('li')
				.on('ds-change ds-resize click', function(event) {
					event.preventDefault();
					var $ul = $(this).parents('ul').eq(0);
					$ul
						.parents('.nav-tabs-container')
						.eq(0)
						.find('.border')
						.css('left', $(this).position().left)
						.css('width', $(this).width());
				})
				.filter('.active')
				.trigger('ds-change');
		});


	/*$('.form-select')
		.each(function() {
			var val = $(this).attr('placeholder'); ///$(this).val() ? $(this).find('option:selected:gt(0)').text() :
			$(this).after('<div class="form-selected">' + val + '</div>');
		})
		.on('ds-init change blur', function() {
			$(this).next('.form-selected').text($(this).find('option:selected').text());
		})
		//.trigger('ds-init');*/

	$('.form-select').wrap('<div class="form-select-wrapper"/>');


	$('.entry__comment')
		.find('.entry__comment-open')
			.on('click', function(event) {
				event.preventDefault();
				$(this).parents('.entry__comment').addClass('active');
			})
			.end()
		.find('.entry__comment-close')
			.on('click', function(event) {
				event.preventDefault();
				$(this).parents('.entry__comment').removeClass('active');
			});

	$('.comment__comment')
		.find('.comment__comment-open')
			.on('click', function(event) {
				event.preventDefault();
				$(this).parents('.comment__comment').toggleClass('active');
			})
			.end()
		.find('.comment__comment-close')
			.on('click', function(event) {
				event.preventDefault();
				$(this).parents('.comment__comment').removeClass('active');
			})
			.end()
		.find('.comment__comment-like')
			.on('click', function(event) {
				event.preventDefault();
				$(this).toggleClass('btn-flat btn-active-flat');
			});


	$('.popup')
		.on('click', 'li.haschildren', function() {
			var
				$popup =  $(this).parents('.popup').eq(0),
				id = $(this).data('node'),
				$submenu = $('#submenu-' + id).addClass('active');

			$popup
				.find('.popup__header')
				.text($(this).text());
			$popup
				.find('.popup__back')
				.data('node', id)
				.addClass('active');
		})
		.on('click', '.popup__back', function() {
			var $popup =  $(this).parents('.popup').eq(0), $submenus = $popup.find('.popup__submenu.active');
			if($submenus.length <= 2) {
				$(this).removeClass('active');
				$popup.find('.popup__header').text($popup.find('popup__header').data('placeholder'));
			}
			if($submenus.length > 1) $submenus.last().removeClass('active');
		});


	$('.slider').each(function() {
		var $slider = {
			'slider': $(this),
			'wrapper' : $(this).find('.slides:eq(0)'),
			'slides': $(this).find('.slides:eq(0) .slide'),
			'detecting': false,
			'moving':false,
			'touch': null,
			'index': 0,
			'x': 0,
			'y': 0,
			'd': 0
		};

		$slider.slider.on('ds-init', function(event) {
			var $li = $slider.slides.filter('.active').first(), index = $li.length > 0 ? $slider.slides.index($li) : 0;
			$slider.slider.trigger('ds-dispatch-change', index);
		});

		$slider.slider.on('ds-dispatch-change', function(event, index) {
			if(event.target !== $(this).get(0)) return;
			$slider.index = index < 0 ? 0 : index > $slider.slides.length - 1 ? $slider.slides.length - 1 : index;
			$(this)
				.find('.slider-changeable')
				.trigger('ds-change');
		});

		$slider.slider.find('.slider-forward').on('click', function(event) {
			event.preventDefault();
			$slider.slider.trigger('ds-dispatch-change', $slider.index + 1);
		});
		
		$slider.slider.find('.slider-rewind').on('click', function(event) {
			event.preventDefault();
			$slider.slider.trigger('ds-dispatch-change', $slider.index - 1);
		});

		$slider.slider.find('.slider-wrapper')
			.on('ds-change', function(event) {
				var
					dx = -$slider.index * $slider.slides.first().width(),
					mx = $slider.wrapper.width() - $slider.slides.first().width() * $slider.slides.length;
					if(dx < mx) dx = mx;
					$slider.d = dx;
				$slider.wrapper
					.css('transform', 'translate3d(' + $slider.d + 'px, 0, 0)')
					.find('.slide')
					.removeClass('active')
					.eq($slider.index)
					.addClass('active');
			})
			.on('touchstart', function(event) {	
				if(event.touches.length != 1 || $slider.moving) return;
				$slider.detecting = true;
				_touch = event.changedTouches[0];
				$slider.x = _touch.pageX;
				$slider.y = _touch.pageY;
			})
			.on('touchmove', function(event) {
				if(!$slider.detecting && !$slider.moving) return;
				var touch = event.changedTouches[0], x = touch.pageX, y = touch.pageY;

				if($slider.detecting) {
					if(Math.abs($slider.x - x) >= Math.abs($slider.y - y)) {
						event.preventDefault();
						$slider.moving = true;
						$slider.slider.addClass('moving');
					}
					$slider.detecting = false;
				}

				if($slider.moving)	{
					event.preventDefault();
					var dx = x - $slider.x;
					dx = dx + $slider.d > 0 || dx + $slider.d < $slider.wrapper.width() - $slider.slides.first().width() * $slider.slides.length ? dx/5 : dx*0.75;
					$slider.wrapper.css('transform', 'translate3d(' + (dx + $slider.d) + 'px, 0, 0)');
				}
			})
			.on('touchend touchcancel', function(event) {
				//if(event.changedTouches[0] !== _touch) return;
				if(!$slider.moving) return;
				event.preventDefault();
				$slider.slider.removeClass('moving');
				$slider.detecting = $slider.moving = false;
				
				var
					dx = event.changedTouches[0].pageX - $slider.x,
					index = -(dx + $slider.d)/$slider.slides.first().width();
			
				index = dx > 30 ? Math.floor(index) : dx < -30 ? Math.ceil(index) : $slider.index;
				$slider.slider.trigger('ds-dispatch-change', index);
			});
	
		$slider.slider
			.find('ul.slider-control')
			.on('ds-change', function(event) {
				$(this)
					.find('li')
					.removeClass('active')
					.eq($slider.index)
					.addClass('active')
					.siblings();
			})
			.find('li')
			.off('click')
			.on('click', function(event) {
				event.preventDefault();
				var
					$ul = $(this).parents('ul').eq(0),
					index = $ul.find('li').index($(this));
				$slider.slider.trigger('ds-dispatch-change', index);
			});

		$slider.slider.trigger('ds-init');
	});


	/*$('.pregnancy-calendar__slider')
		.find('.pregnancy-calendar__title')
			.on('ds-change', function(event, index) {
				$(this).text(String('I').repeat(index+1) + ' триместр');
			});*/


	// Set all sliders to start
	//$('.slider').trigger('ds-dispatch-change', 0)

	// Hide Search form
	$(window).scrollTop(
		$('.scroll-top').length ?
		$('.scroll-top:first').offset().top - $('header').height() :
		$('header').height()
	);


	//$('#popup-articles').trigger('ds-open');
});