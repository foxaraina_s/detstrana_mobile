$(function() {

    $('.text-editor__editable').on('keyup mouseup change ds-change', function() {

        var
            maxlength = $(this).data('maxlength'),
            length = $(this).text().length,
            $parent = $(this).parent('.text-editor');
        if(!maxlength) return true;
        if(maxlength <= length) $parent.addClass('form-group-limit');
        else $parent.removeClass('form-group-limit');

        $('.form-limit', $parent).text(length + ' / ' + maxlength);

        //if(length > 0) $(this).prev('label').css('opacity', 1);
        //else $(this).prev('label').css('opacity', 0);
    }).trigger('ds-change');


    $('ul.analysis-calendar-bordered')
        .each(function() {
            $(this)
                .wrap('<div class="analysis-calendar-container"/>')
                .parents('.analysis-calendar-container')
                .eq(0)
                .addClass($(this).hasClass('analysis-calendar-scrollable') ? 'slides' : '')
                .find('.border')
                .remove()
                .end()
                .append('<div class="border"/>');

            if($(this).hasClass('analysis-calendar-scrollable'))
                $(this).parents('.slider').eq(0).append('<a href="#" class="slider-rewind"></a><a href="#" class="slider-forward"></a>')
        })
        .find('li')
        .on('ds-change ds-resize click', function(event) {
            event.preventDefault();
            var $ul = $(this).parents('ul').eq(0);
            $ul
                .parents('.analysis-calendar-container')
                .eq(0)
                .find('.border')
                .css('left', $(this).position().left)
                .css('width', $(this).width());
        })
        .filter('.active')
        .trigger('ds-change');


    $('ul.edit-text-slide')
        .each(function() {
            $(this)
                .wrap('<div class="edit-text-container"/>')
                .parents('.edit-text-container')
                .eq(0)
                .addClass($(this).hasClass('edit-text-scrollable') ? 'slides' : '')
                .end();

            if($(this).hasClass('edit-text-scrollable'))
                $(this).parents('.slider').eq(0).append('<a href="#" class="slider-rewind"></a><a href="#" class="slider-forward"></a>')
        })
        .find('li')

        .filter('.active')
        .trigger('ds-change');

    $('.form-select').wrap('<div class="form-select-wrapper"/>');


    // Gender Blood group
    $('.blood__group--btn').on('click', function() {
        $(this).addClass('active').siblings().removeClass('active');
        return false;
    });
// Gender Chinese Table
    $('.chienese-results td').on('click', function() {
        $(this).addClass('active')
        $('.chienese-results td').not(this).removeClass('active');
        return false;
    });

// Accordion
    $('.accordion > .accordion__item:eq(0) .accordion__title').addClass('active').next().slideDown();

    $('.accordion .accordion__title').click(function(j) {
        var dropDown = $(this).closest('.accordion__item').find('.accordion__content');

        $(this).closest('.accordion').find('.accordion__content').not(dropDown).slideUp();

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).closest('.accordion').find('.accordion__title.active').removeClass('active');
            $(this).addClass('active');
        }

        dropDown.stop(false, true).slideToggle();

        j.preventDefault();
    });

});




