'use strict';

(function ($) {

    $(function () {

        function babyGenderMonth(elem){
            var babyGenderMonth;
            if(elem == 1) babyGenderMonth = 'Январь';
            if(elem == 2) babyGenderMonth =  'Февраль';
            if(elem == 3) babyGenderMonth =  'Март';
            if(elem == 4) babyGenderMonth =  'Апрель';
            if(elem == 5) babyGenderMonth =  'Май';
            if(elem == 6) babyGenderMonth =  'Июнь';
            if(elem == 7) babyGenderMonth =  'Июль';
            if(elem == 8) babyGenderMonth =  'Август';
            if(elem == 9) babyGenderMonth =  'Сентябрь';
            if(elem == 10) babyGenderMonth =  'Октябрь';
            if(elem == 11) babyGenderMonth =  'Ноябрь';
            if(elem == 12) babyGenderMonth =  'Декабрь';

            return babyGenderMonth;
        }

        function babyGenderDays(elem) {
            var babyGenderDays;
            var ending = String(elem).slice(String(elem).length - 1, String(elem).length);
            if(ending == 0 || ending == 5 || ending == 6 || ending == 7 || ending == 8 || ending == 9) babyGenderDays = elem + ' лет';
            if(ending == 1) babyGenderDays = elem + ' год';
            if(ending == 2 || ending == 3 || ending == 4) babyGenderDays = elem + ' года';

            return babyGenderDays;
        }

        /* Таблицы определения пола: */

        /* Chinese table */
        var chineseTable = [["0","1","0","1","1","1","1","1","1","1","1","1"],
            ["1","0","1","0","1","0","1","1","1","0","1","0"],
            ["0","1","0","1","1","1","1","1","1","0","1","1"],
            ["1","0","0","0","0","0","0","0","0","0","0","0"],
            ["0","1","1","0","1","0","0","1","0","0","0","0"],
            ["1","1","0","1","1","0","1","0","1","1","1","0"],
            ["1","0","1","1","0","1","1","0","0","0","0","0"],
            ["0","1","1","0","0","1","0","1","1","1","1","1"],
            ["1","0","1","0","0","1","0","1","0","0","0","0"],
            ["0","1","0","1","0","0","1","1","1","1","0","1"],
            ["1","0","1","0","0","0","1","1","1","1","0","0"],
            ["0","1","0","0","1","1","0","0","0","1","1","1"],
            ["1","0","0","0","0","0","0","0","0","0","1","1"],
            ["1","0","1","0","0","0","0","0","0","0","0","1"],
            ["1","0","1","0","0","0","0","0","0","0","0","1"],
            ["0","1","0","1","0","0","0","1","0","0","0","1"],
            ["0","0","1","0","0","0","0","0","0","0","1","1"],
            ["1","1","0","1","0","0","0","1","0","0","1","1"],
            ["0","1","1","0","1","0","0","0","1","1","1","1"],
            ["1","0","1","1","0","1","0","1","0","1","0","1"],
            ["0","1","0","1","1","0","1","0","1","0","1","0"],
            ["1","0","1","1","1","0","0","1","0","0","0","0"],
            ["0","1","0","1","0","1","1","0","1","0","1","0"],
            ["1","0","1","0","1","0","1","1","0","1","0","1"],
            ["0","1","0","1","0","1","0","1","1","0","1","0"]
        ];
        /* Chinese table. End. */

        /* Japanese table 1. */
        var japaneseTableFirst = [["1","5","9","1","5","9","1","5","9","1","5","9"],
            ["10","2","6","10","2","6","10","2","6","10","2","6"],
            ["7","11","3","7","11","3","7","11","3","7","11","3"],
            ["4","8","12","4","8","12","4","8","12","4","8","12"],
            ["1","5","9","1","5","9","1","5","9","1","5","9"],
            ["10","2","6","10","2","6","10","2","6","10","2","6"],
            ["7","11","3","7","11","3","7","11","3","7","11","3"],
            ["4","8","12","4","8","12","4","8","12","4","8","12"],
            ["1","5","9","1","5","9","1","5","9","1","5","9"],
            ["10","2","6","10","2","6","10","2","6","10","2","6"],
            ["7","11","3","7","11","3","7","11","3","7","11","3"],
            ["4","8","12","4","8","12","4","8","12","4","8","12"]
        ];
        /* Japanese table 1. End. */

        /* Japanese table 2. */
        var japaneseTableSecond = [["","","","","","1","","","","","","","1","1"],
            ["","","","","1","2","","","","","","","6","1"],
            ["","","","1","2","3","","","","","","","1","2"],
            ["","","1","2","3","4","","","","","","","1","1"],
            ["","1","2","3","4","5","","","","","","","2","1"],
            ["1","2","3","4","5","6","","","","","","","1","1"],
            ["2","3","4","5","6","7","","","","","","","1","2"],
            ["3","4","5","6","7","8","","","","","","1","1","3"],
            ["4","5","6","7","8","9","","","","","1","2","1","2"],
            ["5","6","7","8","9","10","","","","1","2","3","10","1"],
            ["6","7","8","9","10","11","","","1","2","3","4","1","1"],
            ["7","8","9","10","11","12","","1","2","3","4","5","1","1"],
            ["8","9","10","11","12","","1","2","3","4","5","6","1","1"],
            ["9","10","11","12","","","2","3","4","5","6","7","5","1"],
            ["10","11","12","","","","3","4","5","6","7","8","1","10"],
            ["11","12","","","","","4","5","6","7","8","9","3","1"],
            ["12","","","","","","5","6","7","8","9","10","3","1"],
            ["","","","","","","6","7","8","9","10","11","1","1"],
            ["","","","","","","7","8","9","10","11","12","1","1"],
            ["","","","","","","8","9","10","11","12","","1","2"],
            ["","","","","","","9","10","11","12","","","1","1"],
            ["","","","","","","10","11","12","","","","8","1"],
            ["","","","","","","11","12","","","","","1","5"],
            ["","","","","","","12","","","","","","1","2"]
        ];
        /* Japanese table 2. End. */


        /* Blood group */
        if(localStorage.getItem('bglDad')) {
            var bglDad = localStorage.getItem('bglDad');
            var bglMother = localStorage.getItem('bglMother');

            $('.blood_group_link.active').removeClass('active');
            $($('.blood_group_links.dad > .blood_group_link')[bglDad - 1]).addClass('active');
            $($('.blood_group_links.mother > .blood_group_link')[bglMother - 1]).addClass('active');

            if (localStorage.getItem('baby_gender') == 'girl') {
                $('.blood_group_result').removeClass('boy').addClass('girl').find('#bgr_gender').text('девочка');
            }
            if(localStorage.getItem('baby_gender') == 'boy') {
                $('.blood_group_result').removeClass('girl').addClass('boy').find('#bgr_gender').text('мальчик');
            }

            window.onload=function(){
                babyGenderResult();
            }

            $.post( "https://detstrana.ru/service/baby-gender/counter/", { item_id: 2 } );

            localStorage.removeItem('bglDad');
            localStorage.removeItem('bglMother');
            localStorage.removeItem('baby_gender');
        }

        /* Refresh blood */
        if(localStorage.getItem('brb1_1')) {

            $('.blood_refresh_block_t  .form-birthday-day').find('select').val(localStorage.getItem('brb1_1'));
            $('.blood_refresh_block_t  .form-birthday-day').find('.form-selected').text(localStorage.getItem('brb1_1'));
            $('.blood_refresh_block_t .form-birthday-month').find('select').val(localStorage.getItem('brb1_2'));
            $('.blood_refresh_block_t .form-birthday-month').find('.form-selected').text(babyGenderMonth(localStorage.getItem('brb1_2')));
            $('.blood_refresh_block_t  .form-birthday-year').find('select').val(localStorage.getItem('brb1_3'));
            $('.blood_refresh_block_t  .form-birthday-year').find('.form-selected').text(localStorage.getItem('brb1_3'));
            $('.blood_refresh_block_m  .form-birthday-day').find('select').val(localStorage.getItem('brb2_1'));
            $('.blood_refresh_block_m  .form-birthday-day').find('.form-selected').text(localStorage.getItem('brb2_1'));
            $('.blood_refresh_block_m  .form-birthday-month').find('select').val(localStorage.getItem('brb2_2'));
            $('.blood_refresh_block_m  .form-birthday-month').find('.form-selected').text(babyGenderMonth(localStorage.getItem('brb2_2')));
            $('.blood_refresh_block_m  .form-birthday-year').find('select').val(localStorage.getItem('brb2_3'));
            $('.blood_refresh_block_m  .form-birthday-year').find('.form-selected').text(localStorage.getItem('brb2_3'));
            $('.blood_refresh_block_b  .form-birthday-day').find('select').val(localStorage.getItem('brb3_1'));
            $('.blood_refresh_block_b  .form-birthday-day').find('.form-selected').text(localStorage.getItem('brb3_1'));
            $('.blood_refresh_block_b  .form-birthday-month').find('select').val(localStorage.getItem('brb3_2'));
            $('.blood_refresh_block_b  .form-birthday-month').find('.form-selected').text(babyGenderMonth(localStorage.getItem('brb3_2')));
            $('.blood_refresh_block_b  .form-birthday-year').find('select').val(localStorage.getItem('brb3_3'));
            $('.blood_refresh_block_b  .form-birthday-year').find('.form-selected').text(localStorage.getItem('brb3_3'));

            if(localStorage.getItem('baby_gender') == 'boy'){
                $('.blood_group_result').removeClass('girl hermaphrodite').addClass('boy').children('.blood_group_result_content').html('<div>ПОЗДРАВЛЯЕМ!</div><div>У ВАС, СКОРЕЕ ВСЕГО, БУДЕТ<br> <span id=bgr_gender>мальчик</span></div>');
                $('.csd_table').removeClass('girl hermaphrodite').addClass('boy');
            }
            if(localStorage.getItem('baby_gender') == 'girl') {
                $('.blood_group_result').removeClass('boy hermaphrodite').addClass('girl').children('.blood_group_result_content').html('<div>ПОЗДРАВЛЯЕМ!</div><div>У ВАС, СКОРЕЕ ВСЕГО, БУДЕТ<br> <span id=bgr_gender>девочка</span></div>');
                $('.csd_table').removeClass('boy hermaphrodite').addClass('girl');
            }
            if (localStorage.getItem('baby_gender') == 'exception'){
                $('.blood_group_result').removeClass('girl boy').addClass('hermaphrodite').children('.blood_group_result_content').html(' <div class="gb-text">Вероятность<br>появления мальчика<br>или девочки одинакова</div>');
                $('.csd_table').removeClass('girl boy').addClass('hermaphrodite');
            }

            window.onload=function(){
                babyGenderResult();
            }

            $.post( "https://detstrana.ru/service/baby-gender/counter/", { item_id: 1 } );

            localStorage.removeItem('brb1_1');
            localStorage.removeItem('brb1_2');
            localStorage.removeItem('brb1_3');
            localStorage.removeItem('brb2_1');
            localStorage.removeItem('brb2_2');
            localStorage.removeItem('brb2_3');
            localStorage.removeItem('brb3_1');
            localStorage.removeItem('brb3_2');
            localStorage.removeItem('brb3_3');
            localStorage.removeItem('baby_gender');
        }

        /* Japanese table */
        if(localStorage.getItem('jtb1')) {

            $('.japanese_table_block_l').find('select').val(localStorage.getItem('jtb1'));
            $('.japanese_table_block_l').find('.form-selected').text(babyGenderMonth(parseInt(localStorage.getItem('jtb1')) + 1));
            $('.japanese_table_block_r').find('select').val(localStorage.getItem('jtb2'));
            $('.japanese_table_block_r').find('.form-selected').text(babyGenderMonth(parseInt(localStorage.getItem('jtb2')) + 1));
            $('.japanese_table_block_c').find('select').val(localStorage.getItem('jtb3'));
            $('.japanese_table_block_c').find('.form-selected').text(babyGenderMonth(parseInt(localStorage.getItem('jtb3')) + 1));

            if(localStorage.getItem('baby_gender') == 'boy'){
                $('.blood_group_result').removeClass('girl hermaphrodite').addClass('boy').children('.blood_group_result_content').html('<div>ПОЗДРАВЛЯЕМ!</div><div>У ВАС, СКОРЕЕ ВСЕГО, БУДЕТ<br> <span id=bgr_gender>мальчик</span></div>');
                $('.csd_table').removeClass('girl hermaphrodite').addClass('boy');
            }
            if(localStorage.getItem('baby_gender') == 'girl') {
                $('.blood_group_result').removeClass('boy hermaphrodite').addClass('girl').children('.blood_group_result_content').html('<div>ПОЗДРАВЛЯЕМ!</div><div>У ВАС, СКОРЕЕ ВСЕГО, БУДЕТ<br> <span id=bgr_gender>девочка</span></div>');
                $('.csd_table').removeClass('boy hermaphrodite').addClass('girl');
            }
            if (localStorage.getItem('baby_gender') == 'exception'){
                $('.blood_group_result').removeClass('girl boy').addClass('hermaphrodite').children('.blood_group_result_content').html(' <div class="gb-text">Вероятность<br>появления мальчика<br>или девочки одинакова</div>');
                $('.csd_table').removeClass('girl boy').addClass('hermaphrodite');
            }


            window.onload=function(){
                babyGenderResult();
            }

            $.post( "https://detstrana.ru/service/baby-gender/counter/", { item_id: 3 } );

            localStorage.removeItem('jtb1');
            localStorage.removeItem('jtb2');
            localStorage.removeItem('jtb3');
            localStorage.removeItem('baby_gender');
        }

        function chineseTableCell(elem) {
            if(elem==0){
                var classNameChineseTable = 'girl';
            } else {
                var classNameChineseTable = 'boy';
            }
            return classNameChineseTable;
        }

        /* Chinese table */
        if(localStorage.getItem('ctb1')) {

            var ctb1 = localStorage.getItem('ctb1');
            var ctb2 = localStorage.getItem('ctb2');
            $('.chinese_table_block_l').find('select').val(ctb1);
            $('.chinese_table_block_l').find('.form-selected').text(babyGenderDays(parseInt(ctb1) + 18));
            $('.chinese_table_block_r').find('select').val(ctb2);
            $('.chinese_table_block_r').find('.form-selected').text(babyGenderMonth(parseInt(ctb2) + 1));

            if (localStorage.getItem('baby_gender') == 'girl') {
                $('.blood_group_result').removeClass('boy').addClass('girl').find('#bgr_gender').text('девочка');
                $('.csd_table').removeClass('boy').addClass('girl');
            } else {
                $('.blood_group_result').removeClass('girl').addClass('boy').find('#bgr_gender').text('мальчик');
                $('.csd_table').removeClass('girl').addClass('boy');
            }
            $("table.csd_table tr:not(:first-child)").remove();
            var startYear = parseInt(ctb1);
            var endYear = parseInt(ctb1) + 5;
            if(chineseTable.length < parseInt(ctb1) + 5) endYear = chineseTable.length;
            for(var s = startYear; s < endYear; s++){
                startYear = startYear + 1;
                $('.csd_table').append('<tr></tr>');
                $('.csd_table').find('tr:last-child').append('<td>' + (startYear + 17) + '</td>');
                for(var m = 0; m<12; m++){
                    if(ctb1 == s && ctb2 == m){
                        var classActive = 'active';
                        $('.csd_table').find('td.active').removeClass('active');
                        $('.csd_table').find('tr:last-child').children('td:first-child').addClass('active');
                        $('.csd_table').find('tr:first-child').children('td:nth-child(' + (m+2) + ')').addClass('active');
                    } else {
                        var classActive = '';
                    }
                    $('.csd_table').find('tr:last-child').append('<td><div class="' + chineseTableCell(chineseTable[s][m]) + ' ' + classActive + '"></div></td>');
                }
            }
            $('.csd_table tr td').on('click', function() {
                $(this).addClass('active')
                $('.chienese-results td').not(this).removeClass('active');
                return false;
            });

            window.onload=function(){
                babyGenderResult();
            }

            $.post( "https://detstrana.ru/service/baby-gender/counter/", { item_id: 4 } );

            localStorage.removeItem('ctb1');
            localStorage.removeItem('ctb2');
            localStorage.removeItem('baby_gender');
        }


        $(document).on('click', function(e){
            if(!$(e.target).closest('.select_big').hasClass('select_big')){
                $('.select_big_list').slideUp();
            }
        }).on('click', '.select_big_list > div > div',  function(){
            $(this).closest('.select_big').find('span').text($(this).text());
            $(this).closest('.select_big').find('input').val($(this).attr('data-val'));
            $(this).closest('.select_big_list').slideUp();
        });

        $('.select_big_input').on('click', function(){
            if($('.baby_gender_button').hasClass('disabled')) $('.baby_gender_button').removeClass('disabled');
            if($(this).hasClass('error')) $(this).removeClass('error');
            $('.select_big_list').slideUp();
            $(this).next('.select_big_list').slideToggle();
        });

        $('.blood_group_link').on('click', function(e){
            if($('.baby_gender_button').hasClass('disabled')) $('.baby_gender_button').removeClass('disabled');
            $(this).siblings('.active').not($(this)).removeClass('active');
            $(this).addClass('active');
        });

        for(var i = 1970; i < 2000; i++){
            $('.year-list').children('div').append('<div data-val='+i+'>'+i+'</div>');
        }


        function babyGenderResult(){
            $('#result').slideDown();
            $('.baby_gender_button').addClass('disabled');
            var topScroll = $('#result').offset().top - 30;
            $('html,body').stop().animate({scrollTop: topScroll}, 700);
        }

        /* Определение пола по обновлению крови */
        function refreshBlood(){
            var yaer_papa1 = String((($('#brb3_3').val() - $('#brb1_3').val()) / 4).toFixed(2));
            var year_mama1 = String((($('#brb3_3').val() - $('#brb2_3').val()) / 3).toFixed(2));

            var year_papa = parseInt(yaer_papa1.slice(yaer_papa1.length - 2, yaer_papa1.length));
            var year_mama = parseInt(year_mama1.slice(year_mama1.length - 2, year_mama1.length));

            if(year_papa < year_mama){
                var baby_gender = 'boy';
            } else {
                var baby_gender = 'girl';
            }
            if (year_papa == year_mama){
                var baby_gender = 'exception';
            }

            history.pushState(null, null, location.href.replace(/#.*/,'') + '#' + baby_gender);
            localStorage.setItem('baby_gender', baby_gender);
        }
        /* Определение пола по обновлению крови. */

        /* Определение пола по группе крови */
        function bloodGroup(){
            var bglDad = document.querySelector('.blood_group_links.dad > .blood_group_link.active').getAttribute('data-val');
            var bglMother = document.querySelector('.blood_group_links.mother > .blood_group_link.active').getAttribute('data-val');

            if (bglDad == 1 && bglMother == 1 || bglDad == 1 && bglMother == 3 || bglDad == 2 && bglMother == 2 || bglDad == 3 && bglMother == 1 || bglDad == 4 && bglMother == 2) {
                var baby_gender = 'girl';
            } else {
                var baby_gender = 'boy';
            }

            history.pushState(null, null, location.href.replace(/#.*/,'') + '#' + baby_gender);
            localStorage.setItem('baby_gender', baby_gender);
        }
        /* Определение пола по группе крови. */

        /* Определение пола по китайскому методу */
        function chineseTableBabyGender(){
            if (chineseTable[$('#ctb1').val()][$('#ctb2').val()] == 0) {
                var baby_gender = 'girl';
            } else {
                var baby_gender = 'boy';
            }

            history.pushState(null, null, location.href.replace(/#.*/,'') + '#' + baby_gender);
            localStorage.setItem('baby_gender', baby_gender);
        }
        /* Определение пола по китайскому методу. */

        /* Определение пола по японскому методу */
        function japaneseTableBabyGender(){
            $('#jtb1').val();
            $('#jtb2').val();
            $('#jtb3').val();

            var jtf = japaneseTableFirst[parseInt($('#jtb2').val())][parseInt($('#jtb1').val())] - 1;

            var jtsLength = japaneseTableSecond.length;
            for(var jts = 0; jts < jtsLength; jts++){
                if(japaneseTableSecond[jts][jtf] == (parseInt($('#jtb3').val()) + 1)){
                    if (japaneseTableSecond[jts][12] < japaneseTableSecond[jts][13]) {
                        var baby_gender = 'girl';
                    } else {
                        if (japaneseTableSecond[jts][12] == japaneseTableSecond[jts][13]){
                            var baby_gender = 'exception';
                        } else {
                            var baby_gender = 'boy';
                        }
                    }
                    break;
                }
            }

            history.pushState(null, null, location.href.replace(/#.*/,'') + '#' + baby_gender);
            localStorage.setItem('baby_gender', baby_gender);
        }
        /* Определение пола по японскому методу. */


        $('.baby_gender_button').on('click', function(){
            if(!$(this).hasClass('disabled')) {
                var validate = 0;

                $('.select_big_input > input').each(function (indx, element) {
                    if (!$(element).val()) {
                        $(element).parent('.select_big_input').addClass('error');
                        validate = 1;
                        $(element).siblings('.tooltip').fadeIn().delay(2500).fadeOut()
                    }
                });

                if ($('.blood_group_link').length != 0 && $('.blood_group_link.active').length != 2) {
                    validate = 1;
                    $('.blood_group_links').each(function (indx, element) {
                        if($(element).children('.blood_group_link.active').length == 0) $(element).children('.tooltip').fadeIn().delay(2000).fadeOut()
                    });
                }

                if (validate == 0) {

                    /* Blood refresh */
                    if ($(this).hasClass('blood_refresh')) {
                        localStorage.setItem('brb1_1', $('#brb1_1').val());
                        localStorage.setItem('brb1_2', $('#brb1_2').val());
                        localStorage.setItem('brb1_3', $('#brb1_3').val());
                        localStorage.setItem('brb2_1', $('#brb2_1').val());
                        localStorage.setItem('brb2_2', $('#brb2_2').val());
                        localStorage.setItem('brb2_3', $('#brb2_3').val());
                        localStorage.setItem('brb3_1', $('#brb3_1').val());
                        localStorage.setItem('brb3_2', $('#brb3_2').val());
                        localStorage.setItem('brb3_3', $('#brb3_3').val());

                        refreshBlood();
                        location.reload();
                    }

                    /* Japanese table */
                    if ($(this).hasClass('japanese_table')) {

                        localStorage.setItem('jtb1', $('#jtb1').val());
                        localStorage.setItem('jtb2', $('#jtb2').val());
                        localStorage.setItem('jtb3', $('#jtb3').val());

                        japaneseTableBabyGender();
                        location.reload();
                    }

                    /* Chinese table */
                    if ($(this).hasClass('chinese_table')) {

                        localStorage.setItem('ctb1', $('#ctb1').val());
                        localStorage.setItem('ctb2', $('#ctb2').val());

                        chineseTableBabyGender();
                        location.reload();
                    }

                    /* Blood group */
                    if ($(this).hasClass('blood_group')) {
                        localStorage.setItem('bglDad', document.querySelector('.blood_group_links.dad > .blood_group_link.active').getAttribute('data-val'));
                        localStorage.setItem('bglMother', document.querySelector('.blood_group_links.mother > .blood_group_link.active').getAttribute('data-val'));
                        bloodGroup();
                        location.reload();
                    }

//                    babyGenderResult();

                }
            }
        });

    });

})(jQuery);