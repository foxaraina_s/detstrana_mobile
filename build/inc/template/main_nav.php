<!-- Popup main navigation -->
<div id="popup-nav-main" class="popup popup-nav-main popup-left">
    <div class="popup__close"></div>
    <div class="popup__header">
        <form class="form-search">
            <input type="text" name="q" placeholder="Поиск">
        </form>
    </div>
    <div class="popup__body">
        <div class="popup__inner">
            <ul class="popup-nav-main__links">
                <li><a href="#">Статьи</a></li>
                <li><a href="#">Новости</a></li>
                <li><a href="#">Сервисы</a></li>
                <li><a href="#">Консультации</a></li>
                <li><a href="#">Конкурсы</a></li>
                <li><a href="#">Блоги</a></li>
                <li><a href="#">Сообщества</a></li>
                <li><a href="#">Друзья</a></li>
            </ul>

            <div class="popup-nav-main__links-alt">
                <a href="#" class="btn btn-active-flat w-46">Планирование</a>
                <a href="#" class="btn btn-active-flat w-23">Дети</a>
                <a href="#" class="btn btn-active-flat w-23">Роды</a>
                <a href="#" class="btn btn-active-flat w-46">Беременность</a>
            </div>

            <div class="popup-nav-main__social">
                <a href="#" class="icon-fb"></a>
                <a href="#" class="icon-vk"></a>
                <a href="#" class="icon-ok"></a>
            </div>

            <div class="popup-nav-main__bottom-links">
                <a href="#">Полная версия</a>
            </div>
        </div>
    </div>
</div>
<!-- /Popup main navigation --><!-- Popup register -->
<div id="popup-register" class="popup popup-register popup-right slider">
    <div class="popup__close"></div>
    <div class="popup__header">
        <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large">
            <li class="w-50 active"><a href="#popup-register__login">Вход</a></li>
            <li class="w-50"><a href="#popup-register__register">Регистрация</a></li>
        </ul>
    </div>

    <div class="popup__body">
        <!--div style="background: #FFF; height:1px;overflow:hidden;">&nbsp;</div-->
        <div class="tabs popup-register__tabs">

            <div id="popup-register__register" class="tab popup-register__register active">
                <form action="" method="post">

                    <div class="popup-register__block">

                        <div class="form-group popup-register__user">
                            <label class="form-label">Ваше имя</label>
                            <input name="user" type="text" placeholder="Ваше имя" maxlength="20" class="form-control">
                            <div class="form-limit">0 / 20</div>
                            <div class="form-error">Заполните поле</div>
                        </div>

                        <div class="form-group form-group-error popup-register__email">
                            <label class="form-label">Ваш e-mail</label>
                            <input name="name" type="text" placeholder="Ваш e-mail" maxlength="100" class="form-control">
                            <div class="form-limit">0 / 100</div>
                            <div class="form-error">Заполните поле</div>
                        </div>

                        <div class="form-group popup-register__password">
                            <label class="form-label">Ваш пароль</label>
                            <input name="name" type="password" placeholder="Ваш пароль" class="form-control">
                            <div class="form-limit">&nbsp;</div>
                            <div class="form-error">Заполните поле</div>
                        </div>
                    </div>

                    <div class="popup-register__block">
                        <div class="form-group form-group-checkbox">
                            <input type="checkbox" name="is_agree" id="popup-register__register-is-agree">
                            <label for="popup-register__register-is-agree">Я даю согласие на обработку персональных данных и соглашаюсь с <a href="#">пользовательским соглашением</a></label>
                        </div>

                        <div class="form-group form-group-checkbox">
                            <input type="checkbox" name="is_subscribe" id="popup-register__register-is-subscribe">
                            <label for="popup-register__register-is-subscribe">Я хочу получать email-письма о конкурсах и акциях</label>
                        </div>

                        <div class="text-center mb-20"><button class="btn btn-confirm btn-shaded w-70">Зарегистрироваться</button></div>


                        <div class="popup-register__social-title">Или зарегистрируйтесь с помощью</div>

                        <div class="popup-register__social">
                            <a href="#" class="icon-fb"></a>
                            <a href="#" class="icon-vk"></a>
                            <a href="#" class="icon-ok"></a>
                        </div>
                    </div>
                </form>
            </div>


            <div id="popup-register__login" class="tab popup-register__login">
                <form action="" method="post">
                    <div class="popup-register__block">
                        <div class="form-group popup-register__user">
                            <label class="form-label">&nbsp;</label>
                            <input name="user" type="text" placeholder="Ваше имя" maxlength="20" class="form-control">
                            <div class="form-limit">&nbsp;</div>
                            <div class="form-error">Заполните поле</div>
                        </div>

                        <div class="form-group popup-register__password">
                            <label class="form-label">&nbsp;</label>
                            <input name="name" type="password" placeholder="Ваш пароль" class="form-control">
                            <div class="form-limit">&nbsp;</div>
                            <div class="form-error">Заполните поле</div>
                        </div>

                        <div class="text-center mb-20 landscape-only"><button class="btn btn-confirm btn-shaded w-70">Войти</button></div>
                    </div>

                    <div class="popup-register__block">

                        <div class="form-group form-group-checkbox">
                            <input type="checkbox" name="is_agree" value="1" id="popup-register__login-is-agree">
                            <label for="popup-register__login-is-agree">Я даю согласие на обработку персональных данных и соглашаюсь с <a href="#">пользовательским соглашением</a></label>
                        </div>

                        <div class="form-group form-group-checkbox">
                            <input type="checkbox" name="is_subscribe" value="1" id="popup-register__login-is-subscribe">
                            <label for="popup-register__login-is-subscribe">Я хочу получать email-письма о конкурсах и акциях</label>
                        </div>

                        <div class="text-center mb-20 portrait-only"><button class="btn btn-confirm btn-shaded w-70">Войти</button></div>


                        <div class="popup-register__social-title">Или войдите с помощью</div>

                        <div class="popup-register__social mb-20">
                            <a href="#" class="icon-fb"></a>
                            <a href="#" class="icon-vk"></a>
                            <a href="#" class="icon-ok"></a>
                        </div>

                        <div class="text-center"><a href="#" class="btn btn-active-flat">Забыли пароль?</a></div>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>
<!-- /Popup register --><!-- Popup profile -->
<div id="popup-profile" class="popup popup-profile popup-right">
    <i class="popup__close"></i>

    <div class="popup__header">
        <div class="popup-profile__avatar"><img src="img/tmp/userpic1.png"></div>
        <div class="popup-profile__username">Анна Петрова</div>
    </div>

    <div class="popup__body">
        <ul class="popup-profile__links">
            <li class="active"><a href="#">Дневник</a></li>
            <li><a href="#">Новости</a></li>
            <li><a href="#">Сообщения</a> <span class="badge badge-notice popup-profile__messages">8</span></li>
            <li><a href="#">Друзья</a></li>
        </ul>
        <ul class="popup-profile__links">
            <li><a href="#">Сообщества</a></li>
            <li><a href="#">Уведомления</a></li>
            <!--li class="alt"><a href="#">Настройки</a></li>
            <li class="alt"><a href="#">Выход</a></li-->
        </ul>
        <div class="popup-profile__bottom-links">
            <a href="#">Настройки</a><br>
            <a href="#">Выход</a>
        </div>
    </div>
</div>
<!-- /Popup profile --><!-- Popup articles categories -->
<div id="popup-articles" class="popup popup-articles popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Статьи</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Категория статей 1</a></li>
            <li><a href="#">Категория статей 2</a></li>
            <li><a href="#">Категория статей 3</a></li>
            <li><a href="#">Категория статей 4</a></li>
            <li><a href="#">Категория статей 5</a></li>
            <li><a href="#">Категория статей 6</a></li>
            <li><a href="#">Категория статей 7</a></li>
            <li><a href="#">Категория статей 8</a></li>
            <li><a href="#">Категория статей 9</a></li>
            <li><a href="#">Категория статей 10</a></li>
            <li><a href="#">Категория статей 11</a></li>
            <li><a href="#">Категория статей 12</a></li>
            <li><a href="#">Категория статей 13</a></li>
            <li><a href="#">Категория статей 14</a></li>
            <li><a href="#">Категория статей 15</a></li>
            <li><a href="#">Категория статей 16</a></li>
            <li><a href="#">Категория статей 17</a></li>
            <li><a href="#">Категория статей 18</a></li>
            <li><a href="#">Категория статей 19</a></li>
            <li><a href="#">Категория статей 20</a></li>
            <li><a href="#">Категория статей 21</a></li>
            <li><a href="#">Категория статей 22</a></li>
            <li><a href="#">Категория статей 23</a></li>
            <li><a href="#">Категория статей 24</a></li>
            <li><a href="#">Категория статей 25</a></li>
            <li><a href="#">Категория статей 26</a></li>
            <li><a href="#">Категория статей 27</a></li>
            <li><a href="#">Категория статей 28</a></li>
            <li><a href="#">Категория статей 29</a></li>
            <li><a href="#">Категория статей 30</a></li>
            <li><a href="#">Категория статей 31</a></li>
            <li><a href="#">Категория статей 32</a></li>
            <li><a href="#">Категория статей 33</a></li>
            <li><a href="#">Категория статей 34</a></li>
            <li><a href="#">Категория статей 35</a></li>
            <li><a href="#">Категория статей 36</a></li>
            <li><a href="#">Категория статей 37</a></li>
            <li><a href="#">Категория статей 38</a></li>
            <li><a href="#">Категория статей 39</a></li>
            <li><a href="#">Категория статей 40</a></li>
            <li><a href="#">Категория статей 41</a></li>
            <li><a href="#">Категория статей 42</a></li>
            <li><a href="#">Категория статей 43</a></li>
            <li><a href="#">Категория статей 44</a></li>
            <li><a href="#">Категория статей 45</a></li>
            <li><a href="#">Категория статей 46</a></li>
            <li><a href="#">Категория статей 47</a></li>
            <li><a href="#">Категория статей 48</a></li>
            <li><a href="#">Категория статей 49</a></li>
            <li><a href="#">Категория статей 50</a></li>
            <li><a href="#">Категория статей 51</a></li>
            <li><a href="#">Категория статей 52</a></li>
            <li><a href="#">Категория статей 53</a></li>
            <li><a href="#">Категория статей 54</a></li>
            <li><a href="#">Категория статей 55</a></li>
            <li><a href="#">Категория статей 56</a></li>
            <li><a href="#">Категория статей 57</a></li>
            <li><a href="#">Категория статей 58</a></li>
            <li><a href="#">Категория статей 59</a></li>
            <li><a href="#">Категория статей 60</a></li>
            <li><a href="#">Категория статей 61</a></li>
            <li><a href="#">Категория статей 62</a></li>
            <li><a href="#">Категория статей 63</a></li>
            <li><a href="#">Категория статей 64</a></li>
            <li><a href="#">Категория статей 65</a></li>
            <li><a href="#">Категория статей 66</a></li>
            <li><a href="#">Категория статей 67</a></li>
            <li><a href="#">Категория статей 68</a></li>
            <li><a href="#">Категория статей 69</a></li>
            <li><a href="#">Категория статей 70</a></li>
            <li><a href="#">Категория статей 71</a></li>
            <li><a href="#">Категория статей 72</a></li>
            <li><a href="#">Категория статей 73</a></li>
            <li><a href="#">Категория статей 74</a></li>
            <li><a href="#">Категория статей 75</a></li>
            <li><a href="#">Категория статей 76</a></li>
            <li><a href="#">Категория статей 77</a></li>
            <li><a href="#">Категория статей 78</a></li>
            <li><a href="#">Категория статей 79</a></li>
            <li><a href="#">Категория статей 80</a></li>
            <li><a href="#">Категория статей 81</a></li>
            <li><a href="#">Категория статей 82</a></li>
            <li><a href="#">Категория статей 83</a></li>
            <li><a href="#">Категория статей 84</a></li>
            <li><a href="#">Категория статей 85</a></li>
            <li><a href="#">Категория статей 86</a></li>
            <li><a href="#">Категория статей 87</a></li>
            <li><a href="#">Категория статей 88</a></li>
            <li><a href="#">Категория статей 89</a></li>
            <li><a href="#">Категория статей 90</a></li>
            <li><a href="#">Категория статей 91</a></li>
            <li><a href="#">Категория статей 92</a></li>
            <li><a href="#">Категория статей 93</a></li>
            <li><a href="#">Категория статей 94</a></li>
            <li><a href="#">Категория статей 95</a></li>
            <li><a href="#">Категория статей 96</a></li>
            <li><a href="#">Категория статей 97</a></li>
            <li><a href="#">Категория статей 98</a></li>
            <li><a href="#">Категория статей 99</a></li>
            <li><a href="#">Категория статей 100</a></li>
        </ul>
    </div>
</div>
<!-- /Popup articles categories --><!-- Popup articles news -->
<div id="popup-news" class="popup popup-news popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Новости</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Планирование</a></li>
            <li><a href="#">Беременность</a></li>
            <li><a href="#">Роды</a></li>
            <li><a href="#">Малыш 0&ndash;1</a></li>
            <li><a href="#">Ребенок 3&ndash;7</a></li>
            <li><a href="#">Дети 7&ndash;10</a></li>
            <li><a href="#">Родители</a></li>
        </ul>
    </div>
</div>
<!-- /Popup articles news -->
<!--div class="overlay"></div-->