<!-- Footer -->
<footer>
    <ul class="footer-links">
        <li><a href="#">Реклама</a></li>
        <li><a href="#">Контакты</a></li>
        <li><a href="#">Правообладателям</a></li>
        <li><a href="#">Пользовательское соглашение</a></li>
    </ul>
    <div class="footer-copy">
        &copy; 2017 Детстрана. Все права защищены.
    </div>
</footer>
<!-- /Footer -->
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="js/main.js"></script>
<script src="js/gender_js.js"></script>