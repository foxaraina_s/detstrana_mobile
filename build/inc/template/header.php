<div class="wrapper">
    <header>
        <div class="header__inner">
            <a href="#" data-target="#popup-profile" class="header__profile">
            </a>
            <a href="#" data-target="#popup-register" class="header__logo"></a>
            <a href="#" data-target="#popup-nav-main" class="header__menu"></a>
        </div>
    </header>
    <!-- Search -->
    <div class="search">
        <form class="form-search">
            <input type="text" name="q" placeholder="Поиск">
        </form>
    </div>
    <!-- /Search -->