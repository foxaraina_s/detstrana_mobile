<div class="articles">
    <div class="articles__list">
        <div class="article">
            <div class="article__image">
                <a href="#"><img src="img/tmp/article1.jpg"></a>
            </div>

            <div class="article__content">
                <a href="#" class="article__category">Психология</a>
                <a href="#" class="article__title">В чем мама всегда виновата</a>
            </div>

            <div class="icon-group article__icons">
                <a href="#" class="icon-info icon-info-views">120</a>
                <a href="#" class="icon-info icon-info-comments">3</a>
                <a href="#" class="icon-info icon-info-likes">3</a>
            </div>
        </div>

        <div class="article">
            <div class="article__mark">Партнёрский материал</div>

            <div class="article__image">
                <a href="#"><img src="img/tmp/article2.jpg"></a>
            </div>

            <div class="article__content">
                <a href="#" class="article__title">8 признаков овуляции, которые можно увидеть или почуствовать</a>
            </div>

            <div class="icon-group article__icons">
                <a href="#" class="icon-info icon-info-views">120</a>
                <a href="#" class="icon-info icon-info-comments">3</a>
            </div>
        </div>

    </div>

    <div class="text-center"><button class="btn btn-active-flat">Больше статей</button></div>
</div>