<div class="content content-user-articles scroll-top pt-20">
	
	<ul class="page-breadcrumbs">
		<li class="back">вернуться в статьи для родителей</li>
	</ul>
	
	<h1 class="mb-40">Статьи для родителей от автора Наталья Ветрова</h1>

	<button data-target="#popup-articles" class="btn btn-popup btn-white mb-20">Разделы статей</button>

	<div class="users">
		<div class="users__list mb-20">
			<div class="user user-online user-active">
				<div class="user__header clickable">
					<div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
					<div class="user__name">Наталья Ветрова</div>
					<div class="user__about">Автор</div>
				</div>
				<div class="user__body">
					<div class="user__body-inner">
						<em>За время пребывания в «Детстране» Наталья написалa для вас 2 статьи!</em>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include 'inc/block/search.php'; ?>

	<?php $mb = 10; include 'inc/block/tabs-slider.php'; ?>

	<div class="articles mb-20">
	<?php include 'inc/block/articles.php' ?>
	</div>

</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>