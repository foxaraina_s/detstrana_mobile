<div id="popup-trimestrs" class="popup popup-services popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Выберите триместр</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Триместр 1</a></li>
            <li><a href="#">Триместр 2</a></li>
            <li><a href="#">Триместр 3</a></li>
        </ul>
    </div>
</div>
<div id="popup-content" class="popup popup-services popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Содержание</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Раздел 1</a></li>
            <li><a href="#">Раздел 2</a></li>
            <li><a href="#">Раздел 3</a></li>
            <li><a href="#">Раздел 4</a></li>
            <li><a href="#">Раздел 5</a></li>
            <li><a href="#">Раздел 6</a></li>
            <li><a href="#">Раздел 7</a></li>
            <li><a href="#">Раздел 8</a></li>
            <li><a href="#">Раздел 9</a></li>
        </ul>
    </div>
</div>
<section class="service service-birthcalendar pt-20 scroll-top">
    <ul class="page-breadcrumbs">
        <li class="back"><a href="#">Вернуться в календарь беременности</a></li>
    </ul>
    <h1 class="mb-40">1 триместр беременности</h1>
    <div class="mb-20">
        <button data-target="#popup-trimestrs" class="btn btn-popup btn-white">Выберите триместр</button>
    </div>
    <div class="mb-20">
        <button data-target="#popup-content" class="btn btn-popup btn-white">Содержание</button>
    </div>

    <div class="birthcalendar-inner">

            <div class="slider birthcalendar-inner__slider">
                <div class="slider-changeable slider-wrapper">
                    <div class="slides">
                        <div class="slide birthcalendar-inner__slide">
                            <div class="birthcalendar-inner__slide-img">
                                <img src="img/birthcalendar/st.png" style="max-width: 50%;">
                            </div>
                            <div class="birthcalendar-inner__slide-title">
                                1 месяц беременности
                            </div>

                        </div>
                        <div class="slide birthcalendar-inner__slide">
                            <div class="birthcalendar-inner__slide-img">
                                <img src="img/birthcalendar/st.png" style="max-width: 50%;">
                            </div>
                            <div class="birthcalendar-inner__slide-title">
                                1 месяц беременности
                            </div>

                        </div>
                        <div class="slide birthcalendar-inner__slide">
                            <div class="birthcalendar-inner__slide-img">
                                <img src="img/birthcalendar/st.png" style="max-width: 50%;">
                            </div>
                            <div class="birthcalendar-inner__slide-title">
                                1 месяц беременности
                            </div>

                        </div>
                        <div class="slide birthcalendar-inner__slide">
                            <div class="birthcalendar-inner__slide-img">
                                <img src="img/birthcalendar/st.png" style="max-width: 50%;">
                            </div>
                            <div class="birthcalendar-inner__slide-title">
                                1 месяц беременности
                            </div>

                        </div>
                        </div>
                    </div>
                <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                </ul>

                </div>

            </div>
    <div class="inner-content mb-20">
        <div class="service__text mb-20">
            1 триместр начинается с первой недели и заканчивается тринадцатой неделей вынашивания малыша. Он практически
            полностью охватывает первые три месяца беременности. Животик в течение этого времени почти не увеличивается, его
            округление начнётся только во втором триместре, когда матка вместе с малышом начнут интенсивно расти. Первый
            триместр начинается даже не с зачатия, а с событий, которые предшествуют встрече сперматозоида и яйцеклетки.
            Иными словами, в момент старта первого триместра до зарождения малыша далековато.
        </div>
        <div class="img__block text-center mb-20">
            <img src="img/birthcalendar/in1.png" class="img-responsive">
        </div>
        <h3 class="mb-20 service__section-title">Признаки и симптомы беременности</h3>
        <div class="service__text">
            <p>Современные представительницы прекрасного пола чаще всего узнают о своём интересном положении где-то в середине
                первого триместра, то есть тогда, когда признаки зарождения новой жизни в животе уже сложно не заметить.
                Впрочем, распознать беременность можно и раньше. Проверенный способ — измерение базальной температуры.</p>
            <div class="inner-list">
                <ul>
                    <li>ректально;</li>
                    <li>утром;</li>
                    <li>до подъема с кровати.</li>
                </ul>
            </div>

            <p>Она повышается тогда, когда у девушки наступает овуляция. Молодые мамочки, которые очень хотят ребёночка,
                наверняка знают всё о базальной температуре тела. Те же, кто не работает над пополнением семьи целенаправленно,
                возможно, даже о ней не слышали. В момент овуляции базальная температура повышается до 37,0-37,2 градусов по
                шкале Цельсия. Если оплодотворение произойдёт, то температура останется таковой вплоть до 14 недели
                беременности. <a href="#">Здесь вы найдёте всё, что вам нужно знать о базальной температуре.</a> А <a href="#">здесь сможете подробно
                    ознакомиться с правилами её измерения.</a></p>
            <p>Впрочем, показатели базальной температуры — не единственные признаки, которые могут помочь распознать беременность на ранних</p>
            <div class="inner-list">
                <ul>
                    <li>главный признак интересного положения в первом триместре — не начавшиеся месячные (если срок подошёл, а
                        менструация никак не начинается, можете смело покупать тест на беременность и делать его);
                    </li>
                    <li>
                        комплекс симптомов, которые указывают на то, что у будущей мамы начал меняться гормональный фон
                        (кружение головы, сонливость, раздражительность, упадок сил).
                    </li>
                </ul>
            </div>
            <a href="#">Ознакомьтесь с нашей тематической статьёй о ранних признаках беременности, чтобы узнать больше.</a>
        </div>
    </div>

    <div class="slider nav-tabs-slider mb-20 block-shaded">
        <div class="slider-wrapper slider-changeable">
            <ul class="nav nav-tabs nav-tabs-font--sm nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100" style="white-space: nowrap;">
                <li class="slide w-50 active"><a href="#">Твой малыш</a></li>
                <li class="slide w-50"><a href="#">Ты </a></li>
                <li class="slide w-50"><a href="#">Что ты делаешь?</a></li>
                <li class="slide w-50"><a href="#">Что ты думаешь?</a></li>
            </ul>
        </div>
    </div>
    <div class="nav-tabs-container mb-40">
        <div class="service__text mb-20">
            То, как выглядит эмбрион в первом триместре, всецело зависит от того, о какой неделе идёт речь. К примеру, на
            первой акушерской неделе беременности эмбриона ещё и вовсе не существует, ведь зачатия не произошло. После
            слияния яйцеклетки и сперматозоида образовывается зигота, которая начинает движение от места встречи женской и
            мужской половой клетки к матке. Приблизительно на пятые сутки с момента оплодотворения эмбрион попадёт в полость
            матки и начнёт своё активное развитие. У него начнут формироваться органы и системы, и очень важно, чтобы
            закладка их была максимально правильной и своевременной. Пока что ваш малыш — песчинка в море. И лишь к концу
            первого триместра эмбриончик будет весить 13 граммов и достигнет роста 61 миллиметр.
        </div>
        <h3 class="mb-20 service__section-title">Определение пола</h3>
        <div class="service__text">
            Пока что определить, каким будет пол малыша, невозможно. Однако вы должны знать, что Природа уже всё расставила
            по своим местам. Если на встречу с яйцеклеткой быстрее всех успеет сперматозоид с хромосомой Х, то у вас через 9
            месяцев родится девочка. Если же более прытким и проворным будет сперматозоид с хромосомой Y, то вы станете
            мамой мальчика. Но не исключено, что у вас родится два однополых или два (а то и больше) разнополых малыша.
            Хотите попытаться определить пол будущего ребёночка по дате зачатия?<a href="#">Следуйте изложенным в статье рекомендациям.</a>
        </div>
    </div>
    <h3 class="mb-20 service__section-title">На 1 триместре беременности полезно:</h3>
    <ul class="nav nav-tabs nav-tabs-bordered w-100 mb-10 nav-tabs-font--sm">
        <li class="w-50 active"><a href="#">Читать блоги</a></li>
        <li class="w-50"><a href="#">Читать консультации</a></li>
    </ul>
    <div class="entries mb-40">
        <div class="entries__list mb-20">
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:58</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                            <div class="slide active"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li class="active"><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">120</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">1</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open entry__comment-open--green ">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="text-center">
            <button class="btn btn-active-flat">Больше записей</button>
        </div>
    </div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->