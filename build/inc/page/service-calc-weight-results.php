
<section class="service service-birthcalendar pt-20 scroll-top">
    <ul class="page-breadcrumbs">
        <li><a href="#">Сервисы</a></li>
        <li><a href="#">Калькулятор расчет веса для беременных</a></li>
        <li><span>32 неделя беременности</span></li>
    </ul>
    <h1 class="mb-20">Вес на 32 неделе беременности</h1>
    <div class="weight mb-20 block-shaded">
        <h2>Поздравляем, для 32-й недели беременности прибавка веса в пределах нормы</h2>
        <div class="weight-scale">
            <div class="weight-scale__item weight-scale__item--less">
                <div class="weight-scale__title">меньше 10 кг</div>
                <div class="weight-scale__coclus">Недобор</div>
            </div>
            <div class="weight-scale__item weight-scale__item--norm">
                <div class="weight-scale__label">Ваша прибавка<br><span>10 кг</span></div>
                <div class="weight-scale__title">10 кг</div>
                <div class="weight-scale__coclus">Норма</div>
            </div>
            <div class="weight-scale__item weight-scale__item--much">
                <div class="weight-scale__title">свыше 10 кг</div>
                <div class="weight-scale__coclus">Перебор</div>
            </div>
        </div>
        <h3 class="mb-10">Ваш индекс массы тела (BMI)</h3>
        <div class="bmi-num">20.20</div>
        <div class="bmi-scale">
            <div class="bmi-scale__item bmi-scale__item--small">
                BMI до 19,8 худощавое телосложение
            </div>
            <div class="bmi-scale__item bmi-scale__item--middle active">
                BMI 19,8 среднее телосложение
            </div>
            <div class="bmi-scale__item bmi-scale__item--big">
                BMI от 26 крупное телосложение
            </div>
        </div>
        <div class="weight-body-part">
            <div class="weight-body-part__item">
                <div class="weight-body-part__name">Молочные железы</div>
                <div class="weight-body-part__kg">400 г</div>
            </div>
            <div class="weight-body-part__item">
                <div class="weight-body-part__name">Жировая ткань</div>
                <div class="weight-body-part__kg">2.4 кг</div>
            </div>
            <div class="weight-body-part__item">
                <div class="weight-body-part__name">Плод</div>
                <div class="weight-body-part__kg">1.7 кг</div>
            </div>
            <div class="weight-body-part__item">
                <div class="weight-body-part__name">Плацента</div>
                <div class="weight-body-part__kg">400 г</div>
            </div>
            <div class="weight-body-part__item">
                <div class="weight-body-part__name">ОЦК</div>
                <div class="weight-body-part__kg">1.4 кг</div>
            </div>
            <div class="weight-body-part__item">
                <div class="weight-body-part__name">Тканевая жидкость</div>
                <div class="weight-body-part__kg">1.4 кг</div>
            </div>
            <div class="weight-body-part__item">
                <div class="weight-body-part__name">Матка</div>
                <div class="weight-body-part__kg">1.0 кг</div>
            </div>
            <div class="weight-body-part__item">
                <div class="weight-body-part__name">Околоплодные воды</div>
                <div class="weight-body-part__kg"></div>
            </div>
            <div class="weight-body-part__item">
                <div class="weight-body-part__name">Отёчная жидкость</div>
                <div class="weight-body-part__kg">300 г</div>
            </div>
        </div>
    </div>
    <div class="inner-block inner-block--shadow mb-20">
        <div class="inner-block__doctor">
            <div class="inner-block__doctor-image">
                <img src="img/birthcalendar/dc1.png" style="width: 64px;">
            </div>
            <div class="inner-block__doctor-content">
                <div class="inner-block__doctor-title">
                    Коцарев Евгений Александрович
                </div>
                <div class="inner-block__doctor-text">
                    Акушер-гинеколог, гинеколог-эндокринолог Центр иммунологии и репродукции
                </div>
            </div>
        </div>
    </div>
    <h2 class="mb-20">Норма веса на 32 неделе беременности</h2>
    <div class="service__text">
        <p>Срок вашей беременности уже восемь месяцев! Сверьте, какой должен быть вес на сроке беременности в 32 недели!
            Прибавка веса составляет от 6,4 кг (при ИМТ более 26) до 11,3 кг (при ИМТ менее 19,8). Рассчитать свой вес и
            индивидуальную прибавку массы тела вы всегда можете с калькулятором набора веса при беременности. Какая
            норма веса ребенка на 32-й неделе беременности? Узнайте, какой должен быть вес плода - около 1600-1700
            граммов. У него сейчас становятся пухленькими ручки и ножки. Рост вашего малыша- 39-40 см.

        <p>Чтобы рассчитать норму прибавки по неделям, укажите свой вес, рост и срок беременности</p>
    </div>

    <div class="weight-form">
        <form method="post">
            <div class="weight-form__week">
                <div class="weight-form__title">Неделя беременности:</div>
                <div class="form-group mb-20">
                    <select name="wp" placeholder="Неделя" class="form-select">
                        <option>Неделя</option>
                        <option value="0">1 неделя</option>
                        <option value="1">2 неделя</option>
                        <option value="2">3 неделя</option>
                        <option value="3">4 неделя</option>
                        <option value="4">5 неделя</option>
                        <option value="5">6 неделя</option>
                        <option value="6">7 неделя</option>
                        <option value="7">8 неделя</option>
                        <option value="8">9 неделя</option>
                        <option value="9">10 неделя</option>
                        <option value="10">11 неделя</option>
                        <option value="11">12 неделя</option>
                        <option value="12">13 неделя</option>
                        <option value="13">14 неделя</option>
                        <option value="14">15 неделя</option>
                        <option value="15">16 неделя</option>
                        <option value="16">17 неделя</option>
                        <option value="17">18 неделя</option>
                        <option value="18">19 неделя</option>
                        <option value="19">20 неделя</option>
                        <option value="20">21 неделя</option>
                        <option value="21">22 неделя</option>
                        <option value="22">23 неделя</option>
                        <option value="23">24 неделя</option>
                        <option value="24">25 неделя</option>
                        <option value="25">26 неделя</option>
                        <option value="26">27 неделя</option>
                        <option value="27">28 неделя</option>
                        <option value="28">29 неделя</option>
                        <option value="29">30 неделя</option>
                        <option value="30">31 неделя</option>
                        <option value="31">32 неделя</option>
                        <option value="32">33 неделя</option>
                        <option value="33">34 неделя</option>
                        <option value="34">35 неделя</option>
                        <option value="35">36 неделя</option>
                        <option value="36">37 неделя</option>
                        <option value="37">38 неделя</option>
                        <option value="38">39 неделя</option>
                        <option value="39">40 неделя</option>
                    </select>
                </div>

            </div>
            <div class="weight-form__growth">
                <div class="weight-form__title">Рост (см):</div>
                <div class="form-group mb-20">
                    <select name="wp" placeholder="см" class="form-select">
                        <option>см</option>
                    </select>
                </div>

            </div>
            <div class="weight-form__weight-b">
                <div class="weight-form__title">Вес до берем-сти (кг):</div>
                <div class="form-group mb-20">
                    <select name="wp" placeholder="кг" class="form-select">
                        <option>кг</option>
                    </select>
                </div>

            </div>
            <div class="weight-form__weight-a">
                <div class="weight-form__title">Вес сейчас (кг):</div>
                <div class="form-group mb-20">
                    <select name="wp" placeholder="кг" class="form-select">
                        <option>кг</option>
                    </select>
                </div>

            </div>
            <a class="btn btn-confirm btn-shaded w-100">Рассчитать</a>
        </form>
    </div>

    <h2 class="mb-20 mlr-16">Календарь анализов и обследований при беременности</h2>
    <h3>Расчет прибавки веса по неделям беременности:</h3>
    <div class="analysis-calendar mb-40">
        <div class="analysis-calendar__item">
            <div class="analysis-calendar__header">
                <div class="analysis-calendar__title">
                    I триместр
                </div>
            </div>
            <div class="analysis-calendar__body">
                <div class="slider analysis-calendar-slider mb-20">
                    <div class="slider-wrapper slider-changeable">
                        <ul class="nav calendar-analysis-nav analysis-calendar-list analysis-calendar-bordered analysis-calendar-scrollable analysis-calendar__first w-100">
                            <li class="slide"><a href="#">1</a></li>
                            <li class="slide"><a href="#">2</a></li>
                            <li class="slide"><a href="#">3</a></li>
                            <li class="slide"><a href="#">4</a></li>
                            <li class="slide"><a href="#">5</a></li>
                            <li class="slide"><a href="#">6</a></li>
                            <li class="slide"><a href="#">7</a></li>
                            <li class="slide"><a href="#">8</a></li>
                            <li class="slide"><a href="#">9</a></li>
                            <li class="slide"><a href="#">10</a></li>
                            <li class="slide"><a href="#">11</a></li>
                            <li class="slide"><a href="#">12</a></li>
                            <li class="slide"><a href="#">13</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="analysis-calendar__item">
            <div class="analysis-calendar__header">
                <div class="analysis-calendar__title">
                    II триместр
                </div>
            </div>
            <div class="analysis-calendar__body">
                <div class="slider analysis-calendar-slider mb-20">
                    <div class="slider-wrapper slider-changeable">
                        <ul class="nav calendar-analysis-nav analysis-calendar-list analysis-calendar-bordered analysis-calendar-scrollable analysis-calendar__second w-100">
                            <li class="slide"><a href="#">14</a></li>
                            <li class="slide"><a href="#">15</a></li>
                            <li class="slide"><a href="#">16</a></li>
                            <li class="slide"><a href="#">17</a></li>
                            <li class="slide"><a href="#">18</a></li>
                            <li class="slide"><a href="#">19</a></li>
                            <li class="slide"><a href="#">20</a></li>
                            <li class="slide"><a href="#">21</a></li>
                            <li class="slide"><a href="#">22</a></li>
                            <li class="slide"><a href="#">23</a></li>
                            <li class="slide"><a href="#">24</a></li>
                            <li class="slide"><a href="#">25</a></li>
                            <li class="slide"><a href="#">26</a></li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="analysis-calendar__item">
            <div class="analysis-calendar__header">
                <div class="analysis-calendar__title">
                    III триместр
                </div>
            </div>
            <div class="analysis-calendar__body">
                <div class="slider analysis-calendar-slider mb-20">
                    <div class="slider-wrapper slider-changeable">
                        <ul class="nav calendar-analysis-nav analysis-calendar-list analysis-calendar-bordered analysis-calendar-scrollable analysis-calendar__third w-100">
                            <li class="slide"><a href="#">27</a></li>
                            <li class="slide"><a href="#">28</a></li>
                            <li class="slide"><a href="#">29</a></li>
                            <li class="slide"><a href="#">30</a></li>
                            <li class="slide"><a href="#">31</a></li>
                            <li class="slide"><a href="#">32</a></li>
                            <li class="slide"><a href="#">33</a></li>
                            <li class="slide"><a href="#">34</a></li>
                            <li class="slide"><a href="#">35</a></li>
                            <li class="slide"><a href="#">36</a></li>
                            <li class="slide"><a href="#">37</a></li>
                            <li class="slide"><a href="#">38</a></li>
                            <li class="slide"><a href="#">39</a></li>
                            <li class="slide"><a href="#">40</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->