
<div id="popup-doctors" class="popup popup-center popup-doctors popup-open">
    <div class="popup__close"></div>
    <div class="popup__header">Врачи</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Аллерголог детский</a></li>
            <li><a href="#">Андролог</a></li>
            <li><a href="#">Астролог</a></li>

        </ul>
        <ul class="filter-abc-linear">
            <li><a href="#">А</a></li>
            <li><a href="#">В</a></li>
            <li><a href="#">Ш</a></li>
            <li><a href="#">Н</a></li>
        </ul>
    </div>
</div>
<div class="content content-consulting pt-10 scroll-top">

    <h1 class="mb-40">Онлайн-консультации</h1>

    <a href="#" class="btn btn-active btn-shaded w-93 mb-20 block-center">Задать вопрос экспертам</a>

    <button data-target="#popup-doctors" class="btn btn-popup btn-white mb-20">Врачи</button>

    <h2 class="mb-20">Популярные консультации</h2>

    <div class="search mb-20">
        <form class="form-search">
            <input type="text" name="q" placeholder="Поиск по вопросам">
        </form>
    </div>

    <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100 mb-20">
        <li class="w-50 active"><a href="#">Новое</a></li>
        <li class="w-50"><a href="#">Популярное</a></li>
    </ul>


    <div class="questions mb-40">
        <div class="questions__list">

            <div class="question">
                <div class="question__header">
                    <div class="question__avatar">
                        <img src="img/tmp/userpic1.png">
                    </div>
                    <div class="question__author">Спрашивает <strong>Рада Мельникова</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Гиперлактация</div>
                    <div class="question__text">
                        Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа,
                        последние две ночи вообще по 5 часов
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                    <a href="#" class="btn btn-confirm-flat block-center">Комментировать</a>
                </div>
            </div>

            <div class="question">
                <div class="question__header">
                    <div class="question__avatar"></div>
                    <div class="question__author"><strong>Анонимный вопрос</strong></div>
                    <div class="question__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="question__body">
                    <div class="question__category">Специалист по ГВ</div>
                    <div class="question__title">Ссадина ранка</div>
                    <div class="question__text">
                        ЗДРАВСТВУЙТЕ! ребенок четыре дня назад упал на ручку. На ладошке появился пузырек плотный. Обработали зеленкой.
                    </div>
                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Смотреть ответ</a>
                    <a href="#" class="btn btn-confirm-flat block-center">Комментировать</a>
                </div>
            </div>

        </div>	</div>

    <ul class="pagination text-center mb-20">
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">...</a></li>
    </ul></div>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->