
<div class="content content-community scroll-top pt-20">
    <ul class="page-breadcrumbs">
        <li><a href="#">Каталог сообществ</a></li>
        <li><a href="#">Планирование и беременность</a></li>
        <li><span>Подготовка к зачатию</span></li>
    </ul>
    <h1 class="mb-20">Подготовка к зачатию</h1>
    <div class="communities mb-20">
        <div class="community-one mb-10">
            <div class="community__image">
                <img src="img/tmp/community1.png">
            </div>
            <div class="community__body">
                <div class="comment__text">
                    Как здорово гулять по парку с коляской, пеленать малыша и с упоением
                    слушать, как он гулит. Скоро все будет! А пока - готовимся и постигаем
                    науку измерения базальной температуры, узнаем, как зачать мальчика, и
                    какие тесты на овуляцию лучше!
                </div>
                <div class="community__buttons">
                    <button class="btn btn-confirm btn-shaded w-46">Создать запись</button>
                    <button class="btn btn-confirm btn-shaded w-46">Вступить</button>
                </div>
                <button class="btn btn-active-flat btn-center">Правила сообщества</button>

            </div>

        </div>
    </div>
    <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-scrollable block-shaded w-100 mb-20">
        <li class="w-50 "><a href="#">Записей (71)</a></li>
        <li class="w-50 active"><a href="#">Участников (10528)</a></li>
    </ul>
    <form class="form-search mb-20">
        <input type="text" name="city" value="" placeholder="Начните вводить имя">
    </form>
    <button data-target="#popup-user-filter" class="btn btn-popup btn-white mb-40">Показать фильтр</button>
    <form class="users-filter-form mb-20 w-23 block-center nowrap">
        <div class="form-group form-group-checkbox text-center">
            <input type="checkbox" name="is-online" id="users-filter__is-online">
            <label for="users-filter__is-online" class="form-label-large">В сети</label>
        </div>
    </form>
    <div class="text-grey caption-small mlr-16 mb-10">Люди, которые вам могут быть интересны:</div>
    <div class="users">
    <div class="users__list mb-20">
        <div class="user user-clickable user-online block-shaded mb-10">
            <div class="user__header clickable">
                <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                <div class="user__name">Рада Мельникова</div>
                <div class="user__about">
                    День рождения: 32 февраля<br>
                    Город: Сургут
                </div>
            </div>
            <div class="user__body">
                <div class="user__body-inner">
                    <div class="text-grey caption-small mb-10">Дети:</div>
                    <div class="user__children">
                        <a href="#" class="user__child user__child--boy">7 лет</a>
                        <a href="#" class="user__child user__child--girl">9 лет</a>
                    </div>

                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                    <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                </div>
            </div>
        </div>
        <div class="user user-clickable user-online block-shaded mb-10">
            <div class="user__header clickable">
                <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                <div class="user__name">Рада Мельникова</div>
                <div class="user__about">
                    День рождения: 32 февраля<br>
                    Город: Сургут
                </div>
            </div>
            <div class="user__body">
                <div class="user__body-inner">
                    <div class="text-grey caption-small mb-10">Дети:</div>
                    <div class="user__children">
                        <a href="#" class="user__child user__child--boy">7 лет</a>
                        <a href="#" class="user__child user__child--girl">9 лет</a>
                    </div>

                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                    <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                </div>
            </div>
        </div>
        <div class="user user-clickable user-online block-shaded user-active  mb-10">
            <div class="user__header clickable">
                <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                <div class="user__name">Рада Мельникова</div>
                <div class="user__about">
                    День рождения: 32 февраля<br>
                    Город: Сургут
                </div>
            </div>
            <div class="user__body">
                <div class="user__body-inner">
                    <div class="text-grey caption-small mb-10">Дети:</div>
                    <div class="user__children">
                        <a href="#" class="user__child user__child--boy">7 лет</a>
                        <a href="#" class="user__child user__child--girl">9 лет</a>
                    </div>

                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                    <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                </div>
            </div>
        </div>
        <div class="user user-clickable user-online block-shaded  mb-10">
            <div class="user__header clickable">
                <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                <div class="user__name">Рада Мельникова</div>
                <div class="user__about">
                    День рождения: 32 февраля<br>
                    Город: Сургут
                </div>
            </div>
            <div class="user__body">
                <div class="user__body-inner">
                    <div class="text-grey caption-small mb-10">Дети:</div>
                    <div class="user__children">
                        <a href="#" class="user__child user__child--boy">7 лет</a>
                        <a href="#" class="user__child user__child--girl">9 лет</a>
                    </div>

                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                    <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                </div>
            </div>
        </div>
        <div class="user user-clickable user-online block-shaded mb-10">
            <div class="user__header clickable">
                <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                <div class="user__name">Рада Мельникова</div>
                <div class="user__about">
                    День рождения: 32 февраля<br>
                    Город: Сургут
                </div>
            </div>
            <div class="user__body">
                <div class="user__body-inner">
                    <div class="text-grey caption-small mb-10">Дети:</div>
                    <div class="user__children">
                        <a href="#" class="user__child user__child--boy">7 лет</a>
                        <a href="#" class="user__child user__child--girl">9 лет</a>
                    </div>

                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                    <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                </div>
            </div>
        </div>
        <div class="user user-clickable user-online block-shaded  mb-10">
            <div class="user__header clickable">
                <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                <div class="user__name">Рада Мельникова</div>
                <div class="user__about">
                    День рождения: 32 февраля<br>
                    Город: Сургут
                </div>
            </div>
            <div class="user__body">
                <div class="user__body-inner">
                    <div class="text-grey caption-small mb-10">Дети:</div>
                    <div class="user__children">
                        <a href="#" class="user__child user__child--boy">7 лет</a>
                        <a href="#" class="user__child user__child--girl">9 лет</a>
                    </div>

                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                    <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                </div>
            </div>
        </div>
        <div class="user user-clickable user-online block-shaded  mb-10">
            <div class="user__header clickable">
                <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                <div class="user__name">Рада Мельникова</div>
                <div class="user__about">
                    День рождения: 32 февраля<br>
                    Город: Сургут
                </div>
            </div>
            <div class="user__body">
                <div class="user__body-inner">
                    <div class="text-grey caption-small mb-10">Дети:</div>
                    <div class="user__children">
                        <a href="#" class="user__child user__child--boy">7 лет</a>
                        <a href="#" class="user__child user__child--girl">9 лет</a>
                    </div>

                    <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                    <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                </div>
            </div>
        </div>
    </div>
    </div>


</div>







<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->
<script>
    $('.users').on('click', '.user.user-clickable .user__header', function() {
        $(this)
            .parents('.user')
            .eq(0)
            .toggleClass('user-active')
    });
</script>