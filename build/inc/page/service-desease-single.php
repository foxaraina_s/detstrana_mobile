
<section class="service service-disease">
    <ul class="page-breadcrumbs">
        <li><a href="#">Сервисы</a></li>
        <li><a href="#">Справочник болезней</a></li>
        <li><span>Алкогольное опьянение</span></li>
    </ul>
    <h1 class="mb-20">Алкогольное опьянение при беременности</h1>
    <div class="inner-block inner-block--shadow mb-20">
        <div class="inner-block__blockquote">
            <p>Алкогольное опьянение – это состояние организма, возникающее под действием спиртных напитков и
                сопровождающееся расстройством поведенческих, психологических и физиологических реакций. Применительно к
                детям, данное состояние имеет отличительную особенность: организма ребёнка очень чувствителен к
                спиртному, в связи с чем даже малые дозы могут вызвать серьёзные нарушения со стороны здоровья.
        </div>

    </div>


    <div class="slider nav-tabs-slider mb-20 block-shaded">
        <div class="slider-wrapper slider-changeable">
            <div class="nav-tabs-container slides">
                <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100"
                    style="white-space: nowrap;">
                    <li class="slide w-50"><a href="#">Беременные</a></li>
                    <li class="slide w-50 active"><a href="#">Кормящие мамы</a></li>
                    <li class="slide w-50"><a href="#">Дети</a></li>
                </ul>
            </div>
        </div>
        <a href="#" class="slider-rewind"></a><a href="#" class="slider-forward"></a>
    </div>
    <ul class="nav service__years">
        <li><a href="#">Причины</a></li>
        <li><a href="#">Симптомы</a></li>
        <li class="active"><a href="#">Осложнение</a></li>
        <li><a href="#">Лечение</a></li>
        <li><a href="#">Профилактика</a></li>
    </ul>
    <div class="service__text">
        <h2 class="mb-20">Причины</h2>
        <p>Основной причиной алкогольного опьянения у будущей мамы является приём алкогольных напитков. Если женщина
            принимает алкоголь во время вынашивания ребёнка, она должна понимать, что наносит непоправимый вред своему
            малышу.</p>
        <p>Алкоголь содержит в себе токсины, которые с кровью попадают в плаценту, преодолевая плацентарный барьер,
            проникают в организм ребёнка. Таким образом, употребляя алкоголь, женщина, возможно, не осознавая того,
            подвергает алкогольной интоксикации своего ребёнка.</p>
        <p>Печень грудного ребёнка ещё недостаточно развита и не способна справиться с обезвреживанием яда. Поэтому даже
            мизерная доза алкоголя оказывает губительное воздействие на детский организм. Это в свою очередь
            отрицательно влияет на развитие внутренних органов, которое происходит в послеродовой период. Последствия
            детской алкогольной интоксикации могут проявиться не сразу, а спустя несколько лет. Самыми распространёнными
            осложнениями является отставание в умственном и физическом развитии ребёнка.
        </p>
        <h2 class="mb-20">Симптомы</h2>
        <p>Симптомы опьянения от приёма алкоголя зависят от степени интоксикации организма. К первым признакам лёгкой
            степени относятся:
        <div class="inner-list">
            <ul>
                <li>раскованность поведения;</li>
                <li>чрезмерная общительность;</li>
                <li>пониженный самоконтроль;</li>
                <li>замедление реакции;</li>
                <li>рассеянное внимание;</li>
                <li>плохая концентрация</li>
            </ul>
        </div>
        <h3 class="mb-20">Диагностика опьянения у беременных</h3>
        <p>Диагностика состояния и определение степени алкогольного опьянения проводится на основе клинической картины.
            Определить, что человек находится под воздействием алкоголя даже в бессознательном состоянии, позволяет
            соответствующий исходящий изо рта и от рвотных масс запах.</p>
        <p>Также степень опьянения можно определить в
            лабораторных условиях. Для правильного диагностирования проводят анализы на реакцию Рапопорта или с помощью
            специальной индикаторной трубки Мохова-Шинкаренко.</p>
    </div>
    <div class="caption-small text-center mb-20">Оцените материал:</div>
    <ul class="rating text-center mb-20">
        <li class="active"><a href="#"></a></li>
        <li class="active"><a href="#"></a></li>
        <li class="active"><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
    </ul>
    <a href="#" class="btn btn-confirm-flat block-center mb-20">Комментировать</a>
    <h2 class="mb-30">Статьи на тему</h2>
    <div class="articles">
        <div class="articles__list">
            <div class="article">
                <div class="article__image">
                    <a href="#"><img src="img/tmp/article1.jpg"></a>
                </div>
    
                <div class="article__content">
                    <a href="#" class="article__category">Психология</a>
                    <a href="#" class="article__title">В чем мама всегда виновата</a>
                </div>
    
                <div class="icon-group article__icons">
                    <a href="#" class="icon-info icon-info-views">120</a>
                    <a href="#" class="icon-info icon-info-comments">3</a>
                    <a href="#" class="icon-info icon-info-likes">3</a>
                </div>
            </div>
    
            <div class="article">
                <div class="article__mark">Партнёрский материал</div>
    
                <div class="article__image">
                    <a href="#"><img src="img/tmp/article2.jpg"></a>
                </div>
    
                <div class="article__content">
                    <a href="#" class="article__title">8 признаков овуляции, которые можно увидеть или почуствовать</a>
                </div>
    
                <div class="icon-group article__icons">
                    <a href="#" class="icon-info icon-info-views">120</a>
                    <a href="#" class="icon-info icon-info-comments">3</a>
                </div>
            </div>
    
        </div>
    
        <div class="text-center"><button class="btn btn-active-flat">Больше статей</button></div>
    </div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->