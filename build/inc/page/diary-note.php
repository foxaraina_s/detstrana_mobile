
<div class="content content-diary scroll-top pt-20">
    <ul class="page-breadcrumbs">
        <li class="back"><a href="#">вернуться к записям</a></li>
    </ul>
    <div class="entries">
        <div class="entries__list">
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author"><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                    Морской привет всем кто на диване
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/diary1.png"></div>
                            <div class="slide"><img src="img/tmp/diary1.png"></div>
                            <div class="slide"><img src="img/tmp/diary1.png"></div>
                            <div class="slide"><img src="img/tmp/diary1.png"></div>
                            <div class="slide"><img src="img/tmp/diary1.png"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text mb-20">
                        — Можете назвать 3 причины, почему стоит пройти процедуру ЭКО именно в «Хадассе»? Почему именно
                        в «Хадассе»? Во-первых, колоссальный опыт. В 1982 году в «Хадассе» открылось первое в Израиле
                        отделение ЭКО, то есть речь идет о 35-летнем опыте! Во-вторых, у нас действует система, которая
                        поддерживает все процессы, связанные с ЭКО, будь то эндокринология, Банк крови, биохимия и пр. И
                        третья, но далеко не последняя причина — обученная команда опытных специалистов, которая
                        заслуженно считается ведущей в Израиле. — Если кто-нибудь из-за границы захочет пройти процедуру
                        экстракорпорального оплодотворения в Израиле… Да, к нам приезжают. Процедура ЭКО может занять 3
                        – 3,5 недели. Это очень длительный срок пребывания за рубежом. Женщина вместе со своим супругом
                        будет находиться в Израиле: расходы на проживание, питание, кроме того, нужно будет оплатить
                        стоимость процедуры. Не каждый может себе позволить такую роскошь, но мы пытаемся сделать
                        процедуру ЭКО доступной для как можно большего числа людей, если они хотят приехать в «Хадассу».
                        — Расскажите подробнее о самой процедуре. Процедура ЭКО состоит из двух этапов. Первый — женщина
                        проходит гормональное лечение и следит за развитием фолликул и яичников, второй — когда все
                        готово, мы проводим операцию по извлечению яйцеклеток. Согласно разработанному
                    </div>
                    <div class="caption-small mb-20">Теги по теме:</div>
                    <ul class="nav nav-tags mb-20">
                        <li><a href="#">Здоровье</a></li>
                        <li class="active"><a href="#">Море</a></li>
                    </ul>
                    <div class="icon-group text-center mb-20">
                        <a href="#" class="icon-info icon-info-views">120</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">3</a>
                    </div>
                    <div class="article-buttons">
                        <a href="#" class="article-buttons__button-rewind">Предыдущая запись</a>
                        <a href="#" class="article-buttons__button-forward">Следующая запись</a>
                    </div>
                    <div class="block-social mb-20">
                        <a href="#" class="icon-fb"></a>
                        <a href="#" class="icon-vk"></a>
                        <a href="#" class="icon-ok"></a>
                    </div>
                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="entries__morecomments">
            <button class="btn btn-active-flat w-100">Еще 15 комментариев</button>
        </div>
    </div>

    <div class="comments">

        <div class="comments__list">

            <div class="comment">

                <div class="comment__header">
                    <div class="comment__author">Виктория Кудрявцева</div>
                    <div class="comment__date">31 мая 2017 года, 12:58</div>
                    <div class="comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>

                <div class="comment__body">
                    <div class="comment__inner">
                        <div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
                    </div>

                    <div class="comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin comment__comment-close"></button>
                        </div>

                        <form class="comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>

            <div class="comment">

                <div class="comment__header">
                    <div class="comment__author">Виктория Кудрявцева</div>
                    <div class="comment__date">31 мая 2017 года, 12:58</div>
                    <div class="comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>

                <div class="comment__body">
                    <div class="comment__inner">

                        <div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>

                        <div class="slider comment__slider">
                            <div class="slider-changeable slider-wrapper">
                                <div class="slides">
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                </div>
                            </div>

                            <ul class="nav nav-dotted nav-dotted-small slider-control slider-changeable">
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin comment__comment-close"></button>
                        </div>

                        <form class="comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <div class="rel mb-10">
                                <button class="btn btn-confirm w-66">Комментировать</button>
                                <button class="btn btn-plus pull-right"></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <div class="comment">

                <div class="comment__header">
                    <div class="comment__author">Катя Медунцева <span>ответила</span></div>
                    <div class="comment__author">Виктория Кудрявцева</div>
                    <div class="comment__date">31 мая 2017 года, 12:58</div>
                    <div class="comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>

                <div class="comment__body">
                    <div class="comment__inner">
                        <div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
                    </div>

                    <div class="comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin comment__comment-close"></button>
                        </div>

                        <form class="comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>

        </div>

    </div>

<h2>Другие записи из дневника</h2>
    <div class="entries mb-20">
        <div class="entries__list mb-20">
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div class="text-center"><button class="btn btn-active-flat">Больше записей</button></div>

    </div>

    
    <!-- Pregnancy calendar -->
    <div class="pregnancy-calendar">
        <div class="pregnancy-calendar__header">
            Следите за ходом<br>
            беременности в личном<br>
            календаре
        </div>
        <div class="pregnancy-calendar__body">
    
    
            <div class="slider pregnancy-calendar__slider">
                <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
                <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>
    
                <div class="slider-changeable pregnancy-calendar__title">I триместр</div>
    
    
    
    
    
                <div class="slider-changeable slider-wrapper">
                    <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                        <div class="slide active">
                            <ul class="pregnancy-calendar__months">
                                <li><a href="#">Месяц 1</a></li>
                                <li><a href="#">Месяц 2</a></li>
                                <li><a href="#">Месяц 3</a></li>
                            </ul>
                            <div class="pregnancy-calendar__week-title">Недели:</div>
                            <ul class="pregnancy-calendar__weeks">
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">6</a></li>
                                <li><a href="#">7</a></li>
                                <li><a href="#">8</a></li>
                                <li><a href="#">9</a></li>
                                <li><a href="#">10</a></li>
                                <li><a href="#">11</a></li>
                                <li><a href="#">12</a></li>
                                <li><a href="#">13</a></li>
                            </ul>
                        </div>
    
                        <div class="slide">
                            <ul class="pregnancy-calendar__months">
                                <li><a href="#">Месяц 4</a></li>
                                <li><a href="#">Месяц 5</a></li>
                                <li><a href="#">Месяц 6</a></li>
                            </ul>
                            <div class="pregnancy-calendar__week-title">Недели:</div>
                            <ul class="pregnancy-calendar__weeks">
                                <li><a href="#">14</a></li>
                                <li><a href="#">15</a></li>
                                <li><a href="#">16</a></li>
                                <li><a href="#">17</a></li>
                                <li><a href="#">18</a></li>
                                <li><a href="#">19</a></li>
                                <li><a href="#">20</a></li>
                                <li><a href="#">21</a></li>
                                <li><a href="#">22</a></li>
                                <li><a href="#">23</a></li>
                                <li><a href="#">24</a></li>
                                <li><a href="#">25</a></li>
                                <li><a href="#">26</a></li>
                            </ul>
                        </div>
    
                        <div class="slide">
                            <ul class="pregnancy-calendar__months">
                                <li><a href="#">Месяц 7</a></li>
                                <li><a href="#">Месяц 8</a></li>
                                <li><a href="#">Месяц 9</a></li>
                            </ul>
                            <div class="pregnancy-calendar__week-title">Недели:</div>
                            <ul class="pregnancy-calendar__weeks">
                                <li><a href="#">27</a></li>
                                <li><a href="#">28</a></li>
                                <li><a href="#">29</a></li>
                                <li><a href="#">30</a></li>
                                <li><a href="#">31</a></li>
                                <li><a href="#">32</a></li>
                                <li><a href="#">33</a></li>
                                <li><a href="#">34</a></li>
                                <li><a href="#">35</a></li>
                                <li><a href="#">36</a></li>
                                <li><a href="#">37</a></li>
                                <li><a href="#">38</a></li>
                                <li><a href="#">39</a></li>
                                <li><a href="#">40</a></li>
                            </ul>
                        </div>
    
                    </div>
                </div>
    
                <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                    <li class="active"><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                </ul>
    
            </div>
        </div>
    </div>
    <!-- /Pregnancy calendar -->