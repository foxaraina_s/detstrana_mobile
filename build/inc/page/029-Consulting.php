<div class="content content-consulting pt-10 scroll-top">

	<h1 class="mb-40">Онлайн-консультации</h1>

	<a href="#" class="btn btn-active btn-shaded w-93 mb-20 block-center">Задать вопрос экспертам</a>

	<button data-target="#popup-doctors" class="btn btn-popup btn-white mb-20">Врачи</button>

	<h2 class="mb-20">Популярные консультации</h2>

	<?php $name = 'Поиск по вопросам';  include 'inc/block/search.php'; ?>


	<ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100 mb-20">
		<li class="w-50 active"><a href="#">Новое</a></li>
		<li class="w-50"><a href="#">Популярное</a></li>
	</ul>


	<div class="questions mb-40">
	<?php include 'inc/block/questions.php' ?>
	</div>

	<?php include 'inc/block/pagination.php' ?>
</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>