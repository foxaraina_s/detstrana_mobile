
<section class="service service-disease">
    <ul class="page-breadcrumbs">
        <li><a href="#">Сервисы</a></li>
        <li><a href="#">Справочник болезней</a></li>
        <li><a href="#">Симптомы</a></li>
        <li><span>Изжога</span></li>
    </ul>
    <h1 class="mb-20">Изжога при беременности</h1>
    <div class="inner-block inner-block--shadow mb-20">
        <div class="inner-block__blockquote">
            <p>Изжога (желудочно-пищеводный рефлюкс) это неприятное ощущение, возникающее в области грудины в виде
                жжения. Также она может сопровождаться болевыми ощущениями в желудки и достигать ротоглотки. Изжога
                возникает из-за того, что содержимое желудка попадает на слизистую пищевода в результате процесса
                регургитации, то есть, быстрого перемещения содержимого в обратном направлении. Изжога может быть
                связана с повышением давления в брюшной полости, при этом она может наблюдаться и у здорового человека.
                Но если данное явление повторяется достаточно часто, оно, скорее всего, свидетельствует о наличии
                какого-либо заболевания или о нарушении моторики пищевода и желудка.</p>
        </div>
    </div>
    <div class="slider nav-tabs-slider mb-20 block-shaded">
        <div class="slider-wrapper slider-changeable">
            <div class="nav-tabs-container slides">
                <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100"
                    style="white-space: nowrap;">
                    <li class="slide w-50 active"><a href="#">Беременные</a></li>
                    <li class="slide w-50"><a href="#">Кормящие мамы</a></li>
                    <li class="slide w-50"><a href="#">Дети</a></li>
                </ul>
            </div>
        </div>
        <a href="#" class="slider-rewind"></a><a href="#" class="slider-forward"></a>
    </div>
    <ul class="nav service__years">
        <li><a href="#">Заболевание по симптомам</a></li>
        <li><a href="#">Лечение и специалисты</a></li>
    </ul>
    <div class="service__text">
        <h2 class="mb-20">Заболевания по симптомам</h2>
        <p>Любой симптом — сигнал организма о том, что нарушен какой-либо орган, отдел или целая система. Чтобы узнать,
            почему возникает изжога у беременных, нужно исключить некоторые заболевания. Позаботьтесь о том, чтобы
            пройти своевременную диагностику, уточните у врачей, отчего появилась изжога и как быстро и эффективно
            улучшить свое состояние.</p>
        <p>Изжога при беременности может возникнуть по следующим причинам:</p>
        <div class="inner-list">
            <ul>
                <li>Изменения гормонального фона;</li>
                <li>Повышение давление внутри брюшины, мешающее выполнять сфинктеру его основные функции;</li>
                <li>Увеличение кислотности желудочного сока;</li>
                <li>Забрасывание желудочной кислоты в пищевод в связи с сильным давлением матки на кишечник, желудок и
                    диафрагму;
                </li>
                <li>Патологии желудочно-кишечного тракта.</li>
            </ul>
        </div>
        <p>
            Изжога при беременности считается обычным явлением, которое не приносит вреда здоровью будущей мамы и её
            малыша. Но если до периода вынашивания ребёнка признаки патологий ЖКТ уже присутствовали, беременной следует
            посетить врача.
        </p>
        <div class="caption-small text-center mb-20">Оцените материал:</div>
        <ul class="rating text-center mb-20">
            <li class="active"><a href="#"></a></li>
            <li class="active"><a href="#"></a></li>
            <li class="active"><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
        </ul>
        <a href="#" class="btn btn-confirm-flat block-center mb-20">Комментировать</a>
    </div>
    <h2 class="mb-20">Проконсультируйтесь по вашим симтомам со специалистом</h2>
    <div class="service__text mb-20">
        Задайте вопрос и получите бесплатную онлайн-консультацию у ведущего специалиста.
    </div>
    <a href="#" class="btn btn-active btn-shaded w-93 mb-20 block-center">Проконсультироваться</a>
    <div class="service__text">
        <p>Вооружайтесь знаниями и читайте полезную информативную статью о заболевании изжога при беременности. Ведь
            быть родителями – значит, изучать всё то, что поможет сохранять градус здоровья в семье на отметке
            «36,6».</p>
        <p>Узнайте, что может вызвать недуг изжога при беременности, как его своевременно распознать. Найдите информацию
            о том, каковы признаки, по которым можно определить недомогание. И какие анализы помогут выявить болезнь и
            поставить верный диагноз.</p>
        <p>В статье вы прочтёте всё о методах лечения такого заболевания, как изжога при беременности. Уточните, какой
            должна быть эффективная первая помощь. Чем лечить: выбрать лекарственные препараты или народные методы?</p>
        <p>Также вы узнаете, чем может быть опасно несвоевременное лечение недуга изжога при беременности, и почему так
            важно избежать последствий. Всё о том, как предупредить изжога при беременности и не допустить осложнений.
            Будьте здоровы!</p>
    </div>
    <div class="inner-block inner-block--shadow mb-20">
        <h2 class="mb-20">Также смотрят:</h2>
        <div class="simple-list">
            <ul>
                <li><a href="#">Болезненность молочных желез</a></li>
                <li><a href="#">Боль в груди</a></li>
                <li><a href="#">Жжение в теле и конечностях</a></li>
                <li><a href="#">Изжога</a></li>
                <li><a href="#">Кашель</a></li>
                <li><a href="#">Набухшая грудь</a></li>
                <li><a href="#">Одышка</a></li>
                <li><a href="#">Опущение молочных желез</a></li>
                <li><a href="#">Отложение жира в области плеч, груди, лица</a></li>
                <li><a href="#">Сбои сердечного ритма</a></li>
                <li><a href="#">Удушье</a></li>
                <li><a href="#">Хрипы в груди</a></li>
            </ul>
        </div>
    </div>

</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->