
<section class="service service-gender scroll-top">
    <div class="service__subtitle"><a href="#">Сервисы</a> <i>❯</i> Определение пола</div>
    <h2 class="service__title">Определение пола ребенка по китайскому методу</h2>
    <div class="service__text">Чтобы узнать пол ребенка, укажите возраст мамы и месяц зачатия ребенка</div>
    <div class="gender__method--wrapper gender-method-chinease">
        <div class="gender__method--form chinese_table_block">
            <form method="post">
            <div class="gender-form-mom gender-form chinese_table_block_l">
                <div class="gender__section-title">Возвраст мамы:</div>
                <div class="form-group form-group-birthday  mb-20">
                    <div class="form-birthday-month">
                        <select name="mm" placeholder="Лет" class="form-select select_big_input" id="ctb1">
                            <option value="0">18</option>
                            <option value="1">19</option>
                            <option value="2">20</option>
                            <option value="3">21</option>
                            <option value="4">22</option>
                            <option value="5">23</option>
                            <option value="6">24</option>
                            <option value="7">25</option>
                            <option value="8">26</option>
                            <option value="9">27</option>
                            <option value="10">28</option>
                            <option value="11">29</option>
                            <option value="12">30</option>
                            <option value="13">31</option>
                            <option value="14">32</option>
                            <option value="15">33</option>
                            <option value="16">34</option>
                            <option value="17">35</option>
                            <option value="18">36</option>
                            <option value="19">37</option>
                            <option value="20">38</option>
                            <option value="21">39</option>
                            <option value="22">40</option>
                            <option value="23">41</option>
                            <option value="24">42</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="mb-40 gender-form-zigota gender-form chinese_table_block_r">
                <div class="gender__section-title">Месяц зачатия ребенка:</div>
                <div class="form-group form-group-birthday  mb-20">
                    <div class="form-birthday-month">
                        <select name="mm" placeholder="Месяц" class="form-select select_big_input" id="ctb2">
                            <option value="0">Январь</option>
                            <option value="1">Февраль</option>
                            <option value="2">Март</option>
                            <option value="3">Апрель</option>
                            <option value="4">Май</option>
                            <option value="5">Июнь</option>
                            <option value="6">Июль</option>
                            <option value="7">Август</option>
                            <option value="8">Сентябрь</option>
                            <option value="9">Октябрь</option>
                            <option value="10">Ноябрь</option>
                            <option value="11">Декабрь</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a class="btn btn-confirm btn-shaded w-93 baby_gender_button chinese_table">Узнать пол</a>
            </div>
            </form>
        </div>
        <div id="result">
            <div class="blood_group_result baby-gender-result">
                <div class="blood_group_result_content baby-gender-result__inner">
                    <div>ПОЗДРАВЛЯЕМ!</div><div>У ВАС, СКОРЕЕ ВСЕГО, БУДЕТ <span id=bgr_gender>девочка</span></div>
                </div>
            </div>
            <h3 class="text-w--regular text-medium gender__subtitle">Календарь зачатия ребенка</h3>
            <table class="chienese-results csd_table">
                <tbody>
                <tr>
                    <td>Возр.<br>мамы</td>
                    <td>Янв</td>
                    <td>Февр</td>
                    <td>Март</td>
                    <td>Апр</td>
                    <td>Май</td>
                    <td>Июнь</td>
                    <td>Июль</td>
                    <td>Авг</td>
                    <td>Снт</td>
                    <td>Окт</td>
                    <td>Ноябр</td>
                    <td>Дек</td>
                </tr>
                </tbody>
            </table>
            <div class="icon-group text-center mt-20">
                <a href="#" class="icon-rounded icon-fb"></a>
                <a href="#" class="icon-rounded icon-vk"></a>
                <a href="#" class="icon-rounded icon-ok"></a>
            </div>
            <h3 class="gender__subtitle">Другие способы определения пола ребенка</h3>
            <div class="gender__choose">
                <div class="gender__choose--methods">
                    <a href="service-baby-gender--blood-refresh.html" class="method--item blood__refresh">
                        По обновлению крови
                    </a>
                    <a href="service-baby-gender-blood-group.html" class="method--item blood__group">
                        По группе крови родителей
                    </a>
                    <a href="service-baby-gender-japan.html" class="method--item japanese__table">
                        Японский метод
                    </a>
                </div>

            </div>

        </div>
        <div class="entries">
            <div class="entries__list">
                <div class="entry entry-nobg">
                    <div class="entry__body no-bg p-no mt-20 ">
                        <div class="icon-group entry__comment">
                            <div class="rel mb-10">
                                <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                                <button class="btn btn-close entry__comment-close"></button>
                            </div>
        
                            <form class="entry__comment-form">
                                <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                                <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                            </form>
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="entries">
        <div class="entries__morecomments">
            <button class="btn btn-active-flat w-100">Еще 15 комментариев</button>
        </div>
        <div class="entry-comments__list">
        
            <div class="entry-comment">
        
                <div class="entry-comment__header">
                    <div class="entry-comment__author">Виктория Кудрявцева</div>
                    <div class="entry-comment__date">31 мая 2017 года, 12:58</div>
                    <div class="entry-comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>
        
                <div class="entry-comment__body">
                    <div class="entry-comment__inner">
                        <div class="entry-comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
                    </div>
        
                    <div class="entry-comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat entry-comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat entry-comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin entry-comment__comment-close"></button>
                        </div>
        
                        <form class="entry-comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>
        
                </div>
            </div>
        
            <div class="entry-comment">
        
                <div class="entry-comment__header">
                    <div class="entry-comment__author">Виктория Кудрявцева</div>
                    <div class="entry-comment__date">31 мая 2017 года, 12:58</div>
                    <div class="entry-comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>
        
                <div class="entry-comment__body">
                    <div class="entry-comment__inner">
        
                        <div class="entry-comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
        
                        <div class="slider entry-comment__slider">
                            <div class="slider-changeable slider-wrapper">
                                <div class="slides">
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                </div>
                            </div>
        
                            <ul class="nav nav-dotted nav-dotted-small slider-control slider-changeable">
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </div>
        
                    </div>
        
                    <div class="entry-comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat entry-comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat entry-comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin entry-comment__comment-close"></button>
                        </div>
        
                        <form class="entry-comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <div class="rel mb-10">
                                <button class="btn btn-confirm w-66">Комментировать</button>
                                <button class="btn btn-plus pull-right"></button>
                            </div>
                        </form>
                    </div>
        
                </div>
            </div>
        
        </div>
        </div>
    <p class="text-center"><button class="btn btn-active-flat">Больше записей</button></p>
    <div class="service__text">Определение пола ребенка по обновлению крови родителей считается одним из самых
        популярных методов. Он основывается на данных о будущих родителях. А именно, на периодах обновления крови у папы
        и у мамы. Считается, что у девушек оно проходит 1 раз за 3 года, а у мужчин – 1 раз в четыре года. <br>
        Так, когда вы пользуетесь калькулятором, определяющим пол будущего ребенка по обновлению крови, нужно знать: чья кровь была
        более «свежая» на момент зачатия, такого пола и будет кроха. <br>
        При расчете пола ребенка по обновлению крови также надо учитывать следующее: если вам делали переливание, то расчет будет вестись от даты последнего переливания
        крови. И еще: результат может быть искаженным, если пол ребенка по обновлению крови родителей вы рассчитали без
        учета недавних операций или различных травм с кровотечениями. Если метод определения пола по обновлению крови
        онлайн кажется вам не совсем достоверным, воспользуйтесь другими видами тестов.
    </div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->
<script src="js/gender_js.js?<?php echo time() ?>"></script>