<div class="service service-names scroll-top">
<?php
	$title = 'Подбор имени';

	include 'inc/block/services/names/header.php';
?>

	<form class="service-names__form-choose">
		<div class="form-group mb-20">
			<label>Дата рождения:</label>
			<?php include 'inc/block/date-dm.php '?>
		</div>

		<div class="form-group mb-20">
			<label>Пол ребёнка:</label>
			<select name="gender" placeholder="Все" class="form-select">
				<option value="">Все</option>
				<option value="1" selected>Мальчик</option>
				<option value="2">Девочка</option>
			</select>
		</div>

		<div class="form-group mb-20">
			<label>Происхождение имени:</label>
			<select name="origin" placeholder="Все" class="form-select">
				<option>Все</option>
				<option value="greek" selected>Греческие</option>
				<option value="jewish">Еврейские</option>
			</select>
		</div>

		<button class="btn btn-confirm btn-shaded w-100 mb-20">Подобрать</button>

	</form>

	<?php include 'inc/block/services/names/text.php' ?>

</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>