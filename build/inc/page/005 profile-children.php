<div class="profile profile-children scroll-top">
	<h1 class="mb-10">Дети</h1>

	<div class="profile__step">Шаг 4 из 4</div>

	<div class="profile__description">Расскажите о ваших детях, чтобы мы смогли узнать о вас чуть больше и предложить удобные и полезные сервисы</div>

	<form action="#" method="post" class="profile__form">
		<div class="form-group form-group-checkbox text-center">
			<input type="checkbox" name="no-children" value="1" id="profile-pregnancy__no-children">
			<label for="profile-pregnancy__no-children">Нет детей</label>
		</div>

		<div class="profile-children__add-child mb-40">
			<button data-target="#popup-add-child" class="btn btn-confirm btn-circle btn-large btn-shaded mb-20">+</button>
			<div>Добавить ребенка</div>
		</div>

		<button class="btn btn-confirm btn-shaded w-100 mb-20">Сохранить и продолжить</button>

		<button class="btn btn-active-flat btn-center">Пропустить шаг</button>
	</form>

	<div id="popup-add-child" class="popup popup-add-child popup-full">
		<div class="popup__close"></div>
		<div class="popup__header">Добавление ребёнка</div>	
		<div class="popup__body">
			<form action="#" method="post">
				<div class="profile__section-title">Укажите пол ребёнка</div>

				<div class="form-group form-group-choice mb-20">
					<input type="radio" name="gender" id="profile-about__girl" value="2" checked>
					<label for="profile-about__girl">Девочка</label>

					<input type="radio" name="gender" id="profile-about__boy" value="1">
					<label for="profile-about__boy">Мальчик</label>
				</div>

				<div class="profile__section-title">Имя и фамилия ребёнка</div>

				<div class="form-group">
					<label class="form-label">Имя ребёнка</label>
					<input name="user" type="text" placeholder="Имя ребёнка" maxlength="20" class="form-control">
					<div class="form-limit">&nbsp;</div>
					<div class="form-error">&nbsp;</div>
				</div>

				<div class="form-group mb-20">
					<label class="form-label">Фамилия ребёнка</label>
					<input name="name" type="text" placeholder="Фамилия ребёнка" class="form-control">
					<div class="form-limit">&nbsp;</div>
					<div class="form-error">&nbsp;</div>
				</div>

				<div class="profile__section-title">День рождения ребёнка</div>


				<div class="form-group form-group-birthday mb-20">
					<?php include 'inc/block/date-dmy.php '?>
				</div>

				<div class="profile-children__add-child mb-20">
					<button data-target="#popup-add-child" class="btn btn-confirm btn-circle btn-shaded mb-20">+</button>
					<div>Добавить ребенка</div>
				</div>

				<button class="btn btn-confirm btn-shaded w-100">Сохранить и продолжить</button>

			</form>
		</div>
	</div>

</div>