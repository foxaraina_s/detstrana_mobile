
<div id="popup-date" class="popup popup-date popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Выберите дату</div>
    <div class="popup__body">
        <div class="popup-date__calendar">
            <div class="popup-date__calendar-header">
                <a href="#" class="popup-date__calendar-rewind"></a>
                <a href="#" class="popup-date__calendar-forward"></a>
                <div class="popup-date__calendar-title">Ноябрь 2017</div>
            </div>

            <div class="popup-date__calendar-weekdays">
                <div>Пн</div>
                <div>Вт</div>
                <div>Ср</div>
                <div>Чт</div>
                <div>Пт</div>
                <div>Сб</div>
                <div>Вс</div>
            </div>

            <div class="popup-date__calendar-days">
                <div></div>
                <div></div>
                <div>1</div>
                <div>2</div>
                <div>3</div>
                <div>4</div>
                <div>5</div>
                <div>6</div>
                <div>7</div>
                <div>8</div>
                <div>9</div>
                <div>10</div>
                <div>11</div>
                <div>12</div>
                <div>13</div>
                <div>14</div>
                <div>15</div>
                <div>16</div>
                <div>17</div>
                <div>18</div>
                <div>19</div>
                <div>20</div>
                <div>21</div>
                <div>22</div>
                <div>23</div>
                <div>24</div>
                <div>25</div>
                <div>26</div>
                <div>27</div>
                <div>28</div>
                <div>29</div>
                <div>30</div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>
<section class="service service-birthcalendar pt-20 scroll-top">
    <h1 class="mb-20">Календарь беременности</h1>
    <div class="service__text">Выберите или рассчитайте свой срок, и мы расскажем вам всё, что нужнл знать будующей
        маме!
    </div>

    <!-- Pregnancy calendar -->
    <div class="pregnancy-calendar mt-20 mb-20">
        <div class="pregnancy-calendar__body">
            <div class="slider pregnancy-calendar__slider">
                <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
                <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>
                <div class="slider-changeable pregnancy-calendar__title">I триместр</div>
                <div class="slider-changeable slider-wrapper">
                    <div class="slides">
                        <div class="slide">
                            <ul class="pregnancy-calendar__months">
                                <li><a href="#">1 месяц</a></li>
                                <li><a href="#">2 месяц</a></li>
                                <li><a href="#">3 месяц</a></li>
                            </ul>
                            <div class="pregnancy-calendar__week-title">Недели:</div>
                            <ul class="pregnancy-calendar__weeks">
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">6</a></li>
                                <li><a href="#">7</a></li>
                                <li><a href="#">8</a></li>
                                <li><a href="#">9</a></li>
                                <li><a href="#">10</a></li>
                                <li><a href="#">11</a></li>
                                <li><a href="#">12</a></li>
                                <li><a href="#">13</a></li>
                            </ul>
                        </div>

                        <div class="slide">
                            <ul class="pregnancy-calendar__months">
                                <li><a href="#">4 месяц</a></li>
                                <li><a href="#">5 месяц</a></li>
                                <li><a href="#">6 месяц</a></li>
                            </ul>
                            <div class="pregnancy-calendar__week-title">Недели:</div>
                            <ul class="pregnancy-calendar__weeks">
                                <li><a href="#">14</a></li>
                                <li><a href="#">15</a></li>
                                <li><a href="#">16</a></li>
                                <li><a href="#">17</a></li>
                                <li><a href="#">18</a></li>
                                <li><a href="#">19</a></li>
                                <li><a href="#">20</a></li>
                                <li><a href="#">21</a></li>
                                <li><a href="#">22</a></li>
                                <li><a href="#">23</a></li>
                                <li><a href="#">24</a></li>
                                <li><a href="#">25</a></li>
                                <li><a href="#">26</a></li>
                            </ul>
                        </div>

                        <div class="slide">
                            <ul class="pregnancy-calendar__months">
                                <li><a href="#">7 месяц</a></li>
                                <li><a href="#">8 месяц</a></li>
                                <li><a href="#">9 месяц</a></li>
                            </ul>
                            <div class="pregnancy-calendar__week-title">Недели:</div>
                            <ul class="pregnancy-calendar__weeks">
                                <li><a href="#">27</a></li>
                                <li><a href="#">28</a></li>
                                <li><a href="#">29</a></li>
                                <li><a href="#">30</a></li>
                                <li><a href="#">31</a></li>
                                <li><a href="#">32</a></li>
                                <li><a href="#">33</a></li>
                                <li><a href="#">34</a></li>
                                <li><a href="#">35</a></li>
                                <li><a href="#">36</a></li>
                                <li><a href="#">37</a></li>
                                <li><a href="#">38</a></li>
                                <li><a href="#">39</a></li>
                                <li><a href="#">40</a></li>
                            </ul>
                        </div>

                    </div>
                </div>

                <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                </ul>

            </div>
        </div>
    </div>
    <!-- /Pregnancy calendar -->
    <div class="pregnancy-count">
        <h2 class="mb-20">Помочь рассчитать срок беременности?</h2>
        <div class="service__text">Укажите первый день последней менструации и узнайте, на какой неделе вы находитесь
            сейчас
        </div>
        <div class="pregnancy-count__form">
            <form method="get">
                <div class="form-group mb-20">
                    <div class="form-select-wrapper"><a href="#" class="form-select" data-target="#popup-date">Выберите
                        дату</a></div>
                </div>
                <div class="text-sm text-light">Ваш срок беременности:</div>
                <div class="text-center mb-20">
                    <div class="pregnancy-count__result pregnancy-count__result--week">
                        <div class="num">39</div>
                        <div class="text">недель</div>
                    </div>
                    <div class="pregnancy-count__result--devide">
                    </div>
                    <div class="pregnancy-count__result pregnancy-count__result--month">
                        <div class="num">9</div>
                        <div class="text">месяцев</div>
                    </div>
                </div>


            </form>
            <a href="#" class="btn btn-confirm btn-shaded w-100">Открыть календарь на 39 неделе</a>
        </div>
    </div>
    <div class="service__text">
        <p>С календарём беременности вы узнаете, что происходит на протяжении 9 месяцев с вами и крохой. Календарь
            беременности по неделям расскажет, как протекает развитие будущего ребенка, какие изменения происходят в
            вашем
            орагнизме, характере и внешности.</p>
        <p>Ведите свой интерактивный календарь: отмечайте изменения, которые с вами
            происходят, задавайте вопросы экспертам и общайтесь с другими будущими мамами.</p>
        <p>Если сомневаетесь в поставленном сроке беременности - определите его с помощью нашего калькулятора! Укажите
            первый день последней менструации, и
            сервис безошибочно поможет рассчитать точный срок.</p>
    </div>
    <div class="text-center mb-40">
        <h2 class="mb-30">Мы расчитали для вас график обследований</h2>
        <a href="#" class="btn btn-active-flat">Посмотреть график</a>
    </div>
    <div class="inner-block inner-block--shadow mb-20">
        <h2 class="mb-30">Рассылка календаря беременности</h2>
        <div class="inner-block__text">
            <p>
                Получите бесплатно иллюстрированный курс о беременности, развитии малыша и изменениях вашего организма.
            </p>
            <p>Подпишитесь на еженедельные уроки и советы!</p>
        </div>
        <form method="post">
            <div class="form-group">
                <label class="form-label">Ваше имя</label>
                <input name="user" type="text" placeholder="Ваше имя" maxlength="20" class="form-control">
                <div class="form-error">Заполните поле</div>
            </div>
            <div class="form-group">
                <label class="form-label">Ваш e-mail</label>
                <input name="name" type="email" placeholder="Ваш e-mail" maxlength="100" class="form-control">
                <div class="form-error">Заполните поле</div>
            </div>
            <span class="inner-block__text">Начать с</span>
            <div class="form-group w-20 mx-8 mb-20 mt-20 d-iblock">
                <select name="pw" id="brb1_1" placeholder="1" class="form-select">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="35">35</option>
                    <option value="36">36</option>
                    <option value="37">37</option>
                    <option value="38">38</option>
                    <option value="39">39</option>
                    <option value="40">40</option>
                </select>
            </div>
            <span class="inner-block__text">недели беременности</span>
            <input type="submit" value="Подписаться на рассылку" class="btn btn-confirm btn-shaded w-100">
        </form>
    </div>
    <div class="birthcalendar-article">
        <h2 class="mb-20">Календарь беременности - ваш личный помощник</h2>
        <div class="birthcalendar-article__inner">

            <div class="slider birthcalendar-article__slider">
                <div class="slider-changeable slider-wrapper">
                    <div class="slides">
                        <div class="slide">
                            <div class="birthcalendar-article__img">
                                <img src="img/birthcalendar/cb2.png">
                            </div>
                            <div class="birthcalendar-article__title">
                                Планируйте важные дела
                            </div>
                            <div class="birthcalendar-article__text">
                                Зная свой срок беременности и то, какие особенности вас ждут в этот период, вам легко
                                будет планировать важные дела: семейный отпуск, обустройство детской комнаты и покупку
                                вещей, сбор сумки в роддом и получение обменной карты.
                            </div>

                        </div>
                        <div class="slide">
                            <div class="birthcalendar-article__img">
                                <img src="img/birthcalendar/cb2.png">
                            </div>
                            <div class="birthcalendar-article__title">
                                Планируйте важные дела
                            </div>
                            <div class="birthcalendar-article__text">
                                Зная свой срок беременности и то, какие особенности вас ждут в этот период, вам легко
                                будет планировать важные дела: семейный отпуск, обустройство детской комнаты и покупку
                                вещей, сбор сумки в роддом и получение обменной карты.
                            </div>

                        </div>
                        <div class="slide">
                            <div class="birthcalendar-article__img">
                                <img src="img/birthcalendar/cb2.png">
                            </div>
                            <div class="birthcalendar-article__title">
                                Планируйте важные дела
                            </div>
                            <div class="birthcalendar-article__text">
                                Зная свой срок беременности и то, какие особенности вас ждут в этот период, вам легко
                                будет планировать важные дела: семейный отпуск, обустройство детской комнаты и покупку
                                вещей, сбор сумки в роддом и получение обменной карты.
                            </div>

                        </div>

                    </div>

                </div>


                <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                </ul>
            </div>

        </div>
    </div>
    <div class="birthcalendar-slider">
        <h2 class="mb-20">Наблюдайте за развитием плода с помощью иллюстраций</h2>
        <div class="service__text">Сдедите за етм, что происходит с вашим телом, и как протекает внутриутробное развитие
            плода. Основные этапы изменений
            наглядно проиллюстрированы и сопровождаются понятными комментариями.
        </div>
        <div class="slider entry__images">
            <div class="slider-changeable slider-wrapper">
                <div class="slides">
                    <div class="slide"><img src="img/birthcalendar/st1.jpg"></div>
                    <div class="slide"><img src="img/birthcalendar/st1.jpg"></div>
                    <div class="slide"><img src="img/birthcalendar/st1.jpg"></div>
                    <div class="slide"><img src="img/birthcalendar/st1.jpg"></div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>
        </div>
    </div>
    <div class="birthcalendar-slider">
        <h2 class="mb-20">Введите график обследований</h2>
        <div class="service__text">Мы рассчитаем для вас индивидуальный график женской консультации</div>
        <div class="diagnostic-time">
            <h4 class="diagnostic-time__title">1 триместр</h4>
            <div class="diagnostic-time__subtitle text-center">
                <span>3 анализа</span>
                <span>4 консультации</span>
            </div>
        </div>
        <div class="slider entry__images">
            <div class="slider-changeable slider-wrapper">
                <div class="slides">
                    <div class="slide"><img src="img/birthcalendar/dg.jpg"></div>
                    <div class="slide"><img src="img/birthcalendar/dg.jpg"></div>
                    <div class="slide"><img src="img/birthcalendar/dg.jpg"></div>
                    <div class="slide"><img src="img/birthcalendar/dg.jpg"></div>
                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>
        </div>
    </div>
    <div class="block block__also-see">
        <h2 class="mb-20">С кадендарем беременности смотрят</h2>
        <div class="service__list">
            <div class="service__item">
                <a href="#">
                    <div class="service__item--title">Расчет даты родов и график обследований</div>
                    <div class="service__item--image"><img src="img/tmp/serv2.jpg"></div>
                    <div class="service__item--category">Беременность и роды</div>
                    <div class="service__item--content">
                        Узнайте дату появления на свет своего малыша и воспользуйтесь личным календарем диагностики
                        здоровья.
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->