
<section class="service service-names name-block">
    <div class="service__subtitle"><a href="#">Сервисы</a> <i>❯</i> Имена</div>
    <h1 class="mb-30">Имя Абдулла</h1>
    <div class="mb-20 text-center">
        <a href="#" class="btn btn-active-flat">Вернуться на страницу имен</a>
    </div>
    <div class="service-names__info mb-40">
        <ul>
            <li>Происхождение имени - арабское</li>
            <li>Значение имена - Раб Бога</li>
        </ul>
    </div>

    <div class="icon-group text-center mb-40">
        <a href="#" class="icon-rounded icon-fb"></a>
        <a href="#" class="icon-rounded icon-vk"></a>
        <a href="#" class="icon-rounded icon-ok"></a>
    </div>
    <div class=" mb-40 service__text service__text-no">
        <p>Маленький Абдулла очень любознательный мальчик. Ему интересно буквально все. Он спрашивает обо всем на свете.
            Родители просто не успевают отвечать на все его бесконечные вопросы. Абдулла не избалован и неприхотлив. Для
            счастья ему нужно совсем минимум. Он любит играть со сверстниками и уважает старших. Обучение дается носителю
            имени не очень легко. Ему не хватает терпения и усидчивости. Он может отвлечься от процесса обучения, прослушать
            учителя и просто постесняться признаться, что не понял новую тему. </p>
        <p>Взрослый Абдулла такой же не собранный, как и маленький. Дома, если Абдулла живет один, у него вечно все разбросано.
            Полный хаос. Но Абдулла не в претензии, у него нет дискомфорта. Руководящей должности обычно Абдулла не занимает. Максимум это среднее руководящее звено.
            Друзья его любят, таким, каким он есть, со всеми его недостатками. Он любит шумные компании, на любом празднике
            Абдулла один из первых начинает веселиться и один из последних уходит. Женится он обычно поздно. Он не спешит
            прощаться со своей свободой. Слишком он привык к своему образу жизни. Абдулла прекрасно понимает, что вместе с
            браком придется менять весь уклад его неорганизованного существования. Однако часто приходит все-таки это время.
            Когда Абдулла переходит тридцатилетний рубеж, он начинает задумываться о семье. </p>
        <p>Мужем он старается быть внимательным. Но жене его приходится нелегко. Слишком Абдулла свободолюбивый. Он не привык считаться с кем-то
            еще кроме себя. Ему важен личный комфорт, хотя он и может заключаться в полном хаосе. Он много времени уделяет
            не семье, а другому своему окружению. Супруге Абдуллы нужно быть готовой, что его дом всегда должен быть открыт
            для многочисленных друзей. И посиделки могут быть до утра. Что касается детей, Абдулла не против ребенка, но
            занимается с сыном или дочерью довольно мало. Он просто не в курсе, как обращаться с ребенком. Поэтому все
            воспитание чада ложится на плечи его жены. С годами, впрочем, в носителе имени заметны изменения. Он становится
            более домашним, привыкает к порядку и уюту в доме. Домой его тянет.</p>
    </div>
    <div class="service-names__horoscope">
        <h3 class="title">Подходит для знаков зодиака</h3>
        <div class="text-center">
            <a class="horoscope-item horoscope-item--capricon" href="#">Козерог</a>
            <a class="horoscope-item horoscope-item--aquarius" href="#">Водолей</a>
            <a class="horoscope-item horoscope-item--virgo" href="#">Дева</a>
            <a class="horoscope-item horoscope-item--taurus" href="#">Телец</a>
        </div>

    </div>
    <div class="service-names__compatibility mb-40">
        <h3 class="title">Совместим с именем</h3>
        <ul>
            <li><a href="#">Ольга</a></li>
            <li><a href="#">Ева</a></li>
            <li><a href="#">Лилия</a></li>
            <li><a href="#">Мария</a></li>
            <li><a href="#">Тамара</a></li>
            <li><a href="#">Сабина</a></li>
            <li><a href="#">Анисия</a></li>
            <li><a href="#">Руфина</a></li>
            <li><a href="#">Диляра</a></li>
            <li><a href="#">Тереза</a></li>
            <li><a href="#">Ефимия</a></li>
            <li><a href="#">Гузель</a></li>

        </ul>
    </div>
    <div class="service-names__talisman">
        <h3 class="title">Талисман</h3>
        <ul>
            <li><span>Камень:</span> берилл;</li>
            <li><span>Растение:</span> василек;</li>
            <li><span>Животное:</span> барс;</li>
        </ul>
    </div>
    <div class="service-names__derived">
        <h3 class="title">Производные имени</h3>
        <ul>
            <li>Абдулла</li>
            <li>Абдул</li>
            <li>Абдааллах</li>
            <li>Абдалла</li>
            <li>Абдуло</li>
        </ul>
    </div>
    <div class="service-names__famous mb-40">
        <h3 class="title">Знаменитости с этим именем</h3>
        <ul>
            <li>
                <img src="img/fm-abdulla1.png">
                <h4 class="uppercase-title-sm">Абдаллах Ансари</h4>
                <div class="text-no">Персидский поэт-суфий</div>
            </li>
            <li>
                <img src="img/fm-abdulla2.png">
                <h4 class="uppercase-title-sm">Абдаллах-Хан II</h4>
                <div class="text-no">Персидский поэт-суфий</div>
            </li>
        </ul>
    </div>
    <div class="service-names__also-see">
        <h3 class="title">Также смотрят</h3>
        <ul>
            <li><a href="#">Адам</a></li>
            <li><a href="#">Аарон</a></li>
            <li><a href="#">Автандил</a></li>
        </ul>
    </div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->