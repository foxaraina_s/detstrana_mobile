
<div class="content content-community scroll-top">
        <h1 class="mb-20">Сообщества</h1>
        <button data-target="#popup-user-filter" class="btn btn-popup btn-white mb-20">Показать фильтр</button>
        <div class="communities mb-20">
            <div class="communities__list">
                <div class="community mb-10">

                    <div class="community__title">
                       Подготовка к зачатию
                    </div>

                    <div class="community__image">
                        <img src="img/tmp/community1.png">
                        </div>
                    <div class="community__body">
                        <div class="icon-group community__icons">
                            <div class="community__icons-item"> <a href="#" class="icon-info icon-info-notes">71 запись</a>
                                <button class="btn btn-confirm btn-shaded w-46">Создать запись</button>
                            </div>
                            <div class="community__icons-item"> <a href="#" class="icon-info icon-info-people">10528 участников</a>
                                <button class="btn btn-confirm btn-shaded w-46">Вступить</button>
                            </div>

                        </div>
                        <ul class="community__notes mb-40">
                            <li><a href="#">Забеременеть в Хаддассе <span>38</span></a></li>
                            <li><a href="#">Хотим зачать пацана <span>52</span></a></li>
                            <li><a href="#">Астрология и дата зачатия связаны? <span>16</span></a></li>
                            <li><a href="#">Забеременеть в Хаддассе <span>7</span></a></li>
                        </ul>
                        <button class="btn btn-active-flat btn-center">Больше записей</button>
                    </div>


                    </div>
                <div class="community mb-10">

                    <div class="community__title">
                        Подготовка к родам и роды
                    </div>

                    <div class="community__image">
                        <img src="img/tmp/community2.png">
                    </div>
                    <div class="community__body">
                        <div class="icon-group community__icons">
                            <div class="community__icons-item"> <a href="#" class="icon-info icon-info-notes">71 запись</a>
                                <button class="btn btn-confirm btn-shaded w-46">Создать запись</button>
                            </div>
                            <div class="community__icons-item"> <a href="#" class="icon-info icon-info-people">10528 участников</a>
                                <button class="btn btn-confirm btn-shaded w-46">Вступить</button>
                            </div>

                        </div>
                        <ul class="community__notes mb-40">
                            <li><a href="#">После родов похудела на 27 кг <span>38</span></a></li>
                            <li><a href="#">Роды за руку с мужем <span>52</span></a></li>
                            <li><a href="#">Боюсь начала схваток! <span>16</span></a></li>
                            <li><a href="#">Забеременеть в Хаддассе <span>7</span></a></li>
                        </ul>
                        <button class="btn btn-active-flat btn-center">Больше записей</button>
                    </div>


                </div>


                </div>
            </div>
    </div>







<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->