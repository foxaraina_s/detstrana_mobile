<div class="service service-ovulation-calculator scroll-top">
<?php
	include 'inc/block/services/ovulation-calculator/header.php';
?>

	<div class="mlr-16 mb-20">
		<form action="#" method="post">

			<div class="form-group mb-20">
				<!-- label class="form-label mb-10">Дата начала цикла</label>
				<select name="cycle-start" class="form-select">
					<option>Выберите дату</option>
				</select -->
				
				<a href="#" class="form-select" data-target="#popup-date">Выберите дату</a>
			</div>

			<div class="form-group mb-20">
				<label class="form-label mb-10">Средняя длина цикла</label>
				<select name="cycle-duration" class="form-select">
					<option>Дней</option>
					<?php foreach(range(21, 35) as $i) echo "<option value=\"$i\">$i</option>" ?>
				</select>
			</div>

			<div class="form-group mb-20">
				<label class="form-label mb-10">Длительность менструации</label>
				<select name="menstruation-duration" class="form-select">
					<option>Дней</option>
					<?php foreach(range(2, 8) as $i) echo "<option value=\"$i\">$i</option>" ?>
				</select>
			</div>
		
			<button class="btn btn-confirm btn-shaded w-100">Рассчитать</button>
		</form>
	</div>



	<div class="service-ovulation-calculator__calculation mb-20">
		
		<div class="service-ovulation-calculator__calculation-header">
			<div class="service-ovulation-calculator__calculation-header-current">Текущий цикл</div>
			<div class="service-ovulation-calculator__calculation-header-period">c 13 июля по 9 августа 2017</div>
		</div>
		
		<div class="service-ovulation-calculator__calculation-buttons">
			<a href="#" class="service-ovulation-calculator__calculation-rewind">Предыдущий цикл</a>
			<a href="#" class="service-ovulation-calculator__calculation-forward">Следующий цикл</a>
		</div>
		
		<div class="service-ovulation-calculator__calculation-calendar">
			<div class="service-ovulation-calculator__calculation-calendar-header">
				<?php foreach(['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'] as $weekday) { ?>
				<div><?php echo $weekday ?></div>
				<?php } ?>
			</div>
			<div class="service-ovulation-calculator__calculation-calendar-days">
				<div>10</div>
				<div>11</div>
				<div>12</div>
				<div class="menstrual"><span>13</span> <b>1</b></div>
				<div class="menstrual"><span>14</span> <b>2</b></div>
				<div class="menstrual"><span>15</span> <b>3</b></div>
				<div class="menstrual"><span>16</span> <b>4</b></div>
				
				<div class="follicular"><span>17</span> <b>5</b></div>
				<div class="follicular"><span>18</span> <b>6</b></div>
				<div class="follicular"><span>19</span> <b>7</b></div>
				<div class="follicular"><span>20</span> <b>8</b></div>
				<div class="follicular"><span>21</span> <b>9</b></div>
				<div class="follicular"><span>22</span> <b>10</b></div>
				<div class="ovulatory"><span>23</span> <b>11</b></div>
				
				<div class="ovulatory"><span>24</span> <b>12</b></div>
				<div class="ovulatory"><span>25</span> <b>13</b></div>
				<div class="ovulatory"><span>26</span> <b>14</b></div>
				<div class="ovulatory-peak"><span>27</span> <b>15</b></div>
				<div class="ovulatory"><span>28</span> <b>16</b></div>
				<div class="ovulatory"><span>29</span> <b>17</b></div>
				<div class="luteal"><span>30</span> <b>18</b></div>
				
				<div class="luteal"><span>31</span> <b>19</b></div>
				<div class="luteal"><span>1</span> <b>20</b></div>
				<div class="luteal"><span>2</span> <b>21</b></div>
				<div class="luteal"><span>3</span> <b>22</b></div>
				<div class="luteal"><span>4</span> <b>23</b></div>
				<div class="luteal"><span>5</span> <b>24</b></div>
				<div class="luteal"><span>6</span> <b>25</b></div>
	
				<div class="luteal"><span>7</span> <b>26</b></div>
				<div class="luteal"><span>8</span> <b>27</b></div>
				<div>9</div>
				<div>10</div>
				<div>11</div>
				<div>12</div>
				<div>13</div>
				
				<div>14</div>
				<div>15</div>
				<div>16</div>
				<div>17</div>
				<div>18</div>
				<div>19</div>
				<div>20</div>
			</div>
		</div>
	
		<div class="service-ovulation-calculator__calculation-best">
			<div class="service-ovulation-calculator__calculation-best-title">При благоприятном зачатии в этом цикле предполагаемая дата родов</div>
			<div class="service-ovulation-calculator__calculation-best-date">19 апреля 2018</div>
		</div>
	
		<div class="service-ovulation-calculator__calculation-legend">
			<div class="service-ovulation-calculator__calculation-legend-menstrual">Менструальная фаза</div>
			<div class="service-ovulation-calculator__calculation-legend-follicular">Фолликулярная фаза</div>
			<div class="service-ovulation-calculator__calculation-legend-ovulatory">Овуляторная фаза</div>
			<div class="service-ovulation-calculator__calculation-legend-luteal">Лютеиновая фаза</div>
		</div>
		
		<div class="service-ovulation-calculator__calculation-periods">
			<div class="service-ovulation-calculator__calculation-periods-title">Благоприятные периоды для зачатия:</div>
			<div class="service-ovulation-calculator__calculation-period">c <b>23</b> сентября по <b>29</b> сентября 2017</div>
			<div class="service-ovulation-calculator__calculation-period">c <b>20</b> октября по <b>26</b> октября 2017</div>
			<div class="service-ovulation-calculator__calculation-period">c <b>16</b> ноября по <b>22</b> ноября 2017</div>
		</div>
	
	</div>



	<div class="mlr-16 mb-20">
		<p><span class="text-menstrual">Менструальная фаза</span> начинается в&nbsp;первый день менструации. В&nbsp;это время происходит отторжение эндометрия матки (кровянистые выделения), ваш организм готовится к&nbsp;появлению новой яйцеклетки.</p>
		<p><span class="text-follicular">Фолликулярная фаза</span> длится в&nbsp;среднем около 10&nbsp;дней. В&nbsp;этот период происходит созревание яйцеклетки в&nbsp;яичнике. Это происходит под воздействием специального импульса, и&nbsp;в&nbsp;яичники поступает фолликулостимулирующий гормон. Так, новая яйцеклетка созревает в&nbsp;доминантном (самом главном) фолликуле.</p>
		<p><span class="text-ovulatory">Овуляторная</span> фаза&nbsp;&mdash; время, когда закончено созревание яйцеклетки. Происходит её&nbsp;высвобождение из&nbsp;фолликула. Она постепенно перемещается в&nbsp;фаллопиевы трубы и&nbsp;находится там в&nbsp;среднем 48&nbsp;часов&nbsp;&mdash; в&nbsp;ожидании оплодотворения. Это время считается благоприятным для овуляции, т.е. оплодотворения.</p>
		<p><span class="text-luteal">Лютеиновая</span> фаза&nbsp;&mdash; время, когда фолликул, из&nbsp;которого высвободилась ваша яйцеклетка, начинает вырабатывать прогестерон. Гормон подготавливает эндометрий матки для так называемой имплантации оплодотворённой яйцеклетки (так знаменуется начальный этап беременности). Яйцеклетка попадает в&nbsp;полость матки и&nbsp;прикрепляет там. Начинает вырабатываться специальный гормон&nbsp;&mdash; хорионический гонадотропин, или ХГЧ. Именно его показатель свидетельствует о&nbsp;наступившей беременности. Если оплодотворения не&nbsp;произошло, то&nbsp;яйцеклетка и&nbsp;фолликул отмирают, эндометрий разрушается и&nbsp;цикл начинается заново&nbsp;&mdash; с&nbsp;первым днём менструации.</p>
	</div>


	<div class="mlr-16 mb-20">
		<h2 class="mb-20">Девочка или мальчик?</h2>

		<p>Зная свои дни овуляции, можно увеличить шансы для зачатия ребенка определенного пола! Узнайте свои благоприятные дни для зачатия девочки или мальчика.</p>
	</div>


	<div class="service-ovulation-calculator__howto-conceive mb-40">
		<div class="service-ovulation-calculator__howto-conceive-girl"><a href="#" class="btn btn-girl btn-shaded block">Как зачать девочку</a></div>
		<div class="service-ovulation-calculator__howto-conceive-boy"><a href="#" class="btn btn-boy btn-shaded block">Как зачать мальчика</a></div>
	</div>


	<div class="mlr-16 mb-40">
		<h2 class="mb-20">Как правильно рассчитать день овуляции</h2>

		<h3 class="mb-20">Считаем сами</h3>

		<p>&laquo;Измерьте&raquo; продолжительность своего менструального цикла&nbsp;&mdash; интервал от&nbsp;первого дня начала менструации
		до&nbsp;дня начала следующих месячных. Отнимите число 14&nbsp;от&nbsp;количества дней вашего цикла. К&nbsp;примеру: продолжительность цикла 28&nbsp;дней.
		Вычитаем: 28-14=14. Четырнадцатый день приходится на&nbsp;овуляцию.</p>

		<h3 class="mb-20">Узнаем базальную температуру</h3>

		<p>А&nbsp;ещё высчитать день овуляции вам поможет специальный &laquo;температурный&raquo; график. Следить за&nbsp;его показателями нужно, как минимум,
		три месяца. <span class="text-active">По&nbsp;данным базальной температуры перед овуляцией, во&nbsp;время неё</span> и&nbsp;<span class="text-active">базальной температурой
		после овуляции</span> можно &laquo;поймать&raquo;
		благоприятные дни для зачатия.</p>
	</div>

	<h2 class="mb-20">Что вам надо знать об овуляции</h2>

	<div class="articles mb-40">
		<div class="articles__list">
			<div class="article">
				<div class="article__image">
					<a href="#"><img src="img/tmp/article6.jpg"></a>
				</div>
				
				<div class="article__content">
					<a href="#" class="article__category">Овуляция</a>
					<a href="#" class="article__title">8 признаков овуляции, которые можно увидеть и почувствовать</a>
				</div>

				<div class="icon-group article__icons">
					<a href="#" class="icon-info icon-info-views">120</a>
					<a href="#" class="icon-info icon-info-comments">3</a>
					<a href="#" class="icon-info icon-info-likes">3</a>
				</div>
			</div>
		</div>
	</div>

	<h2 class="mb-20">5 фактов об овуляции</h2>

	<div class="service-ovulation-calculator__facts mb-40">
		<div class="service-ovulation-calculator__fact-1">При овуляции мозг работает активней</div>
		<div class="service-ovulation-calculator__fact-2">Ритм овуляции нарушается после 35&nbsp;лет</div>
		<div class="service-ovulation-calculator__fact-3">Яйцеклетка больше сперматозоида в&nbsp;85000 раз</div>
		<div class="service-ovulation-calculator__fact-4">На&nbsp;14&nbsp;день цикла усиливается половое влечение и&nbsp;&laquo;тянет&raquo; живот</div>
		<div class="service-ovulation-calculator__fact-5">Для определения овуляции существует минимум 4&nbsp;специальных теста!</div>
	</div>

	
</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>