<div class="profile profile-city scroll-top">
	<h1 class="mb-10">Город</h1>

	<div class="profile__step">Шаг 2 из 4</div>
	
	<div class="profile__description">Указание города поможет найти друзей, живущих рядом</div>
	
		
	<form action="#" method="post" class="profile__form">
		<div class="profile-city__city-popup">
			<div class="close"></div>
			<div class="profile-city__city-popup-header">Город</div>
			<ul>
				<li><a href="#">Алматы</a></li>
				<li><a href="#">Барнаул</a></li>
				<li><a href="#">Белгород</a></li>
			</ul>
		</div>
	
		<input type="text" name="city" value="" placeholder="Начните вводить город" class="profile__search-city">
	
		<div class="profile-city__city-popular">
			<a href="#">Москва</a>
			<a href="#">Санкт-Петербург</a>
			<a href="#">Новосибирск</a>
			<a href="#">Екатеринбург</a>
		</div>
	
		<button class="btn btn-confirm btn-shaded w-100 mb-20">Сохранить и продолжить</button>

		<button class="btn btn-active-flat btn-center">Пропустить шаг</button>
	</form>

</div>

<script>
$('.profile-city__city-popup')
.on('ds-on', function() {
	//$('body').addClass('overlayed');
	$(this).addClass('active');
	
})
.on('ds-off', function() {
	//$('body').removeClass('overlayed');
	$(this).removeClass('active');
})
.find('.close')
.on('click', function(event) {
	event.preventDefault();
	$(this).parents('.profile-city__city-popup').trigger('ds-off');
	
});
	
$('.profile__search-city').on('keydown change', function() {
	$('.profile-city__city-popup').trigger('ds-on');
	$(this).trigger('blur');
	
});
	
</script>