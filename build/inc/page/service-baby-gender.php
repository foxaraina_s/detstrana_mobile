
<section class="service service-gender scroll-top">
    <div class="service__subtitle"><a href="#">Сервисы</a> <i>❯</i> Определение пола</div>
    <h2 class="service__title">Определение пола ребенка</h2>
    <div class="service__text">Используйте 4 популярных способа, чтобы узнать пол вашего ребенка</div>
    <div class="gender__choose gender__choose--main">
        <div class="gender__choose--wrapper">
            <div class="gender__choose--title">
                <div>4</div>
                <div>способа узнать</div>
                <div>пол ребенка</div>
            </div>
        </div>
        <div class="gender__choose--methods">
            <a href="service-baby-gender--blood-refresh.html" class="method--item blood__refresh">
                По обновлению крови
            </a>
            <a href="service-baby-gender-blood-group.html" class="method--item blood__group">
                По группе крови родителей
            </a>
            <a href="service-baby-gender-japan.html" class="method--item japanese__table">
                Японский метод
            </a>
            <a href="service-baby-gender-chinise.html" class="method--item chinese_table">
                Китайский метод
            </a>
        </div>
    </div>
    <div class="service__text">Девочка или мальчик? Этим вопросом озадачены многие будущие родители. Вы тоже хотите
        узнать, какой пол будет у ребёнка? Как выявить его со всей точностью? Воспользуйтесь нашим сервисом, благодаря
        которому вы сможете без труда определить пол будущего малыша. Выбирайте, какой вариант вам ближе: китайские или
        японские таблицы для определения пола, или вы хотели бы рассчитать пол ребенка по обновлению крови? Кстати, наши
        прабабушки и прадедушки тоже пытались прогнозировать, кто появится на свет – маленькая принцесса или мальчуган.
        И со всей тщательностью подходили к выбору даты зачатия. А еще вокруг вопроса о том, кто же родится у мамы и
        папы, сформировалось множество суеверий и примет. Мы вам предлагаем методы определения пола ребёнка (например,
        по таблицам), которыми воспользовались уже тысячи родителей. Попробуйте один из четырех предложенных вариантов и
        не забудьте поделиться с нами своим результатом!
    </div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->