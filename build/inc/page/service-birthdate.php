
<div id="popup-date" class="popup popup-date popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Выберите дату</div>
    <div class="popup__body">
        <div class="popup-date__calendar">
            <div class="popup-date__calendar-header">
                <a href="#" class="popup-date__calendar-rewind"></a>
                <a href="#" class="popup-date__calendar-forward"></a>
                <div class="popup-date__calendar-title">Ноябрь 2017</div>
            </div>

            <div class="popup-date__calendar-weekdays">
                <div>Пн</div>
                <div>Вт</div>
                <div>Ср</div>
                <div>Чт</div>
                <div>Пт</div>
                <div>Сб</div>
                <div>Вс</div>
            </div>

            <div class="popup-date__calendar-days">
                <div></div>
                <div></div>
                <div>1</div>
                <div>2</div>
                <div>3</div>
                <div>4</div>
                <div>5</div>
                <div>6</div>
                <div>7</div>
                <div>8</div>
                <div>9</div>
                <div>10</div>
                <div>11</div>
                <div>12</div>
                <div>13</div>
                <div>14</div>
                <div>15</div>
                <div>16</div>
                <div>17</div>
                <div>18</div>
                <div>19</div>
                <div>20</div>
                <div>21</div>
                <div>22</div>
                <div>23</div>
                <div>24</div>
                <div>25</div>
                <div>26</div>
                <div>27</div>
                <div>28</div>
                <div>29</div>
                <div>30</div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>
<section class="service service-birthcalendar scroll-top">
    <ul class="page-breadcrumbs">
        <li><a href="#">Сервисы</a></li>
        <li><span>Расчет даты родов и график обследований</span></li>
    </ul>
    <h1 class="mb-20">Калькулятор даты<br> родов и график<br> обследований при беременности</h1>
    <div class="service__text">
        <p>
            Для того чтобы ваша беременность стала еще комфортней, мы создали сервис, помогающий рассчитать дату
            появления на свет малыша. А также калькулятор позаботится о том, чтобы вы не пропустили основные этапы
            беременности и вовремя прошли необходимые обследования.
        </p>
        <p>
            В основе калькулятора – формула Негеле, которая применяется врачами-гинекологами и акушерами.
            Воспользоваться ею легко и просто. Введите в окно дату – первый день вашего последнего менструального цикла.
            С помощью формулы вы узнаете предположительную дату ваших родов!
        </p>
    </div>
    <h3 class="mb-20 service__section-title">Рассчитать дату родов и <br> срок беременности</h3>
    <div class="service__text">
        <p>
            Мы знаем, что будущие мамы – очень любознательные. И, несмотря на то, что врач-гинеколог уже на первом
            приеме по беременности определяет предварительную дату рождения малыша, вам всегда интересно самостоятельно
            рассчитать ПДР..
        </p>
        <p>
            Помощником в этом станет календарь беременности, рассчитать дату родов при помощи которого очень легко –
            лишь указав первый день вашего менструального цикла.
        </p>
        <p>
            А еще календарь беременности – это кладезь информации о каждой неделе беременности.
        </p>
    </div>
    <div class="pregnancy-count--shadow pregnancy-count">
        <h2 class="mb-20">Чтобы узнать предположительную дату родов и срок беременности,</h2>
        Уточните первый день последней менструации

        <div class="pregnancy-count__form mt-20">
            <form method="get">
                <div class="form-group mb-20">
                    <div class="form-select-wrapper"><a href="#" class="form-select" data-target="#popup-date">Выберите
                        дату</a></div>
                </div>
                <input type="submit" value="Рассчитать" class="btn btn-confirm btn-shaded w-100">
                <div class="text-sm text-light mt-20">Ваш срок беременности:</div>
                <div class="text-center mb-20">
                    <div class="pregnancy-count__result pregnancy-count__result--week">
                        <div class="num">39</div>
                        <div class="text">недель</div>
                    </div>
                    <div class="pregnancy-count__result--devide">
                    </div>
                    <div class="pregnancy-count__result pregnancy-count__result--month">
                        <div class="num">9</div>
                        <div class="text">месяцев</div>
                    </div>
                </div>
            </form>
            <div class="text-sm text-light">Примерная дата родов:</div>
            <div class="pregnancy-count__date-result">неизвестно</div>
            <div><em>Узнайте все самое важное о вас и малыше!</em></div>
        </div>
    </div>
    <div class="birthcalendar-diagnostic">
        <div class="service__text mb-30">
            Мы рассчитали для вас график обследований
        </div>
        <h2 class="mb-20">Календарь анализов и обследований при беременности</h2>
        <div class="accordion">
            <div class="accordion__item">
                <div class="accordion__title">
                    I триместр (с 1 по 13 неделю)
                    <div class="accordion__title-text--sm">с 30 марта 2017 по 28июня 2017 г</div>
                </div>
                <div class="accordion__content">
                    <table class="table diagnostic-table">
                        <tbody>
                        <tr>
                            <td>Статус</td>
                            <td>Анализы и обследования</td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[5]" value="1">
                                    <label for="diag[5]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Прием у гинеколога</div>
                                <div class="diagnostic-table__time">на 28 неделе беременности</div>
                                <div class="diagnostic-table__list">Измерение давления, взвешивание, измерение высоты стояния дна матки и объем живота,
                                    пульса и
                                    температуры тела, положения и предлежания плода.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[6]" value="1">
                                    <label for="diag[6]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Оформление дородового отпуска</div>
                                <div class="diagnostic-table__time">на 30 неделе беременности</div>
                                <div class="diagnostic-table__list">Наступило время оформлять декретный отпуск и готовиться к предстоящим родам</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[7]" value="1">
                                    <label for="diag[7]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Прием у гинеколога</div>
                                <div class="diagnostic-table__time">на 28 неделе беременности</div>
                                <div class="diagnostic-table__list">Измерение давления, взвешивание, измерение высоты стояния дна матки и объем живота,
                                    пульса и
                                    температуры тела, положения и предлежания плода.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[8]" value="1">
                                    <label for="diag[8]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Оформление дородового отпуска</div>
                                <div class="diagnostic-table__time">на 30 неделе беременности</div>
                                <div class="diagnostic-table__list">Наступило время оформлять декретный отпуск и готовиться к предстоящим родам</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="accordion__item">
                <div class="accordion__title">
                    II триместр (с 14 по 28 неделю)
                    <div class="accordion__title-text--sm">с 30 марта 2017 по 28июня 2017 г</div>
                </div>
                <div class="accordion__content">
                    <table class="table diagnostic-table">
                        <tbody>
                        <tr>
                            <td>Статус</td>
                            <td>Анализы и обследования</td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[9]" value="1">
                                    <label for="diag[9]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Прием у гинеколога</div>
                                <div class="diagnostic-table__time">на 28 неделе беременности</div>
                                <div class="diagnostic-table__list">Измерение давления, взвешивание, измерение высоты стояния дна матки и объем живота,
                                    пульса и
                                    температуры тела, положения и предлежания плода.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[10]" value="1">
                                    <label for="diag[10]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Оформление дородового отпуска</div>
                                <div class="diagnostic-table__time">на 30 неделе беременности</div>
                                <div class="diagnostic-table__list">Наступило время оформлять декретный отпуск и готовиться к предстоящим родам</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[11]" value="1">
                                    <label for="diag[11]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Прием у гинеколога</div>
                                <div class="diagnostic-table__time">на 28 неделе беременности</div>
                                <div class="diagnostic-table__list">Измерение давления, взвешивание, измерение высоты стояния дна матки и объем живота,
                                    пульса и
                                    температуры тела, положения и предлежания плода.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[12]" value="1">
                                    <label for="diag[12]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Оформление дородового отпуска</div>
                                <div class="diagnostic-table__time">на 30 неделе беременности</div>
                                <div class="diagnostic-table__list">Наступило время оформлять декретный отпуск и готовиться к предстоящим родам</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="accordion__item">
                <div class="accordion__title">
                    III триместр (с 29 по 40 неделю)
                    <div class="accordion__title-text--sm">с 30 марта 2017 по 28июня 2017 г</div>
                </div>
                <div class="accordion__content">
                    <table class="table diagnostic-table">
                        <tbody>
                        <tr>
                            <td>Статус</td>
                            <td>Анализы и обследования</td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[1]" value="1">
                                    <label for="diag[1]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Прием у гинеколога</div>
                                <div class="diagnostic-table__time">на 28 неделе беременности</div>
                                <div class="diagnostic-table__list">Измерение давления, взвешивание, измерение высоты стояния дна матки и объем живота,
                                    пульса и
                                    температуры тела, положения и предлежания плода.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[2]" value="1">
                                    <label for="diag[2]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Оформление дородового отпуска</div>
                                <div class="diagnostic-table__time">на 30 неделе беременности</div>
                                <div class="diagnostic-table__list">Наступило время оформлять декретный отпуск и готовиться к предстоящим родам</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[3]" value="1">
                                    <label for="diag[3]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Прием у гинеколога</div>
                                <div class="diagnostic-table__time">на 28 неделе беременности</div>
                                <div class="diagnostic-table__list">Измерение давления, взвешивание, измерение высоты стояния дна матки и объем живота,
                                    пульса и
                                    температуры тела, положения и предлежания плода.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="block-checkbox block-checkbox--center">
                                    <input type="checkbox" name="no-children" id="diag[4]" value="1">
                                    <label for="diag[4]"><span>до 12 июня</span></label>
                                </div>
                            </td>
                            <td>
                                <div class="diagnostic-table__type">Оформление дородового отпуска</div>
                                <div class="diagnostic-table__time">на 30 неделе беременности</div>
                                <div class="diagnostic-table__list">Наступило время оформлять декретный отпуск и готовиться к предстоящим родам</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
        <div class="inner-block inner-block--shadow mt-20 mb-20">
            <div class="inner-block__doctor">
                <div class="inner-block__doctor-image">
                    <img src="img/birthcalendar/dc1.png" style="width: 64px;">
                </div>
                <div class="inner-block__doctor-content">
                    <div class="inner-block__doctor-title">
                        Ирина Тимошина
                    </div>
                    <div class="inner-block__doctor-text">
                        акушер-гинеколог, научный сотрудник 1-го акушерского отделения патологии беременности, ФГБУ
                        «Научный центр акушерства, гинекологии и перинатологии им. В.И. Кулакова» Минздрава России
                    </div>
                </div>
            </div>
        </div>
        <div class="service__text">
            Подробнее узнать как рассчитать дату родом можно в нашей <a href="#"> статье</a>
        </div>
        <div class="block block__also-see mt-20">
            <h2 class="mb-20">Смотрите также</h2>
            <div class="service__list">
                <div class="service__item">
                    <a href="#">
                        <div class="service__item--mark">Планирование</div>
                        <div class="service__item--mark">Беременность</div>
                        <div class="service__item--image"><img src="img/birthcalendar/in4.jpg"> </div>
                        <div class="service__item--category">Роды</div>
                        <div class="service__item--title">Подскажите</div>
                        <div class="service__item--content">
                           На второй день после зачатия поднялась температура.
                        </div>
                    </a>
                </div>
            </div>
        </div>



    </div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->