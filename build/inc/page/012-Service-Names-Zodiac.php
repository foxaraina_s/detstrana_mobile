<div class="service service-names scroll-top">
<?php
	$title = 'Январь&nbsp;&mdash; выбор имени по святцам для ребёнка';

	include 'inc/block/services/names/header.php';

?>
	<form class="service-names__form-choose">
		<div class="form-group mb-20">
			<label>Выберите знак зодиака:</label>
			<select name="zodiac" placeholder="Все" class="form-select">
				<option>Все</option>
				<option value="aries">Овен</option>
				<option value="taurus">Телец</option>
				<option value="gemini">Близнецы</option>
				<option value="cancer">Рак</option>
				<option value="leo">Лев</option>
				<option value="virgo">Дева</option>
				<option value="libra" selected>Весы</option>
				<option value="scorpio">Скорпион</option>
				<option value="sagittarius">Стрелец</option>
				<option value="capricorn" selected>Козерог</option>
				<option value="aquarius">Водолей</option>
				<option value="pisces">Рыбы</option>
			</select>
		</div>
	</form>

<?php

	include 'inc/block/services/names/table.php';

	include 'inc/block/services/names/text.php';
?>
</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>