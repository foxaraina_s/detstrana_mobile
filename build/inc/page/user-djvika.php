
<div class="content content-users scroll-top pt-20">
    <ul class="page-breadcrumbs">
        <li><a href="#">Сервисы</a></li>
        <li><span>Определение пола</span></li>
    </ul>
    <div class="users">
        <div class="users__list mb-20">
            <div class="user user-online user-active">
                <div class="user__header clickable">
                    <div class="user__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="user__name">Рада Мельникова</div>
                    <div class="user__about">
                        День рождения: 24 сентября<br>
                        Город: Сургут
                    </div>
                </div>
                <div class="user__body">
                    <div class="user__body-inner">
                        <div class="text-grey text-caption-small mb-10">Дети:</div>
                        <div class="user__children">
                            <a href="#" class="user__child user__child--girl">9 лет</a>
                            <a href="#" class="user__child user__child--boy">3 мес.</a>
                        </div>
                        <div class="user__about mb-20">
                            <div class="text-grey text-caption-small mb-10">Обо мне:</div>
                            <div class="user__about--text">Счастливая мама двух прекрасных сыночков.</div>
                        </div>
                        <a href="#" class="btn btn-confirm btn-shaded w-100 mb-20">Написать сообщение</a>
                        <a href="#" class="btn btn-confirm btn-shaded w-100">Добавить в друзья</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slider nav-tabs-slider mb-20 block-shaded">
            <div class="slider-wrapper slider-changeable">
                <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100">
                    <li class="slide w-50 active"><a href="#">Записи (250)</a></li>
                    <li class="slide w-50"><a href="#">Друзья (315)</a></li>
                    <li class="slide w-50"><a href="#">Подписчики</a></li>
                    <li class="slide w-50"><a href="#">Сообщества</a></li>
                    <li class="slide w-50"><a href="#">Альбомы</a></li>
                </ul>
            </div>
    </div>
    <form class="form-search mb-20">
        <input type="text" name="city" value="" placeholder="Поиск записей в блоге">
    </form>
    <button data-target="#popup-user-filter" class="btn btn-popup btn-white mb-40">Показать фильтр</button>
    <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-scrollable block-shaded w-100 mb-20">
        <li class="w-50 active"><a href="#">Записи в дневнике</a></li>
        <li class="w-50"><a href="#">Записи из сообществ</a></li>
    </ul>
    <div class="entries">
        <div class="entries__list">
            <div class="entry mb-10">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">120</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">1</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:58</div>
                </div>

                <div class="entry__title">
                    Гиперлактация
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides">
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div class="entries__morecomments">
            <button class="btn btn-active-flat w-100">Еще 15 комментариев</button>
        </div>
    </div>
    <div class="comments">

        <div class="comments__list">

            <div class="comment">

                <div class="comment__header">
                    <div class="comment__author">Виктория Кудрявцева</div>
                    <div class="comment__date">31 мая 2017 года, 12:58</div>
                    <div class="comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>

                <div class="comment__body">
                    <div class="comment__inner">
                        <div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
                    </div>

                    <div class="comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin comment__comment-close"></button>
                        </div>

                        <form class="comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>

            <div class="comment">

                <div class="comment__header">
                    <div class="comment__author">Виктория Кудрявцева</div>
                    <div class="comment__date">31 мая 2017 года, 12:58</div>
                    <div class="comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>

                <div class="comment__body">
                    <div class="comment__inner">

                        <div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>

                        <div class="slider comment__slider">
                            <div class="slider-changeable slider-wrapper">
                                <div class="slides">
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                    <div class="slide">
                                        <img src="img/tmp/article5.jpg">
                                        <img src="img/tmp/article5.jpg">
                                    </div>
                                </div>
                            </div>

                            <ul class="nav nav-dotted nav-dotted-small slider-control slider-changeable">
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin comment__comment-close"></button>
                        </div>

                        <form class="comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <div class="rel mb-10">
                                <button class="btn btn-confirm w-66">Комментировать</button>
                                <button class="btn btn-plus pull-right"></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <div class="comment">

                <div class="comment__header">
                    <div class="comment__author">Катя медунцева <span>ответила</span></div>
                    <div class="comment__author">Виктория Кудрявцева</div>
                    <div class="comment__date">31 мая 2017 года, 12:58</div>
                    <div class="comment__icons">
                        <a href="#" class="icon-info icon-info-likes-dark">5</a>
                    </div>
                </div>

                <div class="comment__body">
                    <div class="comment__inner">
                        <div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
                    </div>

                    <div class="comment__comment active">
                        <div class="rel mb-20">
                            <button class="btn btn-flat comment__comment-like">Нравится</button>
                            <button class="btn btn-active-flat comment__comment-open">Ответить</button>
                            <button class="btn btn-close-thin comment__comment-close"></button>
                        </div>

                        <form class="comment__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <div class="entries">
        <div class="entries__list">
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:56</div>
                </div>

                <div class="entry__title">
                   Гиперлактация
                </div>

                <div class="entry__body pt-0">

                    <div class="entry__text">
                        Добрый день. Ребенку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">31</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">5</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="text-center mt-20 mb-40"><button class="btn btn-active-flat">Больше записей</button></div>

    </div>



</div>


<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->