<div id="popup-month" class="popup popup-services popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Выберите месяц</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Месяц 1</a></li>
            <li><a href="#">Месяц 2</a></li>
            <li><a href="#">Месяц 3</a></li>
            <li><a href="#">Месяц 4</a></li>
            <li><a href="#">Месяц 5</a></li>
            <li><a href="#">Месяц 6</a></li>
            <li><a href="#">Месяц 7</a></li>
            <li><a href="#">Месяц 8</a></li>
            <li><a href="#">Месяц 9</a></li>
        </ul>
    </div>
</div>
<div id="popup-content" class="popup popup-services popup-full">
    <div class="popup__close"></div>
    <div class="popup__header">Содержание</div>
    <div class="popup__body">
        <ul class="popup__links">
            <li><a href="#">Раздел 1</a></li>
            <li><a href="#">Раздел 2</a></li>
            <li><a href="#">Раздел 3</a></li>
            <li><a href="#">Раздел 4</a></li>
            <li><a href="#">Раздел 5</a></li>
            <li><a href="#">Раздел 6</a></li>
            <li><a href="#">Раздел 7</a></li>
            <li><a href="#">Раздел 8</a></li>
            <li><a href="#">Раздел 9</a></li>
        </ul>
    </div>
</div>
<section class="service service-birthcalendar scroll-top">
    <ul class="page-breadcrumbs">
        <li class="back"><a href="#">Вернуться в календарь беременности</a></li>
    </ul>
    <h1 class="mb-40">1 месяц беременности</h1>
    <div class="mb-20">
        <button data-target="#popup-month" class="btn btn-popup btn-white">Выберите месяц</button>
    </div>
    <div class="mb-20">
        <button data-target="#popup-content" class="btn btn-popup btn-white">Содержание</button>
    </div>

    <div class="birthcalendar-inner">

        <div class="slider birthcalendar-inner__slider">
            <div class="slider-changeable slider-wrapper">
                <div class="slides">
                    <div class="slide birthcalendar-inner__slide">
                        <div class="birthcalendar-inner__slide-img">
                            <img src="img/birthcalendar/wk.png" style="max-width: 50%;">
                        </div>
                        <div class="birthcalendar-inner__slide-title">
                            1 неделя беременности
                        </div>

                    </div>
                    <div class="slide birthcalendar-inner__slide">
                        <div class="birthcalendar-inner__slide-img">
                            <img src="img/birthcalendar/wk.png" style="max-width: 50%;">
                        </div>
                        <div class="birthcalendar-inner__slide-title">
                            1 неделя беременности
                        </div>

                    </div>
                    <div class="slide birthcalendar-inner__slide">
                        <div class="birthcalendar-inner__slide-img">
                            <img src="img/birthcalendar/wk.png" style="max-width: 50%;">
                        </div>
                        <div class="birthcalendar-inner__slide-title">
                            1 неделя беременности
                        </div>

                    </div>
                    <div class="slide birthcalendar-inner__slide">
                        <div class="birthcalendar-inner__slide-img">
                            <img src="img/birthcalendar/wk.png" style="max-width: 50%;">
                        </div>
                        <div class="birthcalendar-inner__slide-title">
                            1 неделя беременности
                        </div>

                    </div>
                </div>
            </div>
            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>

    </div>
    <div class="inner-content mb-20">
        <div class="service__text mb-20">
            1 месяц беременности — это самое начало первого триместра. 1 месяц длится с первой недели беременности и
            немного захватывает пятую. В этот период большинство будущих мамочек даже не догадываются о своём интересном
            положении, потому что никаких видимых признаков беременности пока нет. Впрочем, к концу первого месяца
            представительница прекрасного пола уже может смело приобретать домашний тест и радоваться двум розовым
            полосочкам — долгожданным и ненаглядным.
        </div>
        <div class="img__block text-center mb-20">
            <img src="img/birthcalendar/in2.jpg" class="img-responsive">
        </div>
        <div class="service__text">
            <p>Самым надёжным и точным признаком беременности на столь раннем сроке является именно тест. Если вы не
                верите одному тесту, можете сделать второй и третий. Но помните: тест стоит приобретать только тогда,
                когда вы поймёте, что месячные задержались на четыре дня и более.
            </p>
            <p>Впрочем, тест — не единственный предвестник интересного положения. Многие мамочки отмечают, что в течение
                первого месяца они ощущали некоторую слабость, им постоянно хотелось спать, а грудь становилась гораздо
                чувствительней, чем раньше. В некоторых случаях повышенная выработка “беременного” гормона прогестерона
                провоцирует потемнение сосков и ореол, однако эти изменения весьма индивидуальны. <a href="#">Больше о
                    признаках
                    беременности, которые проявляются сразу после овуляции, можно узнать тут.</a>
            <p>Но давайте не забывать о том, что головные боли, головокружения, слабость и сонливость могут быть не
                только признаками зарождения новой жизни. Поэтому предлагаем вам доверять лишь проверенным
                диагностическим методам.
            </p>
            <h3 class="mb-20 service__section-title">Тест на беременность</h3>
            <p>
                Итак, как мы уже сказали выше, если менструация задержалась на срок от трёх до пяти дней, пришло самое
                время делать тест. Он представляет собой тоненькую полоску в пластиковом обрамлении или без него.
                Достаточно просто опустить полосочку в мочу и немного подождать. Спустя указанное на упаковке время вы
                уже сможете созерцать результат. Две ярко выраженные красные полосочки говорят о том, что вскоре вы
                станете мамой. Искренне вас поздравляем с этим потрясающим событием! <a href="#">Здесь вы сможете найти
                обзор тестов
                на беременность.</a>
            </p>
        </div>
    </div>

    <div class="slider nav-tabs-slider mb-20 block-shaded">
        <div class="slider-wrapper slider-changeable">
            <ul class="nav nav-tabs nav-tabs-font--sm nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100"
                style="white-space: nowrap;">
                <li class="slide w-50 active"><a href="#">Твой малыш</a></li>
                <li class="slide w-50"><a href="#">Ты </a></li>
                <li class="slide w-50"><a href="#">Что ты делаешь?</a></li>
                <li class="slide w-50"><a href="#">Что ты думаешь?</a></li>
            </ul>
        </div>
    </div>
    <div class="nav-tabs-container mb-40">
        <div class="service__text mb-20">
            К концу первого месяца ваш малыш, который для врачей пока что является эмбрионом, достигает в длину четырёх
            сантиметров. Совсем малютка, правда? Но этот месяц является чуть ли ни решающим для его развития.
            <div class="inner-list">
                <ul>
                    <li>В течение первой недели яйцеклетка, которая была оплодотворена сперматозоидом, передвигается по
                        маточной трубе в полость матки. Попутно сама яйцеклетка делится, а после, уже оказавшись в маточной
                        полости, ещё около трёх дней продвигается к
                    </li>
                    <li>На второй неделе беременности внешний слой зародыша начинает выработку хорионического гонадотропина.
                        ХГЧ говорит материнскому организму о том, что пришло время гормональной перестройки, потому что
                        внутри него зародилась новая жизнь. Яйцеклетка уже не способна питать зародыш, и эта функция
                        переходит к жёлтому телу. Вскоре и жёлтое тело себя исчерпает, передав важнейшую работу плаценте и
                        пуповине, которые пока ещё не
                    </li>
                    <li>На третьей неделе происходит закладка хорды зародыша, формируются так называемые листки, из которых
                        чуточку позже начнут формироваться внутренние органы и системы. К 22 дню вашей беременности
                        эмбриончик уже имеет сердце, которое бьётся и качает кровь, подобно насосу.
                    </li>
                    <li>
                        На четвёртой неделе малыш похож на человеческое ухо. Его окружает амниотическая жидкость в небольшом
                        количестве. У крошки формируются глазные яблоки и зачатки нижних и верхних конечностей.
                    </li>
                    <li>
                        Тельце эмбриона мягкое на ощупь и практически прозрачное. Однако хвостик и голова уже вполне
                        различимы.
                    </li>
                </ul>
            </div>
        </div>

    </div>
    <h3 class="mb-20 service__section-title">На 1 триместре беременности полезно:</h3>
    <ul class="nav nav-tabs nav-tabs-bordered w-100 mb-10 nav-tabs-font--sm">
        <li class="w-50 active"><a href="#">Читать блоги</a></li>
        <li class="w-50"><a href="#">Читать консультации</a></li>
    </ul>
    <div class="entries mb-40">
        <div class="entries__list mb-20">
            <div class="entry">

                <div class="entry__header">
                    <div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
                    <div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
                    <div class="entry__date">31 мая 2017 года, 12:58</div>
                </div>

                <div class="entry__title">
                    Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
                </div>

                <div class="slider entry__images">
                    <div class="slider-changeable slider-wrapper">
                        <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                            <div class="slide active"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                            <div class="slide"><img src="img/tmp/article2.jpg"></div>
                        </div>
                    </div>

                    <ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
                        <li class="active"><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>

                <div class="entry__body">

                    <div class="entry__text">
                        Добрый день. Ребёнку 1 неделя, сосёт хорошо по 20-30мин и потом спит 2-3 часа, последние 2 ночи вообще по 5
                    </div>

                    <p><a href="#" class="entry__readmore">Читать далее..</a></p>

                    <div class="icon-group entry__icons">
                        <a href="#" class="icon-info icon-info-views">120</a>
                        <a href="#" class="icon-info icon-info-comments">3</a>
                        <a href="#" class="icon-info icon-info-likes">1</a>
                    </div>

                    <div class="icon-group entry__comment">
                        <div class="rel mb-10">
                            <button class="btn btn-confirm-flat w-100 entry__comment-open entry__comment-open--green ">Комментировать</button>
                            <button class="btn btn-close entry__comment-close"></button>
                        </div>

                        <form class="entry__comment-form">
                            <textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
                            <button class="btn btn-confirm btn-shaded w-100">Отправить</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="text-center">
            <button class="btn btn-active-flat">Больше записей</button>
        </div>
    </div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->