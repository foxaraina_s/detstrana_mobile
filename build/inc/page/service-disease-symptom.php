
<section class="service service-disease pt-20 scroll-top">
    <ul class="page-breadcrumbs">
        <li><a href="#">Сервисы</a></li>
        <li><span>Справочник болезней</span></li>
    </ul>
    <h1 class="mb-20">Справочник симптомов заболевания</h1>
    <form class="service__form-search">
        <input type="text" name="city" value="" placeholder="Поиск по болезням">
    </form>
    <div class="service__text">Укажите, для кого вы ищете</div>
    <ul class="service-disease__icons">
        <li class="service-disease__icon-pg active"><a href="#">Беременность</a></li>
        <li class="service-disease__icon-fm"><a href="#">Кормящие мамы</a></li>
        <li class="service-disease__icon-nch"><a href="#">Новорожденные<br>(0-4 нед.)</a></li>
        <li class="service-disease__icon-ch"><a href="#">Дети</a></li>
    </ul>
    <div class="slider nav-tabs-slider mb-20 block-shaded">
        <div class="slider-wrapper slider-changeable">
            <div class="nav-tabs-container slides">
                <ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large nav-tabs-scrollable w-100"
                    style="white-space: nowrap;">
                    <li class="slide w-50"><a href="#">По алфавиту</a></li>
                    <li class="slide w-50"><a href="#">По категория</a></li>
                    <li class="slide w-50 active"><a href="#">По симптомам</a></li>
                </ul>
                <div class="border"></div>
            </div>
        </div>
        <a href="#" class="slider-rewind"></a><a href="#" class="slider-forward"></a>
    </div>
    <ul class="filter-abc">

        <li><a href="#">А</a></li>

        <li><a href="#">Б</a></li>

        <li><a href="#">В</a></li>

        <li><a href="#">Г</a></li>

        <li><a href="#">Д</a></li>

        <li><a href="#">Е</a></li>

        <li><a href="#">Ж</a></li>

        <li><a href="#">З</a></li>

        <li><a href="#">И</a></li>

        <li><a href="#">К</a></li>

        <li><a href="#">Л</a></li>

        <li><a href="#">М</a></li>

        <li><a href="#">Н</a></li>

        <li><a href="#">О</a></li>

        <li><a href="#">П</a></li>

        <li><a href="#">Р</a></li>

        <li><a href="#">С</a></li>

        <li><a href="#">Т</a></li>

        <li><a href="#">У</a></li>

        <li><a href="#">Ф</a></li>

        <li><a href="#">Х</a></li>

        <li><a href="#">Ц</a></li>

        <li><a href="#">Ч</a></li>

        <li><a href="#">Ш</a></li>

        <li><a href="#">Щ</a></li>

        <li><a href="#">Э</a></li>

        <li><a href="#">Ю</a></li>

        <li><a href="#">Я</a></li>

    </ul>
    <table class="table table-striped table-arrowed w-100 mb-20">
        <tbody>
        <tr>
            <td><a href="#">Бёдра</a></td>
        </tr>
        <tr>
            <td><a href="#">Горло</a></td>
        </tr>
        <tr>
            <td><a href="#">Половые органы</a></td>
        </tr>
        <tr>
            <td><a href="#">Боли в процессе деятельности</a></td>
        </tr>
        </tbody>
    </table>
<div class="service__text">
    <p>Многие мамы обладают способностью выявлять признаки заболеваний и симптомы болезней у своего ребёнка прежде, чем
    малыш признается, что у него болит горло или поднялась температура. Действительно, чтобы помочь крохе и оказать ему
        первую помощь, важно знать, как по симптомам определить заболевание.Родители, специально для вас мы создали справочник с удобным поиском, где в один клик можно получить актуальную информацию. Диагностика заболеваний по
        симптомам позволит вам принимать оперативные меры для оперативного лечения.</p>
</div>
    <h3 class="mb-30 service__section-title">Темы дня</h3>
<div class="articles">
    <div class="articles__list">
        <div class="article">
            <div class="article__image">
                <a href="#"><img src="img/tmp/article1.jpg"></a>
            </div>

            <div class="article__content">
                <a href="#" class="article__category">Психология</a>
                <a href="#" class="article__title">В чем мама всегда виновата</a>
            </div>

            <div class="icon-group article__icons">
                <a href="#" class="icon-info icon-info-views">120</a>
                <a href="#" class="icon-info icon-info-comments">3</a>
                <a href="#" class="icon-info icon-info-likes">3</a>
            </div>
        </div>

        <div class="article">
            <div class="article__mark">Партнёрский материал</div>

            <div class="article__image">
                <a href="#"><img src="img/tmp/article2.jpg"></a>
            </div>

            <div class="article__content">
                <a href="#" class="article__title">8 признаков овуляции, которые можно увидеть или почуствовать</a>
            </div>

            <div class="icon-group article__icons">
                <a href="#" class="icon-info icon-info-views">120</a>
                <a href="#" class="icon-info icon-info-comments">3</a>
            </div>
        </div>

    </div>

    <div class="text-center"><button class="btn btn-active-flat">Больше статей</button></div>
</div>
</section>

<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
    <div class="pregnancy-calendar__header">
        Следите за ходом<br>
        беременности в личном<br>
        календаре
    </div>
    <div class="pregnancy-calendar__body">


        <div class="slider pregnancy-calendar__slider">
            <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
            <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

            <div class="slider-changeable pregnancy-calendar__title">I триместр</div>





            <div class="slider-changeable slider-wrapper">
                <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="slide active">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 1</a></li>
                            <li><a href="#">Месяц 2</a></li>
                            <li><a href="#">Месяц 3</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 4</a></li>
                            <li><a href="#">Месяц 5</a></li>
                            <li><a href="#">Месяц 6</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#">18</a></li>
                            <li><a href="#">19</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">21</a></li>
                            <li><a href="#">22</a></li>
                            <li><a href="#">23</a></li>
                            <li><a href="#">24</a></li>
                            <li><a href="#">25</a></li>
                            <li><a href="#">26</a></li>
                        </ul>
                    </div>

                    <div class="slide">
                        <ul class="pregnancy-calendar__months">
                            <li><a href="#">Месяц 7</a></li>
                            <li><a href="#">Месяц 8</a></li>
                            <li><a href="#">Месяц 9</a></li>
                        </ul>
                        <div class="pregnancy-calendar__week-title">Недели:</div>
                        <ul class="pregnancy-calendar__weeks">
                            <li><a href="#">27</a></li>
                            <li><a href="#">28</a></li>
                            <li><a href="#">29</a></li>
                            <li><a href="#">30</a></li>
                            <li><a href="#">31</a></li>
                            <li><a href="#">32</a></li>
                            <li><a href="#">33</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li><a href="#">36</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                            <li><a href="#">39</a></li>
                            <li><a href="#">40</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                <li class="active"><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /Pregnancy calendar -->