<div class="service service-children-goods-rating pt-20 scroll-top">
	<ul class="page-breadcrumbs">
		<li><a href="#">Сервисы</a></li>
		<li>Рейтинги товаров для детей</li>
	</ul>

	<h1 class="mb-20">Рейтинги и отзывы о товарах</h1>

	<div class="mlr-16 mb-20">Выбирайте товары в соответствии с рейтингом и отзывом. Делитесь мнением с другими родителями.</div>

	<button data-target="#popup-articles" class="btn btn-popup btn-white">Каталог товаров</button>


	<h2 class="mb-20">Лучшие товары рейтинга</h2>

	<div class="children-goods-rating">
		<div class="children-goods-rating__image">
			<img src="img/tmp/rating-1.jpg">
		</div>
		<a href="#" class="children-goods-rating__category">Солнцезащитные средства</a>
		<a href="#" class="children-goods-rating__title">Крем солнцезащитный Chicco SPF50+ Baby</a>
		<div class="children-goods-rating__rating mb-40">
			4.89
			<a href="#">41 отзыв</a>
		</div>

		<ul class="nav nav-dotted">
			<li class="active"><a href="#"></a></li>
			<li><a href="#"></a></li>
			<li><a href="#"></a></li>
			<li><a href="#"></a></li>
			<li><a href="#"></a></li>
		</ul>
	</div>

	<h2 class="mb-20">Производители товаров</h2>

	<ul class="nav nav-tags text-center mb-20">
		<li><a href="#">А-Я</a></li>
		<li><a href="#">A-Z</a></li>
	</ul>

	<table class="table table-striped table-arrowed w-100 mb-20">
		<tr><td><a href="#">Бренд..</a></td></tr>
		<tr><td><a href="#">Бренд..</a></td></tr>
		<tr><td><a href="#">Бренд..</a></td></tr>
	</table>
</div>

<?php include 'inc/block/pregnancy-calendar.php' ?>