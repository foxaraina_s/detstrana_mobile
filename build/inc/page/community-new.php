<div class=" content content-community scroll-top pt-20">
    <ul class="page-breadcrumbs">
        <li class="back"><a href="#">вернуться к сообществу</a></li>
    </ul>
    <h1 class="mb-20">Новая публикация для сообщества «Подготовка к зачатия»</h1>
    <div class="mlr-16 mb-20">
        <p>Вы не соостоите в сообществе «Подготовка к зачатию». С публикпцией записи вы автоматически
            становитесь участником сообщества.</p>
    </div>
    <div class="new-record-form mlr-16 mb-20">
        <form action="#" method="post" enctype="multipart/form-data">
            <div class="form-group mb-20">
                <label class="form-label form-label-hidden">Заголовок</label>
                <input name="name" type="text" placeholder="Заголовок" class="form-control">
                <div class="form-limit">&nbsp;</div>
                <div class="form-error">&nbsp;</div>
            </div>
            <div class="text-editor">
                <div class="text-editor__toolbar btn-toolbar">
                    <div class="slider text-editor-slider mb-20">
                        <div class="slider-wrapper slider-changeable">
                            <ul class="nav edit-text-slide edit-text-scrollable  w-100">
                                <li class="slide">
                                    <button class="semibold"><i class="material-icons">&#xE238;</i> </button>
                                    <button class="italic"><i class="material-icons">&#xE23F;</i> </button>
                                    <button class="underline"><i class="material-icons">&#xE249;</i> </button>
                                    <button class="color"><i class="material-icons">&#xE23A;</i> </button>
                                </li>
                                <li class="slide">
                                    <button class="left-align"><i class="material-icons">&#xE236;</i> </button>
                                    <button class="center-align"><i class="material-icons">&#xE234;</i> </button>
                                    <button class="right-align"><i class="material-icons">&#xE234;</i> </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="text-editor__editable">
                    <span>Введите текст</span>
                    <br>
                </div>
            </div>
            <button data-target="#popup-category" class="btn btn-popup btn-white mb-20">Выберите категорию публикации</button>
            <div class="form-group mb-20">
                <select name="see-note" class="form-select form-select-center" placeholder="Запись видна">
                    <option value="0">Запись видна</option>
                    <option value="1">Всем</option>
                    <option value="2">Только друзьям</option>
                    <option value="3">Только мне</option>

                </select>
            </div>
            <div class="mb-20"><em>Вашу запись увидят и прочитают множетсво людей. Вы получите большее количество комментариев,
                лайков и станенте самым читаемы пользователем</em></div>
            <div class="form-group form-group-checkbox mb-10">
                <input type="checkbox" name="active_mom" id="active_mom">
                <label for="active_mom" class="form-label-large">На конкурс «Активная мама»*</label>
            </div>
            <div class="mb-20"><em>*Я подтверждаю, что запись соответствует правила конкурса «Активная мама» и содержит уникальный контент</em></div>
            <button class="btn btn-confirm btn-shaded w-100 mb-20">Опубликовать</button>
            <div class="text-center"><button class="btn btn-active-flat">Отмена</button></div>
        </form>
    </div>




    
    <!-- Pregnancy calendar -->
    <div class="pregnancy-calendar">
        <div class="pregnancy-calendar__header">
            Следите за ходом<br>
            беременности в личном<br>
            календаре
        </div>
        <div class="pregnancy-calendar__body">
    
    
            <div class="slider pregnancy-calendar__slider">
                <a href="#" class="slider-forward pregnancy-calendar__forward"></a>
                <a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>
    
                <div class="slider-changeable pregnancy-calendar__title">I триместр</div>
    
    
    
    
    
                <div class="slider-changeable slider-wrapper">
                    <div class="slides" style="transform: translate3d(0px, 0px, 0px);">
                        <div class="slide active">
                            <ul class="pregnancy-calendar__months">
                                <li><a href="#">Месяц 1</a></li>
                                <li><a href="#">Месяц 2</a></li>
                                <li><a href="#">Месяц 3</a></li>
                            </ul>
                            <div class="pregnancy-calendar__week-title">Недели:</div>
                            <ul class="pregnancy-calendar__weeks">
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">6</a></li>
                                <li><a href="#">7</a></li>
                                <li><a href="#">8</a></li>
                                <li><a href="#">9</a></li>
                                <li><a href="#">10</a></li>
                                <li><a href="#">11</a></li>
                                <li><a href="#">12</a></li>
                                <li><a href="#">13</a></li>
                            </ul>
                        </div>
    
                        <div class="slide">
                            <ul class="pregnancy-calendar__months">
                                <li><a href="#">Месяц 4</a></li>
                                <li><a href="#">Месяц 5</a></li>
                                <li><a href="#">Месяц 6</a></li>
                            </ul>
                            <div class="pregnancy-calendar__week-title">Недели:</div>
                            <ul class="pregnancy-calendar__weeks">
                                <li><a href="#">14</a></li>
                                <li><a href="#">15</a></li>
                                <li><a href="#">16</a></li>
                                <li><a href="#">17</a></li>
                                <li><a href="#">18</a></li>
                                <li><a href="#">19</a></li>
                                <li><a href="#">20</a></li>
                                <li><a href="#">21</a></li>
                                <li><a href="#">22</a></li>
                                <li><a href="#">23</a></li>
                                <li><a href="#">24</a></li>
                                <li><a href="#">25</a></li>
                                <li><a href="#">26</a></li>
                            </ul>
                        </div>
    
                        <div class="slide">
                            <ul class="pregnancy-calendar__months">
                                <li><a href="#">Месяц 7</a></li>
                                <li><a href="#">Месяц 8</a></li>
                                <li><a href="#">Месяц 9</a></li>
                            </ul>
                            <div class="pregnancy-calendar__week-title">Недели:</div>
                            <ul class="pregnancy-calendar__weeks">
                                <li><a href="#">27</a></li>
                                <li><a href="#">28</a></li>
                                <li><a href="#">29</a></li>
                                <li><a href="#">30</a></li>
                                <li><a href="#">31</a></li>
                                <li><a href="#">32</a></li>
                                <li><a href="#">33</a></li>
                                <li><a href="#">34</a></li>
                                <li><a href="#">35</a></li>
                                <li><a href="#">36</a></li>
                                <li><a href="#">37</a></li>
                                <li><a href="#">38</a></li>
                                <li><a href="#">39</a></li>
                                <li><a href="#">40</a></li>
                            </ul>
                        </div>
    
                    </div>
                </div>
    
                <ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
                    <li class="active"><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                </ul>
    
            </div>
        </div>
    </div>
    <!-- /Pregnancy calendar -->