<div class="content content-user-articles scroll-top pt-10">
	
	<h1 class="mb-20">Статьи для родителей</h1>

	<button data-target="#popup-articles" class="btn btn-popup btn-white mb-20">Разделы статей</button>

	<div class="mlr-16 mb-20">
		<em>Выполнена фильтрация по тегу: <strong>Беременность</strong></em>

	</div>


	<?php include 'inc/block/search.php'; ?>

	<?php $mb = 10; include 'inc/block/tabs-slider.php'; ?>

	<div class="articles mb-20">
		<?php include 'inc/block/articles.php'; ?>
	</div>

</div>

<?php include 'inc/block/pregnancy-calendar.php'; ?>