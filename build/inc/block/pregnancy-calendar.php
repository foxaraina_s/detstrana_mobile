<!-- Pregnancy calendar -->
<div class="pregnancy-calendar">
	<div class="pregnancy-calendar__header">
		Следите за ходом<br>
		беременности в личном<br>
		календаре
	</div>
	<div class="pregnancy-calendar__body">


		<div class="slider pregnancy-calendar__slider">
			<a href="#" class="slider-forward pregnancy-calendar__forward"></a>
			<a href="#" class="slider-rewind pregnancy-calendar__rewind"></a>

			<div class="slider-changeable slider-wrapper">
				<div class="slides">
					<div class="slide">
						<div class="pregnancy-calendar__title">I триместр</div>
						<ul class="pregnancy-calendar__months">
							<li><a href="#">Месяц 1</a></li>
							<li><a href="#">Месяц 2</a></li>
							<li><a href="#">Месяц 3</a></li>
						</ul>
						<div class="pregnancy-calendar__week-title">Недели:</div>
						<ul class="pregnancy-calendar__weeks">
							<?php for($i = 1; $i <= 13; $i++) { ?>
							<li><a href="#"><?php echo $i ?></a></li>
							<?php } ?>
						</ul>
					</div>

					<div class="slide">
						<div class="pregnancy-calendar__title">II триместр</div>
						<ul class="pregnancy-calendar__months">
							<li><a href="#">Месяц 4</a></li>
							<li><a href="#">Месяц 5</a></li>
							<li><a href="#">Месяц 6</a></li>
						</ul>
						<div class="pregnancy-calendar__week-title">Недели:</div>
						<ul class="pregnancy-calendar__weeks">
							<?php for($i = 14; $i <= 26; $i++) { ?>
							<li><a href="#"><?php echo $i ?></a></li>
							<?php } ?>
						</ul>
					</div>

					<div class="slide">
						<div class="pregnancy-calendar__title">III триместр</div>
						<ul class="pregnancy-calendar__months">
							<li><a href="#">Месяц 7</a></li>
							<li><a href="#">Месяц 8</a></li>
							<li><a href="#">Месяц 9</a></li>
						</ul>
						<div class="pregnancy-calendar__week-title">Недели:</div>
						<ul class="pregnancy-calendar__weeks">
							<?php for($i = 27; $i <= 40; $i++) { ?>
							<li><a href="#"><?php echo $i ?></a></li>
							<?php } ?>
						</ul>
					</div>

				</div>
			</div>
	
			<ul class="nav nav-dotted nav-dotted-dark slider-control slider-changeable">
				<li><a href="#"></a></li>
				<li><a href="#"></a></li>
				<li><a href="#"></a></li>
			</ul>

		</div>
	</div>
</div>
<!-- /Pregnancy calendar -->