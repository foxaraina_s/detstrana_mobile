<!-- Feed -->

<!---div class="btn-group btn-group-large btn-group-border-light btn-group-shaded mb-10">
	<button class="btn w-50 btn-active">Лента</button>
	<button class="btn w-50 btn-active-flat btn-white">Популярное</button>
</div-->

<ul class="nav nav-tabs nav-tabs-bordered block-shaded w-100 mb-10">
	<li class="w-50 active"><a href="#">Лента</a></li>
	<li class="w-50"><a href="#">Популярное</a></li>
</ul>

<div class="entries">
	<div class="entries__list">
		<div class="entry">
			
			<div class="entry__header">
				<div class="entry__avatar"><img src="img/tmp/userpic1.png"></div>
				<div class="entry__author">Запись в дневнике<br><strong>Виктория Кудрявцева</strong></div>
				<div class="entry__date">31 мая 2017 года, 12:58</div>
			</div>

			<div class="entry__title">
				Бюджет моей семьи: как мы живем на 6-7 тысяч рублей в месяц
			</div>
			
			<div class="slider entry__images">
				<div class="slider-changeable slider-wrapper">
					<div class="slides">
						<?php foreach(range(1, 5) as $i) { ?>
						<div class="slide"><img src="img/tmp/article2.jpg"></div>
						<?php } ?>
					</div>
				</div>

				<ul class="nav nav-dotted nav-dotted-small nav-dotted-inverse slider-control slider-changeable">
					<?php foreach(range(1, 5) as $i) { ?>
					<li><a href="#"></a></li>
					<?php } ?>
				</ul>
			</div>
			
			<div class="entry__body">
			
				<div class="entry__text">
					Спешу поделиться радостью - мой малыш наконец-то сказал слово "МАМА"!
					Сам он был от этого восторге (как-никак, новое умение), а я была, конечно же, счастлива)
					Свекровь говорит, что есть такое поверье..
				</div>
				
				<p><a href="#" class="entry__readmore">Читать далее..</a></p>
				
				<div class="icon-group entry__icons">
					<a href="#" class="icon-info icon-info-views">31</a>
					<a href="#" class="icon-info icon-info-comments">3</a>
					<a href="#" class="icon-info icon-info-likes">5</a>
				</div>

				<div class="icon-group entry__comment">
					<div class="rel mb-10">
						<button class="btn btn-confirm-flat w-100 entry__comment-open">Комментировать</button>
						<button class="btn btn-close entry__comment-close"></button>
					</div>

					<form class="entry__comment-form">
						<textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
						<button class="btn btn-confirm btn-shaded w-100">Отправить</button>
					</form>
				</div>

			</div>
		</div>
	</div>

	<div class="entries__morecomments">
		<button class="btn btn-active-flat w-100">Еще 15 комментариев</button>
	</div>

</div>



<!-- /Feed -->