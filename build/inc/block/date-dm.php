			<div class="grid grid-2">
				<div class="cell text-center">
					<select name="birthday-day" placeholder="Число" class="form-select">
						<option>Число</option>
						<?php foreach(range(1, 31) as $day) { ?>
						<option value="<?php echo $day ?>"><?php echo $day ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="cell text-center">
					<select name="birthday-month" placeholder="Месяц" class="form-select">
						<option>Месяц</option>
						<?php foreach(['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'] as $key => $mon) { ?>
						<option value="<?php echo $key ?>"><?php echo $mon ?></option>
						<?php } ?>
					</select>
				</div>
			</div>