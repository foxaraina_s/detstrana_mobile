<div class="comments">
	
	<div class="comments__list">

		<div class="comment">

			<div class="comment__header">
				<div class="comment__author"><a href="#">Виктория Кудрявцева</a></div>
				<div class="comment__date">31 мая 2017 года, 12:58</div>
				<div class="comment__icons">
					<a href="#" class="icon-info icon-info-likes-dark">5</a>
				</div>
			</div>

			<div class="comment__body">
				<div class="comment__inner">
					<div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
				</div>
			
				<div class="comment__comment active">
					<div class="rel mb-20">
						<button class="btn btn-flat comment__comment-like">Нравится</button>
						<button class="btn btn-active-flat comment__comment-open">Ответить</button>
						<button class="btn btn-close-thin comment__comment-close"></button>
					</div>

					<form class="comment__comment-form">
						<textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
						<button class="btn btn-confirm btn-shaded w-100">Отправить</button>
					</form>
				</div>

			</div>
		</div>
	
		<div class="comment">

			<div class="comment__header">
				<div class="comment__author"><a href="#">Виктория Кудрявцева</a></div>
				<div class="comment__date">31 мая 2017 года, 12:58</div>
				<div class="comment__icons">
					<a href="#" class="icon-info icon-info-likes-dark">5</a>
				</div>
			</div>

			<div class="comment__body">
				<div class="comment__inner">

					<div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>

					<div class="slider comment__slider">
						<div class="slider-changeable slider-wrapper">
							<div class="slides">
								<?php foreach(range(1, 3) as $i) { ?>
								<div class="slide">
									<img src="img/tmp/article5.jpg">
									<img src="img/tmp/article5.jpg">
								</div>
								<?php } ?>
							</div>
						</div>

						<ul class="nav nav-dotted nav-dotted-small slider-control slider-changeable">
							<?php foreach(range(1, 3) as $i) { ?>
							<li><a href="#"></a></li>
							<?php } ?>
						</ul>
					</div>

				</div>
			
				<div class="comment__comment active">
					<div class="rel mb-20">
						<button class="btn btn-flat comment__comment-like">Нравится</button>
						<button class="btn btn-active-flat comment__comment-open">Ответить</button>
						<button class="btn btn-close-thin comment__comment-close"></button>
					</div>

					<form class="comment__comment-form">
						<textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
						<div class="rel mb-10">
							<button class="btn btn-confirm w-66">Комментировать</button>
							<button class="btn btn-plus pull-right"></button>
						</div>
					</form>
				</div>
				
			</div>
		</div>


		<div class="comment">

			<div class="comment__header">
				<div class="comment__author"><a href="#">Катя Медунцова</a> ответила <a href="#">Виктория Кудрявцева</a></div>
				<div class="comment__date">31 мая 2017 года, 12:58</div>
				<div class="comment__icons">
					<a href="#" class="icon-info icon-info-likes-dark">5</a>
				</div>
			</div>

			<div class="comment__body">
				<div class="comment__inner">
					<div class="comment__text">Дома у нас малыш уже умеет позировать. Как поведёт в студии-сложно сказать.</div>
				</div>
			
				<div class="comment__comment">
					<div class="rel mb-20">
						<button class="btn btn-flat comment__comment-like">Нравится</button>
						<button class="btn btn-active-flat comment__comment-open">Ответить</button>
						<button class="btn btn-close-thin comment__comment-close"></button>
					</div>

					<form class="comment__comment-form">
						<textarea rows="5" placeholder="Расскажите, что вы думаете по этому поводу"></textarea>
						<button class="btn btn-confirm btn-shaded w-100">Отправить</button>
					</form>
				</div>

			</div>
		</div>

	</div>

</div>