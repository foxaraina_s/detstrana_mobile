<!-- Popup profile -->
<div id="popup-profile" class="popup popup-profile popup-right">
	<i class="popup__close"></i>
	
	<div class="popup__header">
		<div class="popup-profile__avatar"><img src="img/tmp/userpic1.png"></div>
		<div class="popup-profile__username">Анна Петрова</div>
	</div>

	<div class="popup__body">
		<ul class="popup-profile__links">
			<li class="active"><a href="#">Дневник</a></li>
			<li><a href="#">Новости</a></li>
			<li><a href="#">Сообщения</a> <span class="badge badge-notice popup-profile__messages">8</span></li>
			<li><a href="#">Друзья</a></li>
		</ul>
		<ul class="popup-profile__links">
			<li><a href="#">Сообщества</a></li>
			<li><a href="#">Уведомления</a></li>
			<!--li class="alt"><a href="#">Настройки</a></li>
			<li class="alt"><a href="#">Выход</a></li-->
		</ul>
		<div class="popup-profile__bottom-links">
			<a href="#">Настройки</a><br>
			<a href="#">Выход</a>
		</div>
	</div>
</div>
<!-- /Popup profile -->