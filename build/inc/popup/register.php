<!-- Popup register -->
<div id="popup-register" class="popup popup-register popup-right slider">
	<div class="popup__close"></div>
	<div class="popup__header block-shaded">
		<ul class="nav nav-tabs nav-tabs-bordered nav-tabs-large">
			<li class="w-50 active"><a href="#popup-register__login">Вход</a></li>
			<li class="w-50"><a href="#popup-register__register">Регистрация</a></li>
		</ul>
	</div>

	<div class="popup__body">
		<!--div style="background: #FFF; height:1px;overflow:hidden;">&nbsp;</div-->
		<div class="tabs popup-register__tabs">

			<div id="popup-register__login" class="tab popup-register__login">
				<form action="" method="post">
					<div class="popup-register__block">
						<div class="form-group popup-register__user">
							<label class="form-label">Ваше имя</label>
							<input name="user" type="text" placeholder="Ваше имя" maxlength="20" class="form-control">
							<div class="form-limit">&nbsp;</div>
							<div class="form-error">Заполните поле</div>
						</div>

						<div class="form-group popup-register__password">
							<label class="form-label">Ваш пароль</label>
							<input name="name" type="password" placeholder="Ваш пароль" class="form-control">
							<div class="form-limit">&nbsp;</div>
							<div class="form-error">Заполните поле</div>
						</div>

						<div class="text-center mb-20 landscape-only"><button class="btn btn-confirm btn-shaded w-70">Войти</button></div>
					</div>

					<div class="popup-register__block">

						<div class="form-group form-group-checkbox">
							<input type="checkbox" name="is_agree" value="1" id="popup-register__login-is-agree">
							<label for="popup-register__login-is-agree">Я даю согласие на обработку персональных данных и соглашаюсь с <a href="#">пользовательским соглашением</a></label>
						</div>

						<div class="form-group form-group-checkbox">
							<input type="checkbox" name="is_subscribe" value="1" id="popup-register__login-is-subscribe">
							<label for="popup-register__login-is-subscribe">Я хочу получать email-письма о конкурсах и акциях</label>
						</div>

						<div class="text-center mb-20 portrait-only"><button class="btn btn-confirm btn-shaded w-70">Войти</button></div>


						<div class="popup-register__social-title">Или войдите с помощью</div>

						<div class="popup-register__social mb-20">
							<a href="#" class="icon-fb"></a>
							<a href="#" class="icon-vk"></a>
							<a href="#" class="icon-ok"></a>
						</div>

						<div class="text-center"><a href="#" class="btn btn-active-flat">Забыли пароль?</a></div>
					</div>
				</form>
			</div>

			<div id="popup-register__register" class="tab popup-register__register active">
				<form action="" method="post">

					<div class="popup-register__block">

						<div class="form-group popup-register__user">
							<label class="form-label">Ваше имя</label>
							<input name="user" type="text" placeholder="Ваше имя" maxlength="20" class="form-control">
							<div class="form-limit">0 / 20</div>
							<div class="form-error">Заполните поле</div>
						</div>

						<div class="form-group form-group-error popup-register__email">
							<label class="form-label">Ваш e-mail</label>
							<input name="name" type="text" placeholder="Ваш e-mail" maxlength="100" class="form-control">
							<div class="form-limit">0 / 100</div>
							<div class="form-error">Заполните поле</div>
						</div>

						<div class="form-group popup-register__password">
							<label class="form-label">Ваш пароль</label>
							<input name="name" type="password" placeholder="Ваш пароль" class="form-control">
							<div class="form-limit">&nbsp;</div>
							<div class="form-error">Заполните поле</div>
						</div>
					</div>

					<div class="popup-register__block">
						<div class="form-group form-group-checkbox">
							<input type="checkbox" name="is_agree" id="popup-register__register-is-agree">
							<label for="popup-register__register-is-agree">Я даю согласие на обработку персональных данных и соглашаюсь с <a href="#">пользовательским соглашением</a></label>
						</div>

						<div class="form-group form-group-checkbox">
							<input type="checkbox" name="is_subscribe" id="popup-register__register-is-subscribe">
							<label for="popup-register__register-is-subscribe">Я хочу получать email-письма о конкурсах и акциях</label>
						</div>

						<div class="text-center mb-20"><button class="btn btn-confirm btn-shaded w-70">Зарегистрироваться</button></div>
					

						<div class="popup-register__social-title">Или зарегистрируйтесь с помощью</div>

						<div class="block-social">
							<?php include 'inc/block/social.php' ?>
						</div>
					</div>
				</form>
			</div>




		</div>

	</div>
</div>
<!-- /Popup register -->