<!-- Popup Date -->
<div id="popup-date" class="popup popup-date popup-full">
	<div class="popup__close"></div>
	<div class="popup__header">Выберите дату</div>	
	<div class="popup__body">
		<div class="popup-date__calendar">
			<div class="popup-date__calendar-header">
				<a href="#" class="popup-date__calendar-rewind"></a>
				<a href="#" class="popup-date__calendar-forward"></a>
				<div class="popup-date__calendar-title">Июль 2017</div>
			</div>
			
			<div class="popup-date__calendar-weekdays">
				<?php foreach(['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'] as $weekday) { ?>
				<div><?php echo $weekday ?></div>
				<?php } ?>
			</div>
			
			<div class="popup-date__calendar-days">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<div>1</div>
				<div>2</div>
				
				<div>3</div>
				<div>4</div>
				<div>5</div>
				<div>6</div>
				<div>7</div>
				<div>8</div>
				<div>9</div>
				
				<div>10</div>
				<div>11</div>
				<div>12</div>
				<div class="active">13</div>
				<div>14</div>
				<div>15</div>
				<div>16</div>
				
				<div>17</div>
				<div>18</div>
				<div>19</div>
				<div>20</div>
				<div>21</div>
				<div>22</div>
				<div>23</div>
				
				<div>24</div>
				<div>25</div>
				<div>26</div>
				<div>27</div>
				<div>28</div>
				<div>29</div>
				<div>30</div>					
			</div>
		</div>
	</div>
</div>
<!-- /Popup Date -->

<script>


$('#popup-date .popup-date__calendar')
	.on('ds-change', function(event, step) {
		var
			step = ($(this).data('step') || 0) + step;
			now = new Date(),
			dt = new Date(),
			html = '',
			months = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];

		now.setMonth(now.getMonth() + step);
		dt.setMonth(dt.getMonth() + step);
		dt.setDate(1);

		for(var i=1; i<=5; i++)
		{
			for(var j=1; j<=7; j++)

				if(now.getMonth() == dt.getMonth() && (dt.getDay() == j || (dt.getDay() == 0 && j == 7)))
				{
					html += '<div>' + dt.getDate() + '</div>';
					dt.setDate(dt.getDate() + 1);
				}
				else
					html += '<div></div>';

			if(now.getMonth() < dt.getMonth()) break;

		}

		$(this).find('.popup-date__calendar-title').html(months[now.getMonth()] + ' ' + now.getFullYear());
		$(this).find('.popup-date__calendar-days').html(html);
		$(this).data('step', step);
	})
	.trigger('ds-change', 0)
	.find('.popup-date__calendar-rewind')
		.on('click', function() {
			$(this).parents('.popup-date__calendar').eq(0).trigger('ds-change', -1);
		})
	.end()
	.find('.popup-date__calendar-forward')
		.on('click', function() {
			$(this).parents('.popup-date__calendar').eq(0).trigger('ds-change', 1);
		})	
</script>